<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakulsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('makuls', function (Blueprint $table) {
            $table->increments('id');
            $table->string("kode", 30);
            $table->string("nama", 100);
            $table->string('jenis', 30);
            $table->string('tipe', 30);
            $table->integer('kelompok_makul_id')->unsigned()->nullable();//liat di putrajaya.ac.id
            $table->integer('sks_teori')->default(0);
            $table->integer('sks_praktek')->default(0);
            $table->integer('sks_total')->default(0);
            $table->integer("kurikulum_id")->unsigned();
            $table->integer("semester")->default(0);

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('kurikulum_id')->references('id')->on('kurikulums')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kelompok_makul_id')->references('id')->on('kelompok_makuls')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('makuls');
    }
}
