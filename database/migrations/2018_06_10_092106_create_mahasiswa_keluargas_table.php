<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaKeluargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_keluargas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("mahasiswa_id")->unsigned();
            $table->string("ayah", 100)->nullable();
            $table->string("kerja_ayah", 100)->nullable();
            $table->string("ibu", 100)->nullable();
            $table->string("kerja_ibu", 100)->nullable();
            $table->string("alamat", 255)->nullable();
            $table->string("no_hp", 20)->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas')->onDelete('cascade')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_keluargas');
    }
}
