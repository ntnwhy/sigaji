<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('krs', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('mahasiswa_id')->unsigned();
            $table->integer('kurikulum_id')->unsigned();
            $table->integer('progdi_id')->unsigned();
            $table->integer('proyek_pendidikan_id')->unsigned();
            $table->integer('dosen_pa_id')->unsigned();
            $table->integer('semester_berjalan');
            $table->integer('semester');
            $table->string('semester_partial', 20)->nullable();
            $table->integer('jml_sks');
            $table->text('komentar')->nullable();

            $table->boolean('is_approved_pa')->default(false);
            $table->string('approved_by_pa', 50)->nullable();
            $table->dateTime('approved_at_pa')->nullable();

            $table->boolean('is_approved_keu')->default(false);
            $table->string('approved_by_keu', 50)->nullable();
            $table->dateTime('approved_at_keu')->nullable();

            $table->integer('status')->default(101);

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kurikulum_id')->references('id')->on('kurikulums')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('progdi_id')->references('id')->on('progdis')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('proyek_pendidikan_id')->references('id')->on('proyek_pendidikans')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen_pa_id')->references('id')->on('dosens')->onDelete('restrict')
                ->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('krs');
    }
}
