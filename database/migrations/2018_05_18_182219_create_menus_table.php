<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 100);
            $table->string('icon')->nullable();
            $table->string('url')->nullable();
            $table->integer('menu_id')->unsigned()->nullable();
            $table->integer('seq')->default(0);
            $table->boolean('status')->default(true);
            $table->string('tag', 100);
            $table->timestamps();

            $table->foreign('menu_id')->references('id')->on('menus')
                ->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
