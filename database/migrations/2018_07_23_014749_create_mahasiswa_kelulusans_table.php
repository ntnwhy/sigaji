<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaKelulusansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_kelulusans', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('mahasiswa_id')->unsigned();
            $table->text('judul_ta')->nullable();
            $table->double('ipk')->nullable()->default(0);
            $table->integer('sks_lulus')->default(0)->nullable();
            $table->integer('dosen1_id')->unsigned()->nullable();
            $table->integer('dosen2_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen2_id')->references('id')->on('dosens')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen1_id')->references('id')->on('dosens')
                ->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_kelulusans');
    }
}
