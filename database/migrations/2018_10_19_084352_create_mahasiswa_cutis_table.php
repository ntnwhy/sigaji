<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaCutisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_cutis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mahasiswa_id')->unique();
            $table->integer('proyek_pendidikan_id')->unsigned();
            $table->date('tgl');
            $table->string('alasan', 100);
            $table->text('keterangan')->nullable();

            $table->boolean('is_approved')->default(false);
            $table->string('approved_by', 50)->nullable();
            $table->dateTime('approved_at')->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();
            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas')
                ->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('proyek_pendidikan_id')->references('id')->on('proyek_pendidikans')
                ->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_cutis');
    }
}
