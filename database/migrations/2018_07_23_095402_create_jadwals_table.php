<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('jadwals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proyek_pendidikan_id')->unsigned();
            $table->integer('hari');
            $table->integer('ruangan_id')->unsigned();
            $table->time('jam_mulai');
            $table->time('jam_selesai');
            $table->integer('dosen_id')->unsigned();
            $table->integer('semester');
            $table->integer('kelas_kuliah_id')->unsigned()->nullable();
            $table->integer('makul_proyekpend_id')->unsigned()->nullable();
            $table->integer('makul_id')->unsigned()->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();
            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('proyek_pendidikan_id')->references('id')->on('proyek_pendidikans')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('ruangan_id')->references('id')->on('ruangans')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen_id')->references('id')->on('dosens')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('makul_id')->references('id')->on('makuls')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('makul_proyekpend_id')->references('id')->on('makul_proyekpends')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kelas_kuliah_id')->references('id')->on('kelas_kuliahs')
                ->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwals');
    }
}
