<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaPindahansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_pindahans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("mahasiswa_id")->unsigned();
            $table->string("nim", 50);
            $table->string("nama", 100);
            $table->string("akredetasi", 100)->nullable();
            $table->string("progdi", 100)->nullable();
            $table->integer("jml_sks")->default(0);
            $table->string("alamat", 255)->nullable();
            $table->integer('provinsi_id')->unsigned()->nullable();
            $table->integer('kota_id')->unsigned()->nullable();
            $table->integer('kecamatan_id')->unsigned()->nullable();
            $table->bigInteger('kelurahan_id')->unsigned()->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('provinsi_id')->references('id')->on('provinsis')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kota_id')->references('id')->on('kecamatans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kelurahan_id')->references('id')->on('kelurahans')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_pindahans');
    }
}
