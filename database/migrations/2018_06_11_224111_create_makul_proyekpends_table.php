<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakulProyekpendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('makul_proyekpends', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kurikulum_id')->unsigned();
            $table->integer('proyek_pendidikan_id')->unsigned();
            $table->integer('makul_id')->unsigned();
            $table->integer('sks_teori')->default(0);
            $table->integer('sks_praktek')->default(0);
            $table->integer('sks_total')->default(0);

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('kurikulum_id')->references('id')->on('kurikulums')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('proyek_pendidikan_id')->references('id')->on('proyek_pendidikans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('makul_id')->references('id')->on('makuls')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('makul_proyekpends');
    }
}
