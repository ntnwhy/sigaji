<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelasKuliahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelas_kuliahs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 100);
            $table->integer('proyek_pendidikan_id')->unsigned();
            $table->integer('makul_proyekpend_id')->unsigned();
            $table->integer('makul_id')->unsigned();
            $table->integer('dosen1_id')->unsigned();
            $table->integer('dosen2_id')->unsigned()->nullable();
            $table->integer('dosen3_id')->unsigned()->nullable();
            $table->integer('dosen4_id')->unsigned()->nullable();
            $table->string('ket', 255)->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('dosen1_id')->references('id')->on('dosens')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen2_id')->references('id')->on('dosens')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen3_id')->references('id')->on('dosens')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen4_id')->references('id')->on('dosens')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('proyek_pendidikan_id')->references('id')->on('proyek_pendidikans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('makul_proyekpend_id')->references('id')->on('makul_proyekpends')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('makul_id')->references('id')->on('makuls')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelas_kuliahs');
    }
}
