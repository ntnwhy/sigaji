<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mahasiswa_id')->unsigned();
            $table->integer('semester')->default(0);
            $table->integer('proyek_pendidikans_id')->unsigned();
            $table->integer('kelas_kuliah_id')->unsigned();
            $table->integer('makul_id')->unsigned();
            $table->double('nilai')->default(0);

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('proyek_pendidikans_id')->references('id')->on('proyek_pendidikans')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kelas_kuliah_id')->references('id')->on('kelas_kuliahs')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('makul_id')->references('id')->on('makuls')
                ->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilais');
    }
}
