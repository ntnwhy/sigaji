<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('kotas', function (Blueprint $table) {
            $table->integer('id')->primary()->unique();
            $table->string('nama');
            $table->integer('provinsi_id')->unsigned();
            $table->timestamps();

            $table->foreign('provinsi_id')->references('id')->on('provinsis')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kotas');
    }
}
