<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitusisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institusis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 30)->unique();
            $table->string('nama');
            $table->string('alamat')->nullable();
            $table->integer('provinsi_id')->unsigned();
            $table->integer('kota_id')->unsigned();
            $table->integer('kecamatan_id')->unsigned();
            $table->bigInteger('kelurahan_id')->unsigned();
            $table->string('kodepos')->nullable();
            $table->string('fax')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('web')->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('provinsi_id')->references('id')->on('provinsis')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kota_id')->references('id')->on('kecamatans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kelurahan_id')->references('id')->on('kelurahans')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institusis');
    }
}
