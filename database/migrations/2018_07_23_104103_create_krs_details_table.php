<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKrsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('krs_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('krs_id')->unsigned();
            $table->integer('kelas_kuliahs_id')->unsigned();
            $table->integer('makul_id')->unsigned();
            $table->integer('jadwal_id')->unsigned();
            $table->integer('dosen_id')->unsigned();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('krs_id')->references('id')->on('mahasiswas')
                ->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('kelas_kuliahs_id')->references('id')->on('kelas_kuliahs')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('jadwal_id')->references('id')->on('jadwals')
                ->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen_id')->references('id')->on('dosens')
                ->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('krs_details');
    }
}
