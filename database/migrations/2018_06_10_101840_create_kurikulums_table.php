<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKurikulumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurikulums', function (Blueprint $table) {

            $table->increments('id');
            $table->string("kode", 30)->unique();
            $table->string("nama", 100);
            $table->integer("progdi_id")->unsigned();
            $table->integer("min_sks_lulus")->default(0);
            $table->year("thn_mulai");
            $table->string("ket", 255)->nullable();
            $table->boolean("status")->default(true);

            $table->boolean("is_approved")->default(false);
            $table->dateTime("approved_at")->nullable();
            $table->string("approved_by", 50)->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();

            $table->timestamps();
            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('progdi_id')->references('id')->on('progdis')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kurikulums');
    }
}
