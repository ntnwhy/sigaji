<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaSekolahasalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_sekolahasals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("mahasiswa_id")->unsigned();
            $table->string("nama", 100);
            $table->string("alamat", 255)->nullable();
            $table->integer('provinsi_id')->unsigned()->nullable();
            $table->integer('kota_id')->unsigned()->nullable();
            $table->integer('kecamatan_id')->unsigned()->nullable();
            $table->bigInteger('kelurahan_id')->unsigned()->nullable();
            $table->integer("thn_lulus")->default(0)->nullable();
            $table->string("no_ijasah", 50)->nullable()->nullable();
            $table->string("jurusan", 100)->nullable()->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('provinsi_id')->references('id')->on('provinsis')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kota_id')->references('id')->on('kotas')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kelurahan_id')->references('id')->on('kelurahans')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_sekolahasals');
    }
}
