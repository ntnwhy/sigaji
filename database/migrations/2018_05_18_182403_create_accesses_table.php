<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->integer('menu_id')->unsigned();
            $table->boolean('ins')->default(false);
            $table->boolean('upd')->default(false);
            $table->boolean('del')->default(false);
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles')
                ->onDelete('restrict')->onUpdate("cascade");

            $table->foreign('menu_id')->references('id')->on('menus')
                ->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accesses');
    }
}
