<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik', 30)->unique();
            $table->string('nidn', 30)->nullable();
            $table->string('nama', 100);
            $table->string('tipe', 30)->nullable();
            $table->string('gender', 20);
            $table->string('pob', 50)->nullable();
            $table->date('dob')->nullable();
            $table->string('ktp', 26)->nullable();
            $table->date('tgl_masuk')->nullable();
            $table->integer('progdi_id')->unsigned();
            $table->string('pendidikan', 30);
            $table->string('gelar', 50)->nullable();
            $table->string('jabatan', 50)->nullable();
            $table->integer('statusdosen_id')->unsigned()->nullable();
            $table->string('alamat', 255)->nullable();
            $table->string('rt', 3)->nullable();
            $table->string('rw', 3)->nullable();
            $table->string('kodepos', 10)->nullable();
            $table->bigInteger('kelurahan_id')->unsigned()->nullable();
            $table->integer('kecamatan_id')->unsigned()->nullable();
            $table->integer('kota_id')->unsigned()->nullable();
            $table->integer('provinsi_id')->unsigned()->nullable();
            $table->string('agama', 20)->nullable();
            $table->string('telp', 20)->nullable();
            $table->string('email', 150)->nullable();
            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();

            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('statusdosen_id')->references('id')->on('statusdosens')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('provinsi_id')->references('id')->on('provinsis')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kota_id')->references('id')->on('kotas')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kelurahan_id')->references('id')->on('kelurahans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('progdi_id')->references('id')->on('progdis')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosens');
    }
}
