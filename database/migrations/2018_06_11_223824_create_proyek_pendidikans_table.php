<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyekPendidikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyek_pendidikans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 30)->unique();
            $table->integer("kurikulum_id")->unsigned();
            $table->integer("progdi_id")->unsigned();
            $table->year('tahun');
            $table->string('semester', 30);
            $table->boolean('status')->default(true);

            $table->boolean('is_approved')->default(false);
            $table->string('approved_by', 50)->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('kurikulum_id')->references('id')->on('kurikulums')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('progdi_id')->references('id')->on('progdis')->onDelete('restrict')->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyek_pendidikans');
    }
}
