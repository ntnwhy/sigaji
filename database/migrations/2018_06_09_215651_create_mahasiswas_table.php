<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nim', 30)->unique();
            $table->string('no_daftar', 30)->nullable();
            $table->string('nama', 100);
            $table->string('gender', 1);
            $table->string('pob', 50);
            $table->date('dob');
            $table->string('agama', 50);
            $table->string('gol_darah', 1)->nullable();
            $table->string('alamat_asal', 255);
            $table->string('rt', 3);
            $table->string('rw', 3);
            $table->integer('provinsi_id')->unsigned()->nullable();
            $table->integer('kota_id')->unsigned()->nullable();
            $table->integer('kecamatan_id')->unsigned()->nullable();
            $table->bigInteger('kelurahan_id')->unsigned()->nullable();
            $table->string('kodepos', 10)->nullable();
            $table->string('alamat_domisili', 255)->nullable();
            $table->string("no_telp", 20)->nullable();
            $table->string("no_hp", 20)->nullable();
            $table->string("email", 150)->nullable();
            $table->string("no_ktp", 16)->nullable();
            $table->integer("status_mhs_id")->unsigned();
            $table->integer("thn_masuk")->default(0);
            $table->string("semester_masuk", 30)->nullable();
            $table->integer("progdi_id")->unsigned();
            $table->integer("mhs_tipe_id")->unsigned();
            $table->integer('dosen_pa_id')->unsigned()->nullable();
            $table->integer('semester_berjalan')->default(1);
            $table->string('password', 200)->nullable();

            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();

            $table->boolean("is_deleted")->default(false);
            $table->string("deleted_by", 50)->nullable();
            $table->softDeletes();

            $table->foreign('provinsi_id')->references('id')->on('provinsis')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kota_id')->references('id')->on('kotas')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('kelurahan_id')->references('id')->on('kelurahans')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('status_mhs_id')->references('id')->on('status_mhs')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('progdi_id')->references('id')->on('progdis')->onDelete('restrict')->onUpdate("cascade");
            $table->foreign('dosen_pa_id')->references('id')->on('dosens')->onDelete('restrict')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
