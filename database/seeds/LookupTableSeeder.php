<?php

use Illuminate\Database\Seeder;
use App\Lookup;

class LookupTableSeeder extends Seeder {

    public function run(){
        $data = [
            [
                'param'     => 'SYSTEM_NAME',
                'value'     => 'title_app',
                'display'   => 'Sistem Informasi Akademik AAK Putra Jaya Batam'
            ],
            [
                'param'     => 'PROFILE_NAME',
                'value'     => 'profil pengguna',
                'display'   => 'MA'
            ],
            [
                'param'     => 'RELIGION',
                'value'     => 'Islam',
                'display'   => 'Islam'
            ],
            [
                'param'     => 'RELIGION',
                'value'     => 'Kristen',
                'display'   => 'Kristen'
            ],
            [
                'param'     => 'RELIGION',
                'value'     => 'Katolik',
                'display'   => 'Katolik'
            ],
            [
                'param'     => 'RELIGION',
                'value'     => 'Hindu',
                'display'   => 'Hindu'
            ],
            [
                'param'     => 'RELIGION',
                'value'     => 'Budha',
                'display'   => 'Budha'
            ],
            [
                'param'     => 'RELIGION',
                'value'     => 'Konghucu',
                'display'   => 'Konghucu'
            ],
            [
                'param'     => 'GENDER',
                'value'     => 'L',
                'display'   => 'Laki - laki'
            ],
            [
                'param'     => 'GENDER',
                'value'     => 'P',
                'display'   => 'Perempuan'
            ],
            [
                'param'     => 'DAY',
                'value'     => '1',
                'display'   => 'Senin'
            ],
            [
                'param'     => 'DAY',
                'value'     => '2',
                'display'   => 'Selasa'
            ],
            [
                'param'     => 'DAY',
                'value'     => '3',
                'display'   => 'Rabu'
            ],
            [
                'param'     => 'DAY',
                'value'     => '4',
                'display'   => 'Kamis'
            ],
            [
                'param'     => 'DAY',
                'value'     => '5',
                'display'   => 'Jumat'
            ],
            [
                'param'     => 'DAY',
                'value'     => '6',
                'display'   => 'Sabtu'
            ],
            [
                'param'     => 'SEMESTER',
                'value'     => '1',
                'display'   => 'Ganjil'
            ],
            [
                'param'     => 'SEMESTER',
                'value'     => '2',
                'display'   => 'Genap'
            ],
            [
                'param'     => 'JENIS_MAKUL',
                'value'     => 'Wajib',
                'display'   => 'Wajib'
            ],
            [
                'param'     => 'JENIS_MAKUL',
                'value'     => 'Pilihan',
                'display'   => 'Pilihan'
            ],
            [
                'param'     => 'TIPE_MAKUL',
                'value'     => 'Teori',
                'display'   => 'Teori'
            ],
            [
                'param'     => 'TIPE_MAKUL',
                'value'     => 'Praktek',
                'display'   => 'Praktek'
            ],
            [
                'param'     => 'TIPE_MAKUL',
                'value'     => 'Teori & Praktek',
                'display'   => 'Teori & Praktek'
            ],
            [
                'param'     => 'SEMESTER_FULL',
                'value'     => '1',
                'display'   => '1'
            ],
            [
                'param'     => 'SEMESTER_FULL',
                'value'     => '2',
                'display'   => '2'
            ],
            [
                'param'     => 'SEMESTER_FULL',
                'value'     => '3',
                'display'   => '3'
            ],
            [
                'param'     => 'SEMESTER_FULL',
                'value'     => '4',
                'display'   => '4'
            ],
            [
                'param'     => 'SEMESTER_FULL',
                'value'     => '5',
                'display'   => '5'
            ],
            [
                'param'     => 'SEMESTER_FULL',
                'value'     => '6',
                'display'   => '6'
            ],
            [
                'param'     => 'SEMESTER_FULL',
                'value'     => '7',
                'display'   => '7'
            ],
            [
                'param'     => 'SEMESTER_FULL',
                'value'     => '8',
                'display'   => '8'
            ],
            [
                'param'     => 'SEMESTER_PARTIAL',
                'value'     => 'Ganjil',
                'display'   => 'Ganjil'
            ],
            [
                'param'     => 'SEMESTER_PARTIAL',
                'value'     => 'Genap',
                'display'   => 'Genap'
            ],
            [
                'param'     => 'MAHASISWA_TIPE',
                'value'     => 'Reguler',
                'display'   => 'Reguler'
            ],
            [
                'param'     => 'MAHASISWA_TIPE',
                'value'     => 'Transfer',
                'display'   => 'Transfer'
            ],
            [
                'param'     => 'MAHASISWA_TIPE',
                'value'     => 'Pindahan',
                'display'   => 'Pindahan'
            ],
            [
                'param'     => 'DOSEN_STATUS',
                'value'     => 'Dosen Tetap',
                'display'   => 'Dosen Tetap'
            ],
            [
                'param'     => 'DOSEN_STATUS',
                'value'     => 'Dosen Tidak Tetap',
                'display'   => 'Dosen Tidak Tetap'
            ],
            [
                'param'     => 'DOSEN_STATUS',
                'value'     => 'Dosen DPK',
                'display'   => 'Dosen DPK'
            ],
            [
                'param'     => 'JABATAN_AKADEMIK',
                'value'     => 'Tenaga Pengajar',
                'display'   => 'Tenaga Pengajar'
            ],
            [
                'param'     => 'JABATAN_AKADEMIK',
                'value'     => 'Asisten Ahli',
                'display'   => 'Asisten Ahli'
            ],
            [
                'param'     => 'JABATAN_AKADEMIK',
                'value'     => 'Lektor',
                'display'   => 'Lektor'
            ],
            [
                'param'     => 'JABATAN_AKADEMIK',
                'value'     => 'Lektor Kepala',
                'display'   => 'Lektor Kepala'
            ],
            [
                'param'     => 'JABATAN_AKADEMIK',
                'value'     => 'Guru Besar',
                'display'   => 'Guru Besar'
            ],
            [
                'param'     => 'ALASAN_CUTI',
                'value'     => 'Sakit',
                'display'   => 'Sakit'
            ],
            [
                'param'     => 'ALASAN_CUTI',
                'value'     => 'Bekerja',
                'display'   => 'Bekerja'
            ],
            [
                'param'     => 'ALASAN_CUTI',
                'value'     => 'Hamil',
                'display'   => 'Hamil'
            ],
            [
                'param'     => 'ALASAN_CUTI',
                'value'     => 'Lainnya',
                'display'   => 'Lainnya'
            ],
        ];

        foreach($data as $r){
            $res = new Lookup;

            $res->param         = $r['param'];
            $res->item_value    = $r['value'];
            $res->item_display  = $r['display'];

            $res->save();
        }
    }
}
