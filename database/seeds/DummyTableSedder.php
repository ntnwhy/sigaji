<?php

use Illuminate\Database\Seeder;

class DummyTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $kurikulum = \App\Models\Kurikulum::query()->first();
        $proyek     = \App\Models\ProyekPendidikan::query()->first();

        $makul = \App\Models\Makul::query()->where('semester', 2)->get();
        foreach ($makul as $r){

            $data  = new \App\Models\Makul_proyekpend();

            $data->kurikulum_id         = $kurikulum->id;
            $data->proyek_pendidikan_id = $proyek->id;
            $data->makul_id             = $r->id;
            $data->sks_teori            = $r->sks_teori;
            $data->sks_praktek          = $r->sks_teori;
            $data->sks_total            = $r->sks_teori;
            $data->sks_total            = $r->sks_teori;

            $data->save();
        }
    }
}
