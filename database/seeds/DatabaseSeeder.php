<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

//        $this->call(DosenUserTableSeeder::class);
//        $this->call(DummyTableSedder::class);
//         $this->call(RoleTableSeeder::class);
//         $this->call(UserTableSeeder::class);
//         $this->call(LookupTableSeeder::class);
//         $this->call(MenuTableSeeder::class);
//         $this->call(ProvinsiTableSeeder::class);
//         $this->call(KotaTableSeeder::class);
//         $this->call(KecamatanTableSeeder::class);
//         $this->call(KelurahanTableSeeder::class);
         $this->call(ScalaNilaiTableSeeder::class);
    }
}
