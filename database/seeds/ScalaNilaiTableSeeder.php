<?php

use Illuminate\Database\Seeder;

class ScalaNilaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["E", 0, 1.5],
            ["D", 1.5, 2.5],
            ["C", 2.5, 3],
            ["B", 3, 3.5],
            ["A", 3.5, 4],
        ];

        foreach ($data as $r){
            $temp = new \App\Models\ScalaNilai();

            $temp->huruf    = $r[0];
            $temp->mulai    = $r[1];
            $temp->sampai   = $r[2];

            $temp->save();
        }
    }
}
