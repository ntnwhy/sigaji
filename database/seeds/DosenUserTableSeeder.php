<?php

use Illuminate\Database\Seeder;

class DosenUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $dosen = \App\Models\Dosen::all();

        foreach ($dosen as $r){
            $data = new \App\User();

            $data->email    = $r->nik;
            $data->name     = $r->nama;
            $data->password = \Illuminate\Support\Facades\Hash::make($r->nik);
            $data->role_id  = 4;

            $data->save();
        }
    }
}
