<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('su', true)->first();

        $data = new User;

        $data->name     = 'Administrator';
        $data->email    = 'admin';
        $data->password = Hash::make('123456');
        $data->role_id  = $role->id;

        $data->save();
    }
}
