<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $data = [
            [
                'nama'      => 'Dashboard',
                'icon'      => 'fa fa-dashboard',
                'url'       => '',
                'seq'       => 1,
                'tag'       => 'home'
            ],
            [
                'nama'      => 'User Manager',
                'icon'      => 'fa fa-group',
                'url'       => '#',
                'seq'       => 2,
                'tag'       => 'userman'
            ]
        ];

        $id = '';

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];

            $menu->save();
            $id = $menu->id;
        }

        $data = [
            [
                'nama'      => 'Users',
                'icon'      => 'fa fa-users',
                'url'       => 'users',
                'seq'       => 1,
                'tag'       => 'userman.users'
            ],
            [
                'nama'      => 'Role',
                'icon'      => 'fa fa-cog',
                'url'       => 'roles',
                'seq'       => 2,
                'tag'       => 'userman.role'
            ],
            [
                'nama'      => 'Menu',
                'icon'      => 'fa fa-server',
                'url'       => 'menus',
                'seq'       => 3,
                'tag'       => 'userman.menu'
            ],
            [
                'nama'      => 'Permission',
                'icon'      => 'fa fa-key',
                'url'       => 'permission',
                'seq'       => 4,
                'tag'       => 'userman.permission'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }
        

        $data = [
                'nama'      => 'Master Academic',
                'icon'      => 'fa fa-university',
                'url'       => '#',
                'seq'       => 3,
                'tag'       => 'masteracademic'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;

        $data = [
            [
                'nama'      => 'Program Studi',
                'icon'      => 'fa fa-university',
                'url'       => 'progdi',
                'seq'       => 1,
                'tag'       => 'masteracademic.progdi'
            ],
            [
                'nama'      => 'Kurikulum',
                'icon'      => 'fa fa-cog',
                'url'       => 'kurikulum',
                'seq'       => 2,
                'tag'       => 'masteracademic.kurikulum'
            ],
            [
                'nama'      => 'Mata Kuliah',
                'icon'      => 'fa fa-server',
                'url'       => 'makul',
                'seq'       => 3,
                'tag'       => 'masteracademic.makul'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }

        $data = [
                'nama'      => 'Lecturer Management',
                'icon'      => 'fa fa-users',
                'url'       => '#',
                'seq'       => 4,
                'tag'       => 'lecturermanagement'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;

        $data = [
            [
                'nama'      => 'Data Dosen',
                'icon'      => 'fa fa-users',
                'url'       => 'dosen',
                'seq'       => 1,
                'tag'       => 'lecturermanagement.dosen'
            ],
            [
                'nama'      => 'Set Dosen PA',
                'icon'      => 'fa fa-users',
                'url'       => 'dosenpa',
                'seq'       => 2,
                'tag'       => 'lecturermanagement.dosenpa'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }


        $data = [
                'nama'      => 'Student Management',
                'icon'      => 'fa fa-users',
                'url'       => '#',
                'seq'       => 5,
                'tag'       => 'studentmanagement'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;

        $data = [
            [
                'nama'      => 'Data Mahasiswa',
                'icon'      => 'fa fa-users',
                'url'       => 'mahasiswa',
                'seq'       => 1,
                'tag'       => 'studentmanagement.mahasiswa'
            ],
            [
                'nama'      => 'Kartu Rencana Studi',
                'icon'      => 'fa fa-book',
                'url'       => 'krs',
                'seq'       => 2,
                'tag'       => 'studentmanagement.krs'
            ],
            [
                'nama'      => 'Cuti Mahasiswa',
                'icon'      => 'fa fa-break',
                'url'       => 'cutimhs',
                'seq'       => 3,
                'tag'       => 'studentmanagement.cutimhs'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }        

        $data = [
                'nama'      => 'Student Portal',
                'icon'      => 'fa fa-group',
                'url'       => '#',
                'seq'       => 6,
                'tag'       => 'studentportal'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;

        $data = [
            [
                'nama'      => 'Student Record',
                'icon'      => 'fa fa-users',
                'url'       => 'studentrecord',
                'seq'       => 1,
                'tag'       => 'studentportal.studentrecord'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }        

        $data = [
                'nama'      => 'Course Management',
                'icon'      => 'fa fa-course',
                'url'       => '#',
                'seq'       => 7,
                'tag'       => 'coursemanagement'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;

        $data = [
            [
                'nama'      => 'Proyek Pendidikan',
                'icon'      => 'fa fa-users',
                'url'       => 'proyekpendidikan',
                'seq'       => 1,
                'tag'       => 'coursemanagement.proyekpendidikan'
            ],
            [
                'nama'      => 'Mata Kuliah Proyek Pendidikan',
                'icon'      => 'fa fa-users',
                'url'       => 'makulpp',
                'seq'       => 2,
                'tag'       => 'coursemanagement.makulpp'
            ],
            [
                'nama'      => 'Kelas Kuliah',
                'icon'      => 'fa fa-users',
                'url'       => 'kelaskuliah',
                'seq'       => 3,
                'tag'       => 'coursemanagement.kelaskuliah'
            ],
            [
                'nama'      => 'Jadwal Kuliah',
                'icon'      => 'fa fa-users',
                'url'       => 'jadwalkuliah',
                'seq'       => 4,
                'tag'       => 'coursemanagement.jadwalkuliah'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }        


        $data = [
                'nama'      => 'Academic Management',
                'icon'      => 'fa fa-academic',
                'url'       => '#',
                'seq'       => 8,
                'tag'       => 'academicmanagement'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;

        $data = [
            [
                'nama'      => 'Penilaian',
                'icon'      => 'fa fa-users',
                'url'       => 'penilaian',
                'seq'       => 1,
                'tag'       => 'academicmanagement.penilaian'
            ],
            [
                'nama'      => 'KHS',
                'icon'      => 'fa fa-users',
                'url'       => 'khs',
                'seq'       => 2,
                'tag'       => 'academicmanagement.khs'
            ],
            [
                'nama'      => 'Cetak',
                'icon'      => 'fa fa-print',
                'url'       => 'cetak',
                'seq'       => 3,
                'tag'       => 'academicmanagement.cetak'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }

        $data = [
                'nama'      => 'University Billing',
                'icon'      => 'fa fa-money',
                'url'       => '#',
                'seq'       => 9,
                'tag'       => 'universitybilling'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;
        $data = [
            [
                'nama'      => 'Approval KRS',
                'icon'      => 'fa fa-users',
                'url'       => 'approvalkrs',
                'seq'       => 1,
                'tag'       => 'universitybilling.approvalkrs'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }       

        $data = [
                'nama'      => 'Report',
                'icon'      => 'fa fa-report',
                'url'       => '#',
                'seq'       => 10,
                'tag'       => 'report'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;

        $data = [
            [
                'nama'      => 'Report Mahasiswa',
                'icon'      => 'fa fa-users',
                'url'       => 'reportmahasiswa',
                'seq'       => 1,
                'tag'       => 'report.reportmahasiswa'
            ],
            [
                'nama'      => 'Report Dosen',
                'icon'      => 'fa fa-users',
                'url'       => 'reportdosen',
                'seq'       => 2,
                'tag'       => 'report.reportdosen'
            ],
            [
                'nama'      => 'PDDIKTI',
                'icon'      => 'fa fa-print',
                'url'       => 'pddikti',
                'seq'       => 3,
                'tag'       => 'report.pddikti'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }

        
        $data = [
                'nama'      => 'Setup',
                'icon'      => 'fa fa-gear',
                'url'       => '#',
                'seq'       => 11,
                'tag'       => 'setup'
            ];

        $id = '';

        $menu = new Menu;
        $menu->nama     = $data['nama'];
        $menu->icon     = $data['icon'];
        $menu->url      = $data['url'];
        $menu->seq      = $data['seq'];
        $menu->tag      = $data['tag'];

        $menu->save();
        $id = $menu->id;

        $data = [
            [
                'nama'      => 'Daftar Pilihan',
                'icon'      => 'fa fa-users',
                'url'       => 'daftarpilihan',
                'seq'       => 1,
                'tag'       => 'setup.daftarpilihan'
            ],
            [
                'nama'      => 'Ruangan',
                'icon'      => 'fa fa-users',
                'url'       => 'ruangan',
                'seq'       => 2,
                'tag'       => 'setup.ruangan'
            ],
            [
                'nama'      => 'Konfigurasi',
                'icon'      => 'fa fa-gear',
                'url'       => 'konfigurasi',
                'seq'       => 3,
                'tag'       => 'setup.konfigurasi'
            ]
        ];

        foreach($data as $r){
            $menu = new Menu;
            $menu->nama     = $r['nama'];
            $menu->icon     = $r['icon'];
            $menu->url      = $r['url'];
            $menu->seq      = $r['seq'];
            $menu->tag      = $r['tag'];
            $menu->menu_id  = $id;

            $menu->save();
        }

    }
}
