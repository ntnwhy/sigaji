<?php

use Illuminate\Database\Seeder;

class StatusMhsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [1, 'Aktif'],
            [2, 'Cuti'],
            [3, 'Mangkir'],
            [4, 'Lulus'],
            [5, 'Drop Out'],
        ];

        foreach ($data as $r){
            $data = new \App\Models\StatusMhs();

            $data->id   = $r[0];
            $data->nama = $r[1];

            $data->save();
        }
    }
}
