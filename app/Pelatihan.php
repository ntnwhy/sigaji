<?php

namespace App;

use App\Models\Kepanitiaan;
use App\Pelatihan;
use App\Models\Periode;
use App\Models\Pegawai;

use Illuminate\Database\Eloquent\Model;

class Pelatihan extends Model
{
    protected $hidden = [
        'is_deleted',
    ];

}