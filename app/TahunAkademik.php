<?php

namespace App;
use App\TahunAkademik;

use Illuminate\Database\Eloquent\Model;

class TahunAkademik extends Model
{

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

}
