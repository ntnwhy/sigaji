<?php

namespace App;
use App\MataKuliah;
use App\KelasMataKuliahProyekPendidikan;
use App\ProyekPendidikan;

use Illuminate\Database\Eloquent\Model;

class MataKuliahProyekPendidikan extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = "MataKuliahProyekPendidikan";


    public function detailmakul(){
	    return $this->belongsTo(MataKuliah::class, 'IdMataKuliah', 'IdMataKuliah');
    }


    public function detailkelas(){
	    return $this->belongsTo(KelasMataKuliahProyekPendidikan::class, 'IdMataKuliah', 'IdMataKuliah');
    }

    public function detailpp(){
	    return $this->hasOne(ProyekPendidikan::class, 'IdProyekPendidikan', 'IdProyekPendidikan');
    }
}
