<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];
}
