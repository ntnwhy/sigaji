<?php

namespace App\Models;

use App\Fungsi;
use Illuminate\Database\Eloquent\Model;

class TunjanganTetap extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    public function fungsi(){
        return $this->hasOne(Fungsi::class, 'id', 'ref_id_fungsi');
    }
}
