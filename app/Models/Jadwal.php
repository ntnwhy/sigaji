<?php

namespace App\Models;

use App\Lookup;
use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model{

    protected $appends = ['haris'];

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    public function getHarisAttribute(){
        $data = $this->day()->first();

        if ($data){
            return $data->item_display;
        }else{
            return '';
        }
    }

    public function pp(){
        return $this->hasOne(ProyekPendidikan::class, 'id', 'proyek_pendidikan_id');
    }

    public function ruang(){
        return $this->hasOne(Ruangan::class, 'id', 'ruangan_id');
    }

    public function day(){
        return $this->hasOne(Lookup::class, 'item_value', 'hari')
            ->where('param', 'DAY');
    }

    public function makul(){
        return $this->hasOne(Makul::class, 'id', 'makul_id');
    }

    public function makulpp(){
        return $this->hasOne(Makul_proyekpend::class, 'id', 'makul_proyekpend_id');
    }

    public function kelas_kuliah(){
        return $this->hasOne(KelasKuliah::class, 'id', 'kelas_kuliah_id');
    }

    public function dosen1(){
        return $this->hasOne(Dosen::class, 'id', 'dosen1_id');
    }

    public function dosen2(){
        return $this->hasOne(Dosen::class, 'id', 'dosen2_id');
    }

    public function dosen3(){
        return $this->hasOne(Dosen::class, 'id', 'dosen3_id');
    }

    public function dosen4(){
        return $this->hasOne(Dosen::class, 'id', 'dosen4_id');
    }
}
