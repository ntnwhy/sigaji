<?php

namespace App\Models;
use App\ProgramStudi;

use Illuminate\Database\Eloquent\Model;

class ProyekPendidikan extends Model{

    protected $connection = 'sqlsrv';
    protected $table = "ProyekPendidikan";

    public function makul(){
	    return $this->hasMany(Makul_proyekpend::class, 'IdProyekPendidikan', 'IdProyekPendidikan');
    }

        public function prodi(){
        return $this->hasOne(ProgramStudi::class, 'IdProgramStudi', 'IdProgramStudi');
    }

}
