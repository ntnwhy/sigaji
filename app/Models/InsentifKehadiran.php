<?php

namespace App\Models;
use App\Golongan;
use Illuminate\Database\Eloquent\Model;

class InsentifKehadiran extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    public function golongan(){
        return $this->hasOne(Golongan::class, 'id', 'ref_id_golongan');
    }

}
