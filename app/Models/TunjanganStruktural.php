<?php

namespace App\Models;

use App\Struktural;
use Illuminate\Database\Eloquent\Model;

class TunjanganStruktural extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    public function struktural(){
        return $this->hasOne(Struktural::class, 'id', 'ref_id_struktural');
    }
}
