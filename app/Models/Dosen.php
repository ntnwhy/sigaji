<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model {

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    protected $fillable = [
        'nik', 'nidn', 'nama'
    ];


    public function progdi(){
        return $this->hasOne(Progdi::class, 'id', 'progdi_id');
    }

    public function mahasiswa(){
        return $this->hasMany(Mahasiswa::class, 'dosen_pa_id', 'id');
    }
}
