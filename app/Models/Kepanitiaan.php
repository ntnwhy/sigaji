<?php

namespace App\Models;
use App\MasterKepanitiaan;
use App\MasterSubkepanitiaan;
use Illuminate\Database\Eloquent\Model;

class Kepanitiaan extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    public function panitia(){
        return $this->hasOne(MasterKepanitiaan::class, 'id', 'ref_id_kepanitiaan');
    }

    public function subpanitia(){
        return $this->hasOne(MasterSubkepanitiaan::class, 'id', 'ref_id_subkepanitiaan');
    }

}