<?php

namespace App\Models;
use App\MataKuliah;
use App\KelasMataKuliahProyekPendidikan;
use App\Models\ProyekPendidikan;

use Illuminate\Database\Eloquent\Model;

class Makul_proyekpend extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = "MataKuliahProyekPendidikan";

    public function detailmakul(){
	    return $this->hasOne(MataKuliah::class, 'IdMataKuliah', 'IdMataKuliah');
    }

    public function detailpp(){
	    return $this->hasOne(ProyekPendidikan::class, 'IdProyekPendidikan', 'IdProyekPendidikan');
    }

    public function detailkelas(){
	    return $this->hasMany(KelasMataKuliahProyekPendidikan::class, 'IdMataKuliahDibuka', 'IdMataKuliahDibuka');
    }
    


}
