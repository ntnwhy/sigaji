<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Makul extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    protected $appends = ['kel'];

    protected $fillable = [
        'kode', 'nama'
    ];

    public function kelompok(){
        return $this->hasOne(KelompokMakul::class, 'id', 'kelompok_makul_id');
    }

    public function kurikulum(){
        return $this->hasOne(Kurikulum::class, 'id', 'kurikulum_id');
    }

    public function getKelAttribute(){
        $data = $this->hasOne(KelompokMakul::class, 'id', 'kelompok_makul_id')->first();

        if ($data){
            return $data;
        }else{
            return "";
        }
    }

    public function mhs(){
        return $this->hasOne(Mahasiswa::class, 'semester', 'semester_berjalan');
    }
}
