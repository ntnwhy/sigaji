<?php

namespace App\Models;

use App\Pendidikan;
use Illuminate\Database\Eloquent\Model;

class TunjanganPendidikan extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    public function pendidikan(){
        return $this->hasOne(Pendidikan::class, 'id', 'ref_id_pendidikan');
    }
}
