<?php

namespace App\Models;
use App\Krs;
use App\KrsDetail;
use App\Models\Mahasiswa;


use Illuminate\Database\Eloquent\Model;

class KelasKuliah extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

	public function pp(){
        return $this->hasOne(ProyekPendidikan::class, 'id', 'proyek_pendidikan_id');
    }

    public function makulpp(){
	    return $this->hasOne(Makul_proyekpend::class, 'id', 'makul_proyekpend_id');
    }

    public function makul(){
	    return $this->hasOne(Makul::class, 'id', 'makul_id');
    }

    public function dosen1(){
	    return $this->hasOne(Dosen::class, 'id', 'dosen1_id');
    }

    public function dosen2(){
        return $this->hasOne(Dosen::class, 'id', 'dosen2_id');
    }

    public function dosen3(){
        return $this->hasOne(Dosen::class, 'id', 'dosen3_id');
    }

    public function dosen4(){
        return $this->hasOne(Dosen::class, 'id', 'dosen4_id');
    }

    public function peserta(){
        return $this->hasMany(KrsDetail::class, 'kelas_kuliahs_id', 'id');
    }    
  
}
