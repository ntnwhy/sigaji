<?php

use App\Model\GajiPokok;
use App\Model\Potongan;
namespace App\Models;
use App\DataPegawai;
use App\Golongan;
use App\Fungsi;
use App\Fungsional;
use App\Penggajian;
use App\UnitKerja;
use App\Struktural;
use App\Pendidikan;
use App\TransaksiKehadiran;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    public function datapegawai(){
        return $this->hasOne(DataPegawai::class, 'id', 'id')->orderby('nama', 'asc');
    }

    public function golongan(){
        return $this->hasOne(Golongan::class, 'id', 'ref_id_golongan');
    }

    public function jabfung(){
        return $this->hasOne(Fungsional::class, 'id', 'ref_id_jabatanfungsional');
    }

    public function struktural(){
        return $this->hasOne(Struktural::class, 'id', 'ref_id_jabatanstruktural');
    }

    public function pendidikan(){
        return $this->hasOne(Pendidikan::class, 'id', 'ref_id_pendidikan');
    }

    public function fungsi(){
        return $this->hasOne(Fungsi::class, 'id', 'ref_id_fungsi');
    }

    public function tunjtetap(){
        return $this->hasOne(TunjanganTetap::class, 'ref_id_fungsi', 'ref_id_fungsi');
    }

    public function gajipokok(){
        return $this->hasOne(GajiPokok::class, 'id', 'ref_id_gp');
    }

    public function tunjfungsional(){
        return $this->hasOne(TunjanganFungsional::class, 'ref_id_fungsional', 'ref_id_jabatanfungsional');
    }

    public function tunjpendidikan(){
        return $this->hasOne(TunjanganPendidikan::class, 'ref_id_pendidikan', 'ref_id_pendidikan');
    }

    public function tunjstruktural(){
        return $this->hasOne(TunjanganStruktural::class, 'ref_id_struktural', 'ref_id_jabatanstruktural');
    }

    public function detailslip(){
        return $this->hasMany(Penggajian::class, 'ref_id_pegawai', 'id');
    }

    public function detailgaji(){
        return $this->hasMany(Penggajian::class, 'ref_id_pegawai', 'id')
        ->where('grup_komponen', 'Gaji');
    }   

    public function detailpotongan(){
        return $this->hasMany(Penggajian::class, 'ref_id_pegawai', 'id')
        ->where('grup_komponen', 'Potongan');
    }  

    public function getkehadiran(){
        return $this->hasOne(TransaksiKehadiran::class, 'ref_id_pegawai', 'id');
    }  

    public function unit(){
        return $this->hasOne(UnitKerja::class, 'id', 'ref_id_unit');
    }  
}
