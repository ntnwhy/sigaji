<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    protected $fillable = [
        'nim', 'nama'
    ];

    public function keluarga(){
    	return $this->hasOne(MahasiswaKeluarga::class, 'mahasiswa_id', 'id');
    }

    public function pindahan(){
    	return $this->hasOne(MahasiswaPindahan::class, 'mahasiswa_id', 'id');
    }

    public function sekolahasal(){
    	return $this->hasOne(MahasiswaSekolahasal::class, 'mahasiswa_id', 'id');
    }

    public function progdi(){
        return $this->hasOne(Progdi::class, 'id', 'progdi_id');
    }

    public function status(){
        return $this->hasOne(StatusMhs::class, 'id', 'status_mhs_id');
    }

    public function pa(){
        return $this->hasOne(Dosen::class, 'id', 'dosen_pa_id');
    }
}
