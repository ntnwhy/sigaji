<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DosenMakul extends Model{

    protected $fillable = [
        'dosen_id',
        'makul_id'
    ];

    public function dosen(){
        return $this->hasOne(Dosen::class, 'id', 'dosen_id');
    }
}
