<?php

namespace App\Models;

use App\DataPegawai;
use App\Models\Pegawai;

use Illuminate\Database\Eloquent\Model;

class Potongan extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    protected $fillable = ['ref_id_pegawai'];

    public function pegawai(){
        return $this->hasOne(DataPegawai::class, 'id', 'ref_id_pegawai');
    }

}
