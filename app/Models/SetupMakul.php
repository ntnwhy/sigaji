<?php

namespace App\Models;

use App\DataPegawai;
use App\MataKuliah;
use App\TahunAkademik;
use App\Models\Pegawai;
use App\Models\ProyekPendidikan;

use Illuminate\Database\Eloquent\Model;

class SetupMakul extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];


    public function makul(){
        return $this->hasOne(MataKuliah::class, 'IdMataKuliah', 'ref_id_makul');
    }

    public function pegawai(){
        return $this->hasOne(DataPegawai::class, 'id', 'ref_id_pegawai');
    }

    public function tahunakademik(){
        return $this->hasOne(TahunAkademik::class, 'id', 'ref_id_tahunakademik');

    }

    public function proyekpendidikan(){
        return $this->hasOne(ProyekPendidikan::class, 'IdProyekPendidikan', 'ref_id_pp');

    }

}
