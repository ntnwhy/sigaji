<?php

namespace App;

use App\DataPegawai;
use App\Models\Periode;

use Illuminate\Database\Eloquent\Model;

class TransaksiLembur extends Model
{
    protected $hidden = [
        'is_deleted',
    ];

    public function pegawai(){
        return $this->hasOne(DataPegawai::class, 'id', 'ref_id_pegawai');
    }

    public function periode(){
        return $this->hasOne(Periode::class, 'id', 'ref_id_periode');
    }
}
