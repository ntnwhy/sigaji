<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiMahasiswa extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = "NilaiMahasiswa";

    public $timestamps = false;

    
}
