<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    protected $connection = 'mysql';
    protected $table = "tbl_unitkerja";
}
