<?php

namespace App;

use App\DataPegawai;
use App\Models\Periode;
use App\Models\Pegawai;

use Illuminate\Database\Eloquent\Model;

class TransaksiKehadiran extends Model
{
    protected $hidden = [
        'is_deleted',
    ];

    protected $fillable = ['ref_id_pegawai'];

    public function pegawai(){
        return $this->hasOne(Pegawai::class, 'id', 'ref_id_pegawai');
    }

    public function periode(){
        return $this->hasOne(Periode::class, 'id', 'ref_id_periode');
    }
}
