<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 14/10/2018
 * Time: 15:09
 */

namespace App\Http\Controllers\Userman;


use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Pengguna';

        return $this->view('userman.users.view', $data);
    }

    public function show(Request $request){
        $data = User::query();

        $data->with('role');

        return $this->dataTables($data->get());
    }

    public function form(){
        $data['title']  = 'Form Pengguna';
        $data['role']   = Role::query()->where('su', false)->get();

        return $this->view('userman.users.form', $data);
    }

    public function edit($id){
        $data['title']  = 'Reset Password Pengguna';
        $data['user']   = User::findOrFail($id);
        $data['link']   = url('users').'/'.$id;

        return $this->view('userman.users.reset', $data);
    }

    public function store(Request $request){
        $data = new User();

        $data->name     = $request->input('nama');
        $data->email    = $request->input('email');
        $data->password = Hash::make($request->input('email'));
        $data->role_id  = $request->input('role');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = User::findOrFail($id);

        $data->password   = Hash::make($request->input('password'));

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = User::findOrFail($id);

        $data->delete();

        return $this->json(true, 'Hapus data berhasil !');
    }

}