<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 11/10/2018
 * Time: 19:00
 */

namespace App\Http\Controllers\Userman;


use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Menu';

        return $this->view('userman.menu.view', $data);
    }

    public function show(Request $request){
        $data = Menu::query();

        return $this->dataTables($data->get());
    }

    public function form($id = null){
        $data['title']  = 'Form Menu';

        $form['link']    = url('menu');
        $form['nama']   = '';
        $form['icon']   = '';
        $form['url']    = '';
        $form['menu']   = '';
        $form['seq']    = '';
        $form['status'] = true;
        $form['tag']    = '';

        if ($id){
            $menu = Menu::findOrFail($id);

            $form['link']    = url('menu').'/'.$id;

            $form['nama']   = $menu->nama;
            $form['icon']   = $menu->icon;
            $form['url']    = $menu->url;
            $form['menu']   = $menu->menu_id;
            $form['seq']    = $menu->seq;
            $form['status'] = $menu->status;
            $form['tag']    = $menu->tag;
        }

        $data['form']   = $form;
        $data['menu']   = Menu::query()->where('status', true)->whereNull('menu_id')->get();

        return $this->view('userman.menu.form', $data);
    }

    public function store(Request $request){
        $data = new Menu();

        $data->nama     = $request->input('nama');
        $data->icon     = $request->input('icon');
        $data->url      = $request->input('url');
        $data->menu_id  = $request->input('menu');
        $data->status   = $request->input('status');
        $data->tag      = $request->input('tag');
        $data->seq      = $request->input('seq');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Menu::findOrFail($id);

        $data->nama     = $request->input('nama');
        $data->icon     = $request->input('icon');
        $data->url      = $request->input('url');
        $data->menu_id  = $request->input('menu');
        $data->status   = $request->input('status');
        $data->tag      = $request->input('tag');
        $data->seq      = $request->input('seq');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = Menu::findOrFail($id);

        $data->delete();

        return $this->json(true, 'Hapus data berhasil !');
    }
}