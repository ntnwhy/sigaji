<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 14/10/2018
 * Time: 14:56
 */

namespace App\Http\Controllers\Userman;


use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Role Pengguna';

        return $this->view('userman.role.view', $data);
    }

    public function show(Request $request){
        $data = Role::query();

        $data->where('su', false);

        return $this->dataTables($data->get());
    }

    public function form($id = null){
        $data['title']  = 'Form Role Pengguna';

        $form['link']    = url('roles');
        $form['nama']   = '';
        $form['status'] = true;

        if ($id){
            $menu = Role::findOrFail($id);

            $form['link']    = url('roles').'/'.$id;

            $form['nama']   = $menu->nama;
            $form['status'] = $menu->status;
        }

        $data['form']   = $form;

        return $this->view('userman.role.form', $data);
    }

    public function store(Request $request){
        $data = new Role();

        $data->nama     = $request->input('nama');
        $data->status   = $request->input('status');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Role::findOrFail($id);

        $data->nama     = $request->input('nama');
        $data->status   = $request->input('status');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = Role::findOrFail($id);

        $data->delete();

        return $this->json(true, 'Hapus data berhasil !');
    }
}