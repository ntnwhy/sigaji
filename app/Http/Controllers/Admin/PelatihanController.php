<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\DataPegawai;
use App\Pelatihan;
use App\MasterKepanitiaan;
use App\Models\Periode;
use App\Http\Controllers\Controller;
use App\Models\Kepanitiaan;

use Illuminate\Http\Request;

class PelatihanController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Pelatihan';

        return $this->view('admin.pelatihan.view', $data);
    }

    public function show(Request $request){

        $user = $this->getUsers()->name;

        $data = Pelatihan::query(); 
        $data->where($this->isDeleted, false);

        return $this->dataTables($data->get());

    }

    public function form($id = null){

        $user = $this->getUsers()->name;

        $form['url']                = url('pelatihan');
        
        $form['nama']               = '';
        $form['tanggal']            = '';
        $form['nik']                = '';
        $form['jk']                 = '';
        $form['alamat']             = '';
        $form['jenis']              = '';


        if ($id){

            $res = Pelatihan::findOrFail($id);

            $form['url']                = url('pelatihan').'/'.$res->id;
           
            $form['nama']               = $res->nama;
            // $form['tanggal']            = $res->tanggalmulai.' - '.$res->tanggalselesai;
            $form['nik']                = $res->nik;
            // $form['jk']                 = $res->jeniskelamin;
            $form['alamat']             = $res->alamat;
            $form['jenis']              = $res->jenis_pelatihan;
    
        }

        $data['title']  = 'Form Pelatihan';
        $data['form']   = $form;
        return $this->view('admin.pelatihan.form', $data);
    }

    public function store(Request $request){

        $user = $this->getUsers()->name;
        $data = new Pelatihan();


        $str = explode("-", $request->input('tanggal'));

        $data->nama               = $request->input('nama');
        $data->tanggalmulai       = $str[0];
        $data->tanggalselesai     = $str[1];
        $data->nik                = $request->input('nik');
        $data->jeniskelamin       = $request->input('jk');
        $data->alamat             = $request->input('alamat');
        $data->jenis_pelatihan    = $request->input('jenis');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){

        $data = Pelatihan::findOrFail($id);

        $str = explode("-", $request->input('tanggal'));

        $data->nama               = $request->input('nama');
        $data->tanggalmulai       = $str[0];
        $data->tanggalselesai     = $str[1];
        $data->nik                = $request->input('nik');
        $data->jeniskelamin       = $request->input('jk');
        $data->alamat             = $request->input('alamat');
        $data->jenis_pelatihan    = $request->input('jenis');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        
        $data = Pelatihan::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil');
    }



}