<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Pendidikan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TunjanganPendidikan;

class TunjanganPendidikanController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Master Tunjangan Pendidikan';

        return $this->view('admin.tunjanganpendidikan.view', $data);
    }

    public function show(Request $request){
        $data = TunjanganPendidikan::query();
        $data->where($this->isDeleted, false);
        $data->with('pendidikan');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['pendidikan']   = Pendidikan::query()->get();


        $form['url']                = url('tunjanganpendidikan');
        $form['pendidikan']         = '';
        $form['nominal']            = '';

        if ($id){
            $res = TunjanganPendidikan::findOrFail($id);

            $form['url']                = url('tunjanganpendidikan').'/'.$res->id;
            $form['pendidikan']             = $res->ref_id_pendidikan;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Tunjangan Pendidikan';
        $data['form']   = $form;
        return $this->view('admin.tunjanganpendidikan.form', $data);
    }

    public function store(Request $request){
        $data = new TunjanganPendidikan();

        $data->ref_id_pendidikan        = $request->input('pendidikan');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = TunjanganPendidikan::findOrFail($id);

        $data->ref_id_pendidikan        = $request->input('pendidikan');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = TunjanganPendidikan::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}