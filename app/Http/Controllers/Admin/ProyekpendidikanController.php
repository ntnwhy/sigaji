<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 07/10/2018
 * Time: 23:57
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Proyekpendidikan;
use App\Lookup;
use App\Models\Progdi;
use App\Models\Kurikulum;
use Illuminate\Http\Request;

class ProyekpendidikanController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Proyek Pendidikan';

        return $this->view('admin.proyekpendidikan.view', $data);
    }

    public function show(Request $request){
        $data = Proyekpendidikan::query();

        $data->with('progdi')
            ->with('kurikulum');

        $data->where($this->isDeleted, false);

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['semester']   = Lookup::where('param', 'SEMESTER_PARTIAL')->get();
        $data['progdi']     = Progdi::where($this->isDeleted, false)->get();
        $data['kurikulum']  = Kurikulum::where($this->isDeleted, false)->get();

        $form['url']                = url('proyekpendidikan');
        $form['kode']               = '';
        $form['kurikulum_id']       = '';
        $form['progdi_id']          = '';
        $form['tahun']              = '';
        $form['semester']           = '';
        $form['status']             = true;

        if ($id){
            $proyekpendidikan = Proyekpendidikan::findOrFail($id);

            $form['url']                = url('proyekpendidikan').'/'.$proyekpendidikan->id;
            $form['kode']               = $proyekpendidikan->kode;
            $form['kurikulum_id']       = $proyekpendidikan->kurikulum_id;
            $form['progdi_id']          = $proyekpendidikan->progdi_id;
            $form['tahun']              = $proyekpendidikan->tahun;
            $form['semester']           = $proyekpendidikan->semester;
            $form['status']             = $proyekpendidikan->status;
        }

        $data['title']  = 'Form Proyek Pendidikan';
        $data['form']   = $form;
        return $this->view('admin.proyekpendidikan.form', $data);
    }

    public function store(Request $request){
        $data = new Proyekpendidikan();

        $data->kode             = $request->input('kode');
        $data->kurikulum_id     = $request->input('kurikulum_id');
        $data->progdi_id        = $request->input('progdi_id');
        $data->tahun            = $request->input('tahun');
        $data->semester         = $request->input('semester');
        $data->status           = $request->input('status');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Proyekpendidikan::findOrFail($id);

        $data->kode             = $request->input('kode');
        $data->kurikulum_id     = $request->input('kurikulum_id');
        $data->progdi_id        = $request->input('progdi_id');
        $data->tahun            = $request->input('tahun');
        $data->semester         = $request->input('semester');
        $data->status           = $request->input('status');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = Proyekpendidikan::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}