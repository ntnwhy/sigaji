<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 07/10/2018
 * Time: 23:57
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Proyekpendidikan;
use App\Models\Makul_proyekpend;
use App\Lookup;
use App\Models\Progdi;
use App\Models\Kurikulum;
use App\Models\Makul;
use Illuminate\Http\Request;

class MakulppController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Mata Kuliah Pendidikan';

        return $this->view('admin.makulpp.view', $data);
    }

    public function show(Request $request){
        $data = Makul_proyekpend::query();

        $data->where($this->isDeleted, false);

        $data->with('makul')
            ->with('proyekpendidikan')
            ->with('kurikulum');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['kurikulum']  = Kurikulum::where($this->isDeleted, false)->get();
        $data['pp']         = Proyekpendidikan::where($this->isDeleted, false)->get();
        $data['makul']      = Makul::where($this->isDeleted, false)->get();

        $form['url']                    = url('makulpp');
        $form['kurikulum_id']           = '';
        $form['proyek_pendidikan_id']   = '';
        $form['makul_id']               = '';
        $form['sks_teori']              = '';
        $form['sks_praktek']            = '';
        $form['sks_total']              = '';

        if ($id){
            $makulpp = Makul_proyekpend::findOrFail($id);

            $form['url']                    = url('makulpp').'/'.$makulpp->id;
            $form['kurikulum_id']           = $makulpp->kurikulum_id;
            $form['proyek_pendidikan_id']   = $makulpp->proyek_pendidikan_id;
            $form['makul_id']               = $makulpp->makul_id;
            $form['sks_teori']              = $makulpp->sks_teori;
            $form['sks_praktek']            = $makulpp->sks_praktek;
            $form['sks_total']              = $makulpp->sks_total;
        }

        $data['title']  = 'Form Mata Kuliah Proyek Pendidikan';
        $data['form']   = $form;
        return $this->view('admin.makulpp.form', $data);
    }

    public function store(Request $request){
        $data = new Makul_proyekpend();

        $data->kurikulum_id         = $request->input('kurikulum_id');
        $data->proyek_pendidikan_id = $request->input('proyek_pendidikan_id');
        $data->makul_id             = $request->input('makul_id');
        $data->sks_teori            = $request->input('sks_teori');
        $data->sks_praktek          = $request->input('sks_praktek');
        $data->sks_total            = $request->input('sks_total');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Makul_proyekpend::findOrFail($id);

        $data->kurikulum_id         = $request->input('kurikulum_id');
        $data->proyek_pendidikan_id = $request->input('proyek_pendidikan_id');
        $data->makul_id             = $request->input('makul_id');
        $data->sks_teori            = $request->input('sks_teori');
        $data->sks_praktek          = $request->input('sks_praktek');
        $data->sks_total            = $request->input('sks_total');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = Makul_proyekpend::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }

    public function makul($id){
        $data = Makul::findOrFail($id);

        return $this->json(true, 'Data Mata Kuliah', $data);
    }
}