<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\MasterKepanitiaan;
use App\MasterSubkepanitiaan;

use App\Http\Controllers\Controller;
use App\Models\Kepanitiaan;

use Illuminate\Http\Request;
use App\MataKuliah;

class ProgramStudiController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Mata Kuliah';

        return $this->view('admin.kepanitiaan.view', $data);
    }

    public function show(Request $request){
        $data = MataKuliah::query();

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['panitia']            = MasterKepanitiaan::query()->get();
        $data['jabatanpanitia']     = Lookup::query()->where('param', 'JABATAN_PANITIA')->get();

        $form['url']                = url('kepanitiaan');
        
        $form['panitia']            = '';
        $form['subpanitia']         = '';
        $form['jabatanpanitia']     = '0';
        $form['nominal']            = '';

        if ($id){
            $res = Kepanitiaan::findOrFail($id);

            $form['url']                = url('kepanitiaan').'/'.$res->id;
           
            $form['panitia']            = $res->ref_id_kepanitiaan;
            $form['subpanitia']         = $res->ref_id_subkepanitiaan;
            $form['jabatanpanitia']     = $res->jabatan_kepanitiaan;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Kepanitiaan';
        $data['form']   = $form;
        return $this->view('admin.kepanitiaan.form', $data);
    }

    public function store(Request $request){
        $data = new Kepanitiaan();

        $data->ref_id_kepanitiaan       = $request->input('panitia');
        $data->ref_id_subkepanitiaan    = $request->input('subpanitia');
        $data->jabatan_kepanitiaan      = $request->input('jabatanpanitia');
        $data->nominal                  = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){

        $data = Kepanitiaan::findOrFail($id);

        $data->ref_id_kepanitiaan       = $request->input('panitia');
        $data->ref_id_subkepanitiaan    = $request->input('subpanitia');
        $data->jabatan_kepanitiaan      = $request->input('jabatanpanitia');
        $data->nominal                  = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        
        $data = Kepanitiaan::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }

    public function sub($id){
        $data = MasterSubkepanitiaan::where('ref_id_kepanitiaan', $id)->get();

        return $this->json(true, 'Data Sub Kepanitiaan', $data);
    }
}