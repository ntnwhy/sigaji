<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\DataPegawai;
use App\TransaksiKepanitiaan;
use App\MasterKepanitiaan;
use App\MasterSubkepanitiaan;
use App\Models\Periode;
use App\Http\Controllers\Controller;
use App\Models\Kepanitiaan;

use Illuminate\Http\Request;

class TransaksiKepanitiaanController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']      = 'Transaksi Kepanitiaan';
        $data['periode']    = Periode::query()->orderBy('id', 'desc')->get();

        return $this->view('admin.transaksikepanitiaan.view', $data);
    }

    public function show(Request $request){

        $user = $this->getUsers()->name;

        $data = TransaksiKepanitiaan::query(); 
        $data->with('pegawai');
        $data->with('periode');
        $data->with('panitia.panitia');
        $data->with('panitia.subpanitia');
        $data->where('created_by', $user);

        if($request->query('periode')){
            
            $data->where('ref_id_periode', $request->query('periode')); 

            if($request->query('cek') == "true"){

                $data->orWhere('status', 0);
            }

        }

        // if($request->query('cek') == 1){

        //     $data->orWhere('status', 0);
        // } 

        $data->where($this->isDeleted, false);

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $user = $this->getUsers()->name;

        $data['panitia']            = Kepanitiaan::query()->with('panitia')->with('subpanitia')->get();
        $data['periode']            = Periode::query()->where($this->isDeleted, false)->where('status', '1')->get();

        $data['pegawai']            = DataPegawai::query()->get();

        $form['url']                = url('transaksikepanitiaan');
        
        $form['panitia']            = '';
        $form['pegawai']            = '';
        $form['nominal']            = '';
        $form['nominalu']           = '';

        if ($id){
            $res = TransaksiKepanitiaan::findOrFail($id);

            $form['url']                = url('transaksikepanitiaan').'/'.$res->id;
           
            $form['pegawai']            = $res->ref_id_pegawai;
            $form['panitia']            = $res->ref_id_panitia;
            $form['nominal']            = $res->nominal;
            $form['nominalu']           = $res->nominal_usul;
    
        }

        $data['title']  = 'Form Transaksi Kepanitiaan';
        $data['form']   = $form;
        return $this->view('admin.transaksikepanitiaan.form', $data);
    }

    public function store(Request $request){

        $user = $this->getUsers()->name;
        $data = new TransaksiKepanitiaan();

        $data->ref_id_pegawai    = $request->input('pegawai');
        $data->ref_id_panitia    = $request->input('panitia');
        $data->ref_id_periode    = $request->input('periode');
        $data->nominal           = $request->input('nominal');
        $data->nominal_usul      = $request->input('nominalu');
        $data->created_by        = $user;
        $data->save();

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){

        $user = $this->getUsers()->name;
        $data = TransaksiKepanitiaan::findOrFail($id);

        $data->ref_id_pegawai    = $request->input('pegawai');
        $data->ref_id_panitia    = $request->input('panitia');
        $data->ref_id_periode    = $request->input('periode');
        $data->nominal           = $request->input('nominal');
        $data->nominal_usul      = $request->input('nominalu');
        $data->updated_by        = $user;

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        
        $data = TransaksiKepanitiaan::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}