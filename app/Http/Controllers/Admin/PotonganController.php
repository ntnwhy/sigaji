<?php

namespace App\Http\Controllers\Admin;
use App\DataPegawai;
use App\Lookup;
use App\Models\Potongan;
use App\Exports\PotonganExport;
use App\Import\PotonganImport;
use App\Models\Pegawai;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PotonganController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Potongan';
        $data['potongan']           = Lookup::where('param', 'POTONGAN')->get();

        return $this->view('admin.potongan.view', $data);
    }

    public function show(Request $request){
        $data = Potongan::query();
        $data->with('pegawai');
        $data->where($this->isDeleted, false);

        if($request->input('potongan')){
            $data->where('potongan', $request->input('potongan'));
        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['potongan']           = Lookup::where('param', 'POTONGAN')->get();
        $data['pegawai']            = DataPegawai::query()->get();

        $form['url']                = url('potongan');
        $form['pegawai']            = '';
        $form['potongan']           = '';
        $form['nominal']            = '';

        if ($id){
            $res = Potongan::findOrFail($id);

            $form['url']                = url('potongan').'/'.$res->id;
            $form['pegawai']            = $res->ref_id_pegawai;
            $form['potongan']           = $res->potongan;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Potongan';
        $data['form']   = $form;
        return $this->view('admin.potongan.form', $data);
    }

    public function export(Request $request){

        $nama = 'TemplatePotongan_'.$request->query('potongan').'.xlsx';

        $nama = str_replace(" ", "", $nama);

        return Excel::download(new PotonganExport($request->query('potongan')),
            $nama);
    }

    public function store(Request $request){
        $data = new Potongan();

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->potongan             = $request->input('potongan');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Potongan::findOrFail($id);

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->potongan             = $request->input('potongan');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = Potongan::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil');
    }

    public function import(Request $request){

        Excel::import(new PotonganImport, $request->file('file'));

        return $this->json(true, 'Import data Potongan berhasil !');
    }
}