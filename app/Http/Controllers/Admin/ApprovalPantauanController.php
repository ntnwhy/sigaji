<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\DataPegawai;
use App\Penggajian;
use App\PantauanPengajaran;
use App\MasterKepanitiaan;
use App\Models\Pegawai;
use App\Models\Periode;
use App\Http\Controllers\Controller;
use App\Models\Kepanitiaan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApprovalPantauanController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title']          = 'Approval Pantauan Pengajaran';
        $data['periode']        = Periode::query()->get();
        $data['pegawai']        = Pegawai::query()->with('datapegawai')->where('jabatan','Dosen Tetap')->orWhere('jabatan', 'Dosen Tidak Tetap')->orderby('id','asc')->get();

        return $this->view('admin.approvalpantauan.view', $data);
        
    }

    public function show(Request $request){

        $user       = $this->getUsers()->name;        
        $userrole   = $this->getUsers()->role_id;  
      
        $data = PantauanPengajaran::query(); 

        if ($request->query('periode')){
            $data->where('ref_id_periode', $request->query('periode'));
        }

        $data->with('periode');
        $data->with('pegawai');
        $data->with('makul');
                   
        $data->where($this->isDeleted, false);

        return $this->dataTables($data->get());
    }

    public function approve($id){

        $user = $this->getUsers()->name;

        $data = PantauanPengajaran::findOrFail($id);

        $data->status              = '1';
        $data->approved_by         = $user;
        $data->approved_at         = date('Y-m-d H:i:s');

        $data->save();

        return $this->json(true, 'Approval data berhasil !');
    }

    public function approvemasal($idperiode, $idpegawai){

        $user = $this->getUsers()->name;

        $data = PantauanPengajaran::where('ref_id_periode', $idperiode)->where('is_deleted', false);

        if ($idpegawai){
            $data->where('ref_id_pegawai', $idpegawai);
        }

        $data->update(['status' => 1]);
        $data->update(['approved_by' => $user]);
        $data->update(['approved_at' => date('Y-m-d H:i:s')]);

        return $this->json(true, 'Approval data Masal berhasil !');
    }

    public function update(Request $request, $id){

        $user = $this->getUsers()->name;
        $id = $request->input('id');

        $data = PantauanPengajaran::findOrFail($id);

        $data->durasi             = $request->input('durasi');
        $data->updated_by         = $user;
        $data->updated_at         = date('Y-m-d H:i:s');
        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

}