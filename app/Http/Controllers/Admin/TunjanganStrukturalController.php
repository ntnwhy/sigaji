<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Struktural;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TunjanganStruktural;

class TunjanganStrukturalController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Master Tunjangan Struktural';

        return $this->view('admin.tunjanganstruktural.view', $data);
    }

    public function show(Request $request){
        $data = TunjanganStruktural::query();
        $data->where($this->isDeleted, false);
        $data->with('struktural');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['struktural']   = Struktural::query()->get();


        $form['url']                = url('tunjanganstruktural');
        $form['struktural']         = '';
        $form['nominal']            = '';

        if ($id){
            $res = TunjanganStruktural::findOrFail($id);

            $form['url']                = url('tunjanganstruktural').'/'.$res->id;
            $form['struktural']         = $res->ref_id_struktural;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Tunjangan Struktural';
        $data['form']   = $form;
        return $this->view('admin.tunjanganstruktural.form', $data);
    }

    public function store(Request $request){
        $data = new TunjanganStruktural();

        $data->ref_id_struktural    = $request->input('struktural');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = TunjanganStruktural::findOrFail($id);

        $data->ref_id_struktural    = $request->input('struktural');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = TunjanganStruktural::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}