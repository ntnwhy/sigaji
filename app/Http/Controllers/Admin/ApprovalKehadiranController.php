<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\Penggajian;
use App\DataPegawai;
use App\TransaksiKehadiran;
use App\MasterKepanitiaan;
use App\Models\Periode;
use App\Http\Controllers\Controller;
use App\Models\Kepanitiaan;

use Illuminate\Http\Request;

class ApprovalKehadiranController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Approval Kehadiran';
        $data['periode']    = Periode::query()->get();

        return $this->view('admin.approvalkehadiran.view', $data);
    }

    public function show(Request $request){
      
        $data = TransaksiKehadiran::query(); 
        $data->with('pegawai.gajipokok.insentifkehadiran');
        $data->with('pegawai.datapegawai');
        $data->with('periode');
        $data->where($this->isDeleted, false);

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $user = $this->getUsers()->name;

        $data['periode']            = Periode::query()->where($this->isDeleted, false)->where('status', '1')->get();

        $data['pegawai']            = DataPegawai::query()->get();


        $form['url']                = url('transaksikehadiran');
        
        $form['pegawai']            = '';
        $form['jam_hadir']          = '';
        $form['hari_hadir']         = '';
        $form['persentase']         = '';

        if ($id){
            $res = TransaksiKehadiran::findOrFail($id);

            $form['url']                = url('transaksikehadiran').'/'.$res->id;
           
            $form['pegawai']            = $res->ref_id_pegawai;
            $form['jam_hadir']          = $res->jumlah_jam_hadir;
            $form['hari_hadir']         = $res->jumlah_hari_hadir;
            $form['persentase']         = $res->persentase_kehadiran;
    
        }

        $data['title']  = 'Form Transaksi Kehadiran';
        $data['form']   = $form;
        return $this->view('admin.transaksikehadiran.form', $data);
    }

    public function store(Request $request){

        $user = $this->getUsers()->name;
        $data = new TransaksiKehadiran();

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->ref_id_periode       = $request->input('periode');
        $data->jumlah_hari_kehadiran= $request->input('hari_hadir');
        $data->jumlah_jam_kehadiran = $request->input('jam_hadir');
        $data->persentase_kehadiran = $request->input('persentase');
        $data->created_by           = $user;
        $data->save();

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function delete($id){

        $data = new TransaksiKehadiran();

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->ref_id_periode       = $request->input('periode');
        $data->jumlah_hari_kehadiran= $request->input('hari_hadir');
        $data->jumlah_jam_kehadiran = $request->input('jam_hadir');
        $data->persentase_kehadiran = $request->input('persentase');
        $data->created_by           = $user;
        $data->save();

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function approve($id){
        $data = TransaksiKehadiran::findOrFail($id);

        $data->status              = '1';

        $data->save();

        // insentif kehadiran
        

        $nominal    = $data->pegawai->gajipokok->insentifkehadiran->nominal;
        $persentase = $data->persentase_kehadiran; 
        $hari       = $data->jumlah_hari_kehadiran; 


        if($persentase == 100){

            $datas = new Penggajian();

            $na         = $nominal * $persentase / 100 ;

            $datas->ref_id_pegawai       = $data->ref_id_pegawai;
            $datas->ref_id_periode       = $data->ref_id_periode;
            $datas->grup_komponen        = "Gaji";
            $datas->komponen             = "Insentif Kehadiran";
            $datas->nominal              = $na;
            $datas->ket                  = $persentase.' %';
            $datas->save();

        }

        if($data->pegawai->jabatan == 'Pegawai Tetap'){

            $datad = new Penggajian();

            $na         = $hari * 10000 * $persentase / 100 ;          

            $datad->ref_id_pegawai       = $data->ref_id_pegawai;
            $datad->ref_id_periode       = $data->ref_id_periode;
            $datad->grup_komponen        = "Gaji";
            $datad->komponen             = "Tunjangan Kehadiran";
            $datad->nominal              = $na;
            $datad->ket                  = $persentase.' %';
            $datad->save();
            
        }




        return $this->json(true, 'Approval data berhasil !');
    }

    public function destroy($id){


        if (is_array($id)) 
            {
                $data = TransaksiKehadiran::destroy($id);
                $this->remove($data);

                return $this->json(true, 'Hapus data berhasil @');
            }
        else
            {
                $data = TransaksiKehadiran::findOrFail($id)->delete();
                $this->remove($data);

                return $this->json(true, 'Hapus data barengberhasil @');
            }


        
        // $data = TransaksiKehadiran::findOrFail($id);

        // $this->remove($data);

        // return $this->json(true, 'Hapus data berhasil @');
    }

}