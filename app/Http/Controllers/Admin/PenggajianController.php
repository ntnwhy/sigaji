<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

use Akaunting\Money\Currency;
use Akaunting\Money\Money;

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\Penggajian;
use App\TransaksiKehadiran;
use App\MasterKepanitiaan;
use App\Models\Periode;
use App\Http\Controllers\Controller;
use App\Models\Kepanitiaan;
use App\Models\Pegawai;
use App\Models\Potongan;

use Illuminate\Http\Request;

class PenggajianController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']      = 'Penggajian';
        $data['periode']    = Periode::query()->get();

        return $this->view('admin.penggajian.view', $data);
    }

    public function show(Request $request){

        $data = Penggajian::selectRaw('ref_id_pegawai, ref_id_periode, sum(nominal) AS gaji')->groupby('ref_id_pegawai', 'ref_id_periode')->with('datapegawai');


          return $this->dataTables($data->get());
    }

    public function showdetail($id){

        $key = explode(".", $id);
        $periode = $key[0];
        $pegawai = $key[1];


        $data = Penggajian::query();
        $data->where('ref_id_periode', $periode)
            ->where('ref_id_pegawai', $pegawai);


          return $this->dataTables($data->get());
    }


    public function showslip(Request $request){

        $data = Pegawai::query();
        $data->with('datapegawai')
            ->with('detailgaji');

        // if ($request->query('smtkhs')){
        //     $data->where('semester_berjalan', $request->query('smtkhs'));
        // }

        // if ($request->query('pp')){
        //     $data->where('proyek_pendidikan_id', $request->query('pp'));
        // }

        return $this->dataTables($data->get());
    }

    public function slip($id = null){

        $data['title']  = 'Cetak';

        $cetak = Pegawai::query();

        $cetak->with('datapegawai')
            ->with('detailgaji');

            $data['cetak'] = $cetak->get();

            return $this->view('admin.cetak.slip', $data);
    }

    public function laporan($id = null){

        $data['title']  = 'Cetak Laporan';

        $cetak = Pegawai::query();

        $cetak->with('datapegawai')
            ->with('detailpotongan')
            ->with('detailslip');

            $data['cetak'] = $cetak->get();

            return $this->view('admin.cetak.laporan', $data);
    }


    public function generate(){

        $data = Pegawai::query()
        ->with('datapegawai')
        ->with('detailgaji')
        ->with('detailpotongan');

        return $this->dataTables($data->get());


    }


    public function generategaji($periode){

        $data = Pegawai::query()
        ->with('datapegawai')
        ->with('golongan')
        ->with('jabfung')
        ->with('struktural')
        ->with('pendidikan')
        ->with('fungsi')
        ->with('tunjtetap')
        ->with('gajipokok')
        ->with('tunjfungsional.fungsional')
        ->with('tunjpendidikan.pendidikan')
        ->with('tunjstruktural')->get();

        foreach ($data as $p) {

            $jabatan    = $p->jabatan;
            $idpegawai  = $p->ref_id_pegawai;


            // potongan

            $data = Potongan::query()
            ->where('is_deleted', false)
            ->where('ref_id_pegawai', $idpegawai)->get();

            foreach ($data as $pot) {

                $data = new Penggajian();

                $data->ref_id_pegawai       = $pot->ref_id_pegawai;
                $data->ref_id_periode       = $periode;
                $data->grup_komponen        = "Potongan";
                $data->komponen             = $pot->potongan;
                $data->nominal              = $pot->nominal;
                $data->ket                  = '';
                $data->save();
                
            }

        // pegawai dan dosen
                if($jabatan == 'Pegawai Tetap' OR $jabatan == 'Dosen Tetap'){

                    // gajipokok

                    if($p->gajipokok){

                        $data = new Penggajian();

                        $data->ref_id_pegawai       = $p->ref_id_pegawai;
                        $data->ref_id_periode       = $periode;
                        $data->grup_komponen        = "Gaji";
                        $data->komponen             = "Gaji Pokok";
                        $data->nominal              = $p->gajipokok->nominal;
                        $data->ket                  = '';
                        $data->save();

                    }

                    // tunjangan pendidikan

                    if($p->tunjpendidikan){

                        $data = new Penggajian();

                        $data->ref_id_pegawai       = $p->ref_id_pegawai;
                        $data->ref_id_periode       = $periode;
                        $data->grup_komponen        = "Gaji";
                        $data->komponen             = "Tunjangan Pendidikan";
                        $data->nominal              = $p->tunjpendidikan->nominal;
                        $data->ket                  = $p->tunjpendidikan->pendidikan->kode;
                        $data->save();
                    }

                    // tunjangan struktural 

                    if($p->tunjstruktural){

                        $data = new Penggajian();

                        $data->ref_id_pegawai       = $p->ref_id_pegawai;
                        $data->ref_id_periode       = $periode;
                        $data->grup_komponen        = "Gaji";
                        $data->komponen             = "Tunjangan Struktural";
                        $data->nominal              = $p->tunjstruktural->nominal;
                        $data->ket                  = '';
                        $data->save();
                    }

                }

        //pegawai

                if($jabatan == 'Pegawai Tetap'){

            // tunj tetap

                    if($p->tunjtetap){

                        $data = new Penggajian();

                        $data->ref_id_pegawai       = $p->ref_id_pegawai;
                        $data->ref_id_periode       = $periode;
                        $data->grup_komponen        = "Gaji";
                        $data->komponen             = "Tunjangan Tetap";
                        $data->nominal              = $p->tunjtetap->nominal;
                        $data->ket                  = '';
                        $data->save();

                    }
                }

                // dosen
                if($jabatan == 'Dosen Tetap'){
                                     
                   // jabatan fungsional

                    if($p->tunjfungsional){

                        $data = new Penggajian();

                        $data->ref_id_pegawai       = $p->ref_id_pegawai;
                        $data->ref_id_periode       = $periode;
                        $data->grup_komponen        = "Gaji";
                        $data->komponen             = "Tunjangan Fungsional";
                        $data->nominal              = $p->tunjfungsional->nominal;
                        $data->ket                  = '';
                        $data->save();

                    }
               
                } 

        }

        return $this->json(true, 'Proses Generete Gaji berhasil !');

    }


}