<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;

use App\Golongan;
use App\Http\Controllers\Controller;
use App\Models\InsentifKehadiran;
use Illuminate\Http\Request;

class InsentifKehadiranController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Master Insentif Kehadiran';

        return $this->view('admin.insentifkehadiran.view', $data);
    }

    public function show(Request $request){
        $data = InsentifKehadiran::query(); 
        $data->with('golongan');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['golongan']   = Golongan::query()->get();

        $form['url']                = url('insentifkehadiran');
        $form['golongan']           = '';
        $form['nominal']            = '';

        if ($id){
            $res = InsentifKehadiran::findOrFail($id);

            $form['url']                = url('insentifkehadiran').'/'.$res->id;
            $form['golongan']           = $res->ref_id_golongan;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Insentif Kehadiran Pokok';
        $data['form']   = $form;
        return $this->view('admin.insentifkehadiran.form', $data);
    }

    public function store(Request $request){
        $data = new InsentifKehadiran();

        $data->ref_id_golongan      = $request->input('golongan');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = InsentifKehadiran::findOrFail($id);

        $data->ref_id_golongan      = $request->input('golongan');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = InsentifKehadiran::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}