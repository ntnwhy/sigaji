<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Golongan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GolonganController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Master Gaji Pokok';

        return $this->view('admin.gajipokok.view', $data);
    }

    public function show(Request $request){
        $data = Golongan::query();       

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['golongan']   = MasterGolongan::query()->get();

        $form['url']                = url('gajipokok');
        $form['golongan']           = '';
        $form['masakerja']          = '';
        $form['nominal']            = '';

        if ($id){
            $res = GajiPokok::findOrFail($id);

            $form['url']                = url('gajipokok').'/'.$res->id;
            $form['golongan']           = $res->ref_id_golongan;
            $form['masakerja']          = $res->masa_kerja;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Gaji Pokok';
        $data['form']   = $form;
        return $this->view('admin.gajipokok.form', $data);
    }

    public function store(Request $request){
        $data = new GajiPokok();

        $data->ref_id_golongan      = $request->input('golongan');
        $data->masa_kerja           = $request->input('masakerja');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = GajiPokok::findOrFail($id);

        $data->ref_id_golongan      = $request->input('golongan');
        $data->masa_kerja           = $request->input('masakerja');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = GajiPokok::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}