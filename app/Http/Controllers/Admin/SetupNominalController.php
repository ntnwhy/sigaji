<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\SetupNominal;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SetupNominalController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Setup Nominal';

        return $this->view('admin.setupnominal.view', $data);
    }

    public function show(Request $request){
        $data = SetupNominal::query()->get();       

        return $this->dataTables($data);
    }

    public function form($id = null){

        // $data['golongan']   = MasterGolongan::query()->get();

        $form['url']                = url('setupnominal');
        $form['nama']               = '';
        $form['nominal']            = '';

        if ($id){
            $res = SetupNominal::findOrFail($id);

            $form['url']                = url('setupnominal').'/'.$res->id;
            $form['nama']               = $res->nama;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Setup Nominal';
        $data['form']   = $form;
        return $this->view('admin.setupnominal.form', $data);
    }

    public function store(Request $request){
        $data = new SetupNominal();

        $data->nama                 = $request->input('nama');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = SetupNominal::findOrFail($id);

        $data->nama                 = $request->input('nama');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = SetupNominal::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}