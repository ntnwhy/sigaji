<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\Penggajian;
use App\DataPegawai;
use App\TransaksiLembur;
use App\Models\Periode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApprovalLemburController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Approval Lembur';
        $data['periode']    = Periode::query()->get();

        return $this->view('admin.approvallembur.view', $data);
    }

    public function show(Request $request){
      
        $data = TransaksiLembur::query(); 
        $data->with('pegawai');
        $data->with('periode');
        $data->where($this->isDeleted, false);

        return $this->dataTables($data->get());
    }

    public function approve($id){
        $data = TransaksiLembur::findOrFail($id);

        $data->status              = '1';

        $data->save();

        $datas = new Penggajian();

        $jam        = $data->total_jam;
        $na         = $jam * 5000 ;

        $datas->ref_id_pegawai       = $data->ref_id_pegawai;
        $datas->ref_id_periode       = $data->ref_id_periode;
        $datas->grup_komponen        = "Gaji";
        $datas->komponen             = "Kelebihan Jam Kerja";
        $datas->nominal              = $na;
        $datas->ket                  = $jam.' Jam';
        $datas->save();

        return $this->json(true, 'Approval data berhasil !');
    }

}