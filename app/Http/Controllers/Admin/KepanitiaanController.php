<?php

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\MasterKepanitiaan;
use App\MasterSubkepanitiaan;
use App\Models\Periode;
use App\Models\Kepanitiaan;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;

class KepanitiaanController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']      = 'Master Kepanitiaan';
        $data['periode']    = Periode::query()->orderby('status' ,'desc')->orderby('id' ,'desc')->get();        

        return $this->view('admin.kepanitiaan.view', $data);
    }

    public function show(Request $request){

        $data = Kepanitiaan::query();
        $data->with('panitia');
        $data->with('subpanitia');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['panitia']            = MasterKepanitiaan::query()->get();
        $data['jabatanpanitia']     = Lookup::query()->where('param', 'JABATAN_PANITIA')->get();

        $form['url']                = url('kepanitiaan');
        $form['urlpanitia']         = url('kepanitiaan/mpanitia');
        $form['urlsubpanitia']      = url('kepanitiaan/msubpanitia');
        
        $form['panitia']            = '';
        $form['subpanitia']         = '';
        $form['jabatanpanitia']     = '0';
        $form['nominal']            = '';

        if ($id){
            $res = Kepanitiaan::findOrFail($id);

            $form['url']                = url('kepanitiaan').'/'.$res->id;
           
            $form['panitia']            = $res->ref_id_kepanitiaan;
            $form['subpanitia']         = $res->ref_id_subkepanitiaan;
            $form['jabatanpanitia']     = $res->jabatan_kepanitiaan;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Kepanitiaan';
        $data['form']   = $form;
        return $this->view('admin.kepanitiaan.form', $data);
    }

    public function formpanitia($id = null){
        
        $form['panitia']            = '';

        $data['title']  = 'Master Panitia';
        $data['form']   = $form;
        return $this->view('admin.kepanitiaan.formpanitia', $data);
    }

    public function showpanitia(Request $request){

        $data = MasterKepanitiaan::query();

        return $this->dataTables($data->get());
    }

    public function store(Request $request){

        $data = new Kepanitiaan();

        $data->ref_id_kepanitiaan       = $request->input('panitia');
        $data->ref_id_subkepanitiaan    = $request->input('subpanitia');
        $data->jabatan_kepanitiaan      = $request->input('jabatanpanitia');
        $data->nominal                  = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){

        $data = Kepanitiaan::findOrFail($id);

        $data->ref_id_kepanitiaan       = $request->input('panitia');
        $data->ref_id_subkepanitiaan    = $request->input('subpanitia');
        $data->jabatan_kepanitiaan      = $request->input('jabatanpanitia');
        $data->nominal                  = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        
        $data = Kepanitiaan::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }

    public function sub($id){
        $data = MasterSubkepanitiaan::where('ref_id_kepanitiaan', $id)->get();

        return $this->json(true, 'Data Sub Kepanitiaan', $data);
    }

    public function mpanitia(Request $request){

        $data = new MasterKepanitiaan();
        $data->nama_kepanitiaan      = $request->input('mpanitia');
        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }
}