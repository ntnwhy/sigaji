<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Makul;
use App\Models\KelompokMakul;
use App\Models\Kurikulum;
use App\Models\Progdi;
use App\Lookup;
use Illuminate\Http\Request;

class MakulController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Mata Kuliah';

        return $this->view('admin.makul.view', $data);
    }

    public function show(Request $request){
        $data = Makul::query();
        $data->where($this->isDeleted, false);

        $data->with('kelompok')
            ->with('kurikulum');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['semester']   = Lookup::where('param', 'SEMESTER_FULL')->get();
        $data['kelompok']   = KelompokMakul::where($this->isDeleted, false)->get();
        $data['kurikulum']  = Kurikulum::where($this->isDeleted, false)->get();
        $data['jenis']      = Lookup::where('param', 'JENIS_MAKUL')->get();
        $data['tipe']       = Lookup::where('param', 'TIPE_MAKUL')->get();

        $form['url']                = url('makul');
        $form['kode']               = '';
        $form['nama']               = '';
        $form['jenis']              = '';
        $form['tipe']               = '';
        $form['kelompok_makul_id']  = '';
        $form['sks_teori']          = '';
        $form['sks_praktek']        = '';
        $form['sks_total']          = '';
        $form['kurikulum_id']       = '';
        $form['semester']           = '';

        if ($id){
            $res = Makul::findOrFail($id);

            $form['url']                = url('makul').'/'.$res->id;
            $form['kode']               = $res->kode;
            $form['nama']               = $res->nama;
            $form['jenis']              = $res->jenis;
            $form['tipe']               = $res->tipe;
            $form['kelompok_makul_id']  = $res->kelompok_makul_id;
            $form['sks_teori']          = $res->sks_teori;
            $form['sks_praktek']        = $res->sks_praktek;
            $form['sks_total']          = $res->sks_total;
            $form['kurikulum_id']       = $res->kurikulum_id;
            $form['semester']           = $res->semester;
    
        }

        $data['title']  = 'Form Mata Kuliah';
        $data['form']   = $form;
        return $this->view('admin.makul.form', $data);
    }

    public function store(Request $request){
        $data = new Makul();

        $data->kode                 = $request->input('kode');
        $data->nama                 = $request->input('nama');
        $data->jenis                = $request->input('jenis');
        $data->tipe                 = $request->input('tipe');
        $data->kelompok_makul_id    = $request->input('kelompok_makul_id');
        $data->sks_teori            = $request->input('sks_teori');
        $data->sks_praktek          = $request->input('sks_praktek');
        $data->sks_total            = $request->input('sks_total');
        $data->kurikulum_id         = $request->input('kurikulum_id');
        $data->semester             = $request->input('semester');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Makul::findOrFail($id);

        $data->kode                 = $request->input('kode');
        $data->nama                 = $request->input('nama');
        $data->jenis                = $request->input('jenis');
        $data->tipe                 = $request->input('tipe');
        $data->kelompok_makul_id    = $request->input('kelompok_makul_id');
        $data->sks_teori            = $request->input('sks_teori');
        $data->sks_praktek          = $request->input('sks_praktek');
        $data->sks_total            = $request->input('sks_total');
        $data->kurikulum_id         = $request->input('kurikulum_id');
        $data->semester             = $request->input('semester');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = Makul::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}