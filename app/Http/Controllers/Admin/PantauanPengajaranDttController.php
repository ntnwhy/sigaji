<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\DataPegawai;
use App\MataKuliah;
use App\SetupMakul;
use App\TahunAkademik;
use App\SetupNominal;
use App\ProgramStudi;
use App\TransaksiKehadiran;
use App\PantauanPengajaran;
use App\MasterKepanitiaan;
use App\MataKuliahProyekPendidikan;
use App\KelasMataKuliahProyekPendidikan;
use App\Models\Periode;
use App\Models\Pegawai;
use App\Models\ProyekPendidikan;
use App\Models\Makul_proyekpend;
use App\Http\Controllers\Controller;
use App\Models\Kepanitiaan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PantauanPengajaranDttController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title']      = 'Pantauan Pengajaran';
        $data['periode']    = Periode::query()->orderby('status' ,'desc')->orderby('id' ,'desc')->get();

        return $this->view('admin.pantauanpengajarandtt.view', $data);
        
    }

    public function show(Request $request){

        $user       = $this->getUsers()->name;        
        $userrole   = $this->getUsers()->role_id;     
 
      
        $data = PantauanPengajaran::query(); 

        if ($request->query('periode')){
            $data->where('ref_id_periode', $request->query('periode'));
        }

        $data->with('periode');
        $data->with('pegawai');
        $data->with('makul');
        
        if($userrole <> 1 AND $userrole <> 5 ){
            $data->where('created_by', $user);
        }
            
        $data->where($this->isDeleted, false);

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $user = $this->getUsers()->name;

        $data['periode']            = Periode::query()->where($this->isDeleted, false)->where('status', '1')->get();

        $data['pegawai']            = Pegawai::query()->with('datapegawai')->Where('jabatan', 'Dosen Tidak Tetap')->orderby('id','asc')->get();
        $data['proyekpendidikan']   = ProyekPendidikan::query()->with('prodi')->where('StatusAktif','1')->orderby('IdProyekPendidikan','DESC')->get();
        $data['tahunakademik']      = TahunAkademik::query()->where('status',true)->get();
        $data['durasi']             = Lookup::query()->where('param', 'DURASI')->get();

        $form['url']                = url('pantauanpengajarandtt');
        
        $form['pegawai']            = '';
        $form['periode']            = '';
        $form['proyekpendidikan']   = '';
        $form['matakuliah']         = '';
        $form['kelaskuliah']        = '';
        $form['idmk']               = '';
        $form['tipemakul']          = '';
        $form['sks']                = '';
        $form['tanggal']            = '';
        $form['durasi']             = '';

        if ($id){
            $res = PantauanPengajaran::findOrFail($id);

            $form['url']                = url('pantauanpengajarandtt').'/'.$res->id;
           
            $form['pegawai']            = $res->ref_id_pegawai;
            $form['periode']            = $res->ref_id_periode;
            $form['proyekpendidikan']   = $res->ref_id_pp;
            $form['matakuliah']         = $res->ref_id_makul;
            $form['kelaskuliah']        = $res->kelas_kuliah;
            $form['tanggal']            = $res->tanggal;
            $form['durasi']             = $res->durasi;
            $form['idmk']               = $res->ref_id_makul;
            $form['tipemakul']          = $res->tipe_makul;
            $form['sks']                = $res->sks;
                
        }

        $data['title']  = 'Pantauan Pengajaran Dosen Tidak Tetap';
        $data['form']   = $form;
        return $this->view('admin.pantauanpengajarandtt.form', $data);
    }

    public function formduplicate($id = null){

        $user = $this->getUsers()->name;

        $data['periode']            = Periode::query()->where($this->isDeleted, false)->where('status', '1')->get();

        $data['pegawai']            = Pegawai::query()->with('datapegawai')->where('jabatan','Dosen Tetap')->orWhere('jabatan', 'Dosen Tidak tetap')->orderby('id','asc')->get();
        $data['proyekpendidikan']   = ProyekPendidikan::query()->with('prodi')->where('StatusAktif','1')->orderby('IdProyekPendidikan','DESC')->get();
        $data['durasi']             = Lookup::query()->where('param', 'DURASI')->get();

        $form['url']                = url('pantauanpengajarandtt');
        
        $form['pegawai']            = '';
        $form['periode']            = '';
        $form['proyekpendidikan']   = '';
        $form['matakuliah']         = '';
        $form['kelaskuliah']        = '';
        $form['idmk']               = '';
        $form['tipemakul']          = '';
        $form['tanggal']            = '';
        $form['durasi']             = '';

        if ($id){
            $res = PantauanPengajaran::findOrFail($id);

            $form['url']                = url('pantauanpengajarandtt');
           
            $form['pegawai']            = $res->ref_id_pegawai;
            $form['periode']            = $res->ref_id_periode;
            $form['proyekpendidikan']   = $res->ref_id_pp;
            $form['matakuliah']         = $res->ref_id_makul;
            $form['kelaskuliah']        = $res->kelas_kuliah;
            $form['tanggal']            = $res->tanggal;
            $form['durasi']             = $res->durasi;
            $form['idmk']               = $res->ref_id_makul;
            $form['tipemakul']          = $res->tipe_makul;
                
        }

        $data['title']  = 'Pantauan Pengajaran';
        $data['form']   = $form;
        return $this->view('admin.pantauanpengajarandtt.form', $data);
    }

    public function store(Request $request){


        $user = $this->getUsers()->name;
        $data = new PantauanPengajaran();

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->ref_id_periode       = $request->input('periode');
        $data->ref_id_makul         = $request->input('idmk');
        $data->kelas_kuliah         = $request->input('kelaskuliah');
        $data->is_flat              = $request->input('is_flat');
        $data->durasi               = $request->input('durasi');
        $data->tanggal              = $request->input('tanggal');
        $data->tipe_makul           = $request->input('tipemakul');
        $data->created_by           = $user;
        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        
        $user = $this->getUsers()->name;
        $data = PantauanPengajaran::findOrFail($id);

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->ref_id_periode       = $request->input('periode');
        $data->ref_id_makul         = $request->input('idmk');
        $data->kelas_kuliah         = $request->input('kelaskuliah');
        $data->is_flat              = $request->input('is_flat');
        $data->tanggal              = $request->input('tanggal');
        $data->durasi               = $request->input('durasi');
        $data->tipe_makul           = $request->input('tipemakul');
        $data->updated_by           = $user;
        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        
        $data = PantauanPengajaran::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }

    public function makul($id){

        $data = SetupMakul::with('makul')->with('tahunakademik')->where('ref_id_pegawai',$id)->get();

        return $this->json(true, 'Data Mata Kuliah', $data);
    }

    public function makulpp($id){

        $data = Makul_proyekpend::with('detailmakul')->with('detailpp')->where('IdProyekPendidikan',$id)->get();

        return $this->json(true, 'Data Mata Kuliah', $data);
    }

    public function kelaspp($id){

        $pecah  = explode("-", $id);
        $pp     = $pecah[0];
        $mk     = $pecah[1];


        $data = Makul_proyekpend::query()->with('detailkelas')->where('IdProyekPendidikan',$pp)->where('IdMataKuliah',$mk)->get();

        return $this->json(true, 'Data Mata Kuliah PP', $data);
    }

    // public function ceknominal($id){

    //     $data = SetupNominal::where('nama',$id)->get();

    //     return $this->json(true, 'Data Nominal', $data);
    // }

    public function cekmakul($id){

        $data = MataKuliah::query()->where('IdMataKuliah',$id)->get();

        return $this->json(true, 'Detail Mata Kuliah', $data);
    }   

    public function rekap(){

        $data['title']      = 'Rekap Pantauan Pengajaran';
        $data['periode']    = Periode::query()->orderby('status' ,'desc')->orderby('id' ,'desc')->get();


        // $data['rekap'] = PantauanPengajaran::groupBy('ref_id_periode', 'ref_id_pegawai', 'ref_id_makul', 'tipe_makul','sks', 'is_malam')->select('ref_id_periode','ref_id_pegawai', 'ref_id_makul', 'tipe_makul','sks', 'is_malam', \DB::raw('sum(durasi) as total'))->with('pegawai')->with('periode')->with('makul')->where('status', 1)->get();

        return $this->view('admin.pantauanpengajaran.rekap', $data);

        // return $this->json(true, 'Rekap data', $data); 
    }   

    public function showrekap(Request $request){

        $data = PantauanPengajaran::groupBy('ref_id_periode', 'ref_id_pegawai', 'ref_id_makul', 'tipe_makul','sks', 'is_malam')->select('ref_id_periode','ref_id_pegawai', 'ref_id_makul', 'tipe_makul','sks', 'is_malam', \DB::raw('sum(durasi) as total'));
        
        if ($request->query('periode')){
            $data->where('ref_id_periode', $request->query('periode'));
            
        } 
        $data->where('status', 1);
        $data->where('is_deleted', false);
        $data->with('pegawai');
        $data->with('kodegaji');
        $data->with('periode');
        $data->with('makul');
        $data->with('nominal');
        $data->orderby('ref_id_pegawai', 'asc');

        return $this->json(true, 'Rekap data', $data->get()); 
    } 

    public function rekaptu(){

        $data['title']      = 'Rekap Pantauan Pengajaran';
        $data['periode']    = Periode::query()->orderby('status' ,'desc')->orderby('id' ,'desc')->get();


        // $data['rekap'] = PantauanPengajaran::groupBy('ref_id_periode', 'ref_id_pegawai', 'ref_id_makul', 'tipe_makul','sks', 'is_malam')->select('ref_id_periode','ref_id_pegawai', 'ref_id_makul', 'tipe_makul','sks', 'is_malam', \DB::raw('sum(durasi) as total'))->with('pegawai')->with('periode')->with('makul')->where('status', 1)->get();

        return $this->view('admin.pantauanpengajaran.rekaptu', $data);

        // return $this->json(true, 'Rekap data', $data); 
    }   

    public function showrekaptu(Request $request){

        $user       = $this->getUsers()->name;        
        $userrole   = $this->getUsers()->role_id;   

        $data = PantauanPengajaran::groupBy('ref_id_periode', 'ref_id_pegawai', 'ref_id_makul', 'kelas_kuliah', 'tipe_makul','sks', 'is_malam')->select('ref_id_periode','ref_id_pegawai', 'ref_id_makul', 'kelas_kuliah', 'tipe_makul', 'sks', 'is_malam', \DB::raw('sum(durasi) as total'));
        
        if ($request->query('periode')){
            $data->where('ref_id_periode', $request->query('periode'));
            
        } 

        if($userrole <> 1 AND $userrole <> 5 ){
            $data->where('created_by', $user);
        }

        $data->where('is_deleted', false);
        $data->with('pegawai');
        $data->with('periode');
        $data->with('makul');
        $data->orderby('ref_id_pegawai', 'asc');

        return $this->json(true, 'Rekap data', $data->get()); 
    } 

}