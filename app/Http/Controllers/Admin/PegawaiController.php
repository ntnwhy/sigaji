<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;

use App\Golongan;
use App\Fungsional;
use App\Fungsi;
use App\Struktural;
use App\Lookup;
use App\Pendidikan;
use App\Penggajian;
use App\DataPegawai;
use App\UnitKerja;
use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\GajiPokok;
use Illuminate\Http\Request;

class PegawaiController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Pegawai';

        return $this->view('admin.pegawai.view', $data);
    }

    public function show(Request $request){

        $data = Pegawai::query(); 
        $data->with('datapegawai');
        $data->with('golongan');
        $data->with('jabfung');
        $data->with('struktural');
        $data->with('pendidikan');
        $data->with('fungsi');
        $data->with('tunjtetap');
        $data->with('gajipokok.golongan');
        $data->with('tunjfungsional');
        $data->with('tunjpendidikan');
        $data->with('tunjstruktural');
        $data->where('is_deleted' , false);
        $data->orderBy('id' , 'asc');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['golongan']               = Golongan::query()->get();
        $data['jabatan_fungsional']     = Fungsional::query()->get();
        $data['fungsi']                 = Fungsi::query()->get();
        $data['jabatan_struktural']     = Struktural::query()->get();
        $data['pendidikan']             = Pendidikan::query()->get();
        $data['unit']                   = UnitKerja::query()->get();
        $data['jabatan']                = Lookup::where('param', 'JABATAN')->get();
        $data['masa_kerja']             = Lookup::where('param', 'MASA_KERJA')->get();

        $form['url']                = url('pegawai');
        $form['nis']                = '';
        $form['kode_gaji']          = '';
        $form['nama']               = '';
        $form['gp']                 = '';
        $form['jabatan']            = '';
        $form['fungsi']             = '';
        $form['unit']               = '';
        $form['golongan']           = '';
        $form['masa_kerja']         = '';
        $form['jabatan_fungsional'] = '';
        $form['jabatan_struktural'] = '';
        $form['pendidikan']         = '';

        if ($id){

            $res        = Pegawai::findOrFail($id);

            $form['url']                = url('pegawai').'/'.$res->id;
            $form['kode_gaji']          = $res->kode_gaji;
            $form['jabatan']            = $res->jabatan;
            $form['nama']               = $res->datapegawai->nama;
            $form['nis']                = $res->datapegawai->nip;
            $form['gp']                 = $res->ref_id_gp;
            $form['unit']               = $res->ref_id_unit;

            
            $fungsi                     = $res->fungsi;
            if( $fungsi == "" ){
                $form['fungsi']         = 0;
            } else {
                $form['fungsi']         = $res->fungsi->id;
            }

            $golongan = $res->gajipokok;
            if( $golongan == "" ){
                $form['golongan']       = 0;
            } else {
                $form['golongan']       = $res->gajipokok->golongan->id;                
            }

            $masa_kerja = $res->gajipokok;
            if( $masa_kerja == "" ){
                $form['masa_kerja']     = 0;
            } else {
                $form['masa_kerja']     = $res->gajipokok->masa_kerja;                
            }

            $jabatan_fungsional = $res->jabfung;
            if( $jabatan_fungsional == "" ){
                $form['jabatan_fungsional'] = 0;
            } else {
                $form['jabatan_fungsional'] = $res->jabfung->id;                
            }

            $jabatan_struktural = $res->struktural;
            if( $jabatan_struktural == "" ){
                $form['jabatan_struktural'] = 0;
            } else {
                $form['jabatan_struktural'] = $res->struktural->id;                
            }

            $pendidikan = $res->pendidikan;
            if( $pendidikan == "" ){
                $form['pendidikan']         = 0;
            } else {
                $form['pendidikan']         = $res->pendidikan->id;                
            }

    
        }

        $data['title']  = 'Form Pegawai';
        $data['form']   = $form;
        return $this->view('admin.pegawai.form', $data);
    }

    public function store(Request $request){


        $data = new DataPegawai();

        $data->nip                  = $request->input('nis');
        $data->nipbaru              = $request->input('nis');
        $data->status_kepegawaian   = $request->input('jabatan');
        $data->nama                 = $request->input('nama');

        $data->save();

        $lastid = $data->id;

        $data = new Pegawai();

        // $data->nis                  = $request->input('nis');
        $data->id                       = $lastid;
        $data->jabatan                  = $request->input('jabatan');
        $data->ref_id_fungsi            = $request->input('fungsi');
        $data->ref_id_gp                = $request->input('gp');
        $data->ref_id_jabatanfungsional = $request->input('jabatan_fungsional');
        $data->ref_id_jabatanstruktural = $request->input('jabatan_struktural');
        $data->ref_id_pendidikan        = $request->input('pendidikan');
        $data->kode_gaji                = $request->input('kode_gaji');
        $data->ref_id_unit              = $request->input('unit');

        $data->save();



        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){

        $data = Pegawai::findOrFail($id);

        $data->jabatan                  = $request->input('jabatan');
        $data->ref_id_fungsi            = $request->input('fungsi');
        $data->ref_id_gp                = $request->input('gp');
        $data->ref_id_jabatanfungsional = $request->input('jabatan_fungsional');
        $data->ref_id_jabatanstruktural = $request->input('jabatan_struktural');
        $data->ref_id_pendidikan        = $request->input('pendidikan');
        $data->kode_gaji                = $request->input('kode_gaji');
        $data->ref_id_unit              = $request->input('unit');

        $data->save();

        $data = DataPegawai::findOrFail($id);

        $data->nip                  = $request->input('nis');
        $data->nipbaru              = $request->input('nis');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = Pegawai::findOrFail($id);

        $this->remove($data);

        $data = DataPegawai::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }

    public function cekgp($id){

        $pecah  = explode("-", $id);
        $gol    = $pecah[0];
        $mk     = $pecah[1];

        $data   = GajiPokok::query()->where('ref_id_golongan',$gol)->where('masa_kerja',$mk)->get();

        return $this->json(true, 'Detail gajipokok', $data);
    } 
}