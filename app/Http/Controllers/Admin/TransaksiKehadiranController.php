<?php

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\DataPegawai;
use App\UnitKerja;
use App\TransaksiKehadiran;
use App\Exports\KehadiranExport;
use App\Import\KehadiranImport;
use App\Models\Pegawai;
use App\Models\Periode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class TransaksiKehadiranController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']      = 'Transaksi Kehadiran';
        $data['periode']    = Periode::query()->orderby('status' ,'desc')->orderby('id' ,'desc')->get();
        $data['unitkerja']  = UnitKerja::query()->get();

        return $this->view('admin.transaksikehadiran.view', $data);
    }

    public function show(Request $request){
      
        $data = TransaksiKehadiran::query(); 
        $data->with('pegawai.datapegawai');
        $data->with('pegawai.unit');
        $data->with('periode');
        $data->where($this->isDeleted, false);
        
        if ($request->query('periode')){
            $data->where('ref_id_periode', $request->query('periode'));
        }


        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $user = $this->getUsers()->name;

        $data['periode']            = Periode::query()->where($this->isDeleted, false)->where('status', '1')->get();

        $data['pegawai']            = DataPegawai::query()->get();


        $form['url']                = url('transaksikehadiran');
        
        $form['pegawai']            = '';
        $form['jam_hadir']          = '';
        $form['hari_hadir']         = '';
        $form['persentase']         = '';

        if ($id){
            $res = TransaksiKehadiran::findOrFail($id);

            $form['url']                = url('transaksikehadiran').'/'.$res->id;
           
            $form['pegawai']            = $res->ref_id_pegawai;
            $form['jam_hadir']          = $res->jumlah_jam_kehadiran;
            $form['hari_hadir']         = $res->jumlah_hari_kehadiran;
            $form['persentase']         = $res->persentase_kehadiran;
    
        }

        $data['title']  = 'Form Transaksi Kehadiran';
        $data['form']   = $form;
        return $this->view('admin.transaksikehadiran.form', $data);
    }

    public function store(Request $request){

        $user = $this->getUsers()->name;
        $data = new TransaksiKehadiran();

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->ref_id_periode       = $request->input('periode');
        $data->jumlah_hari_kehadiran= $request->input('hari_hadir');
        $data->jumlah_jam_kehadiran = $request->input('jam_hadir');
        $data->persentase_kehadiran = $request->input('persentase');
        $data->created_by           = $user;
        $data->save();

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        
        $user = $this->getUsers()->name;

        $data = TransaksiKehadiran::findOrFail($id);

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->ref_id_periode       = $request->input('periode');
        $data->jumlah_hari_kehadiran= $request->input('hari_hadir');
        $data->jumlah_jam_kehadiran = $request->input('jam_hadir');
        $data->persentase_kehadiran = $request->input('persentase');
        $data->updated_by           = $user;
        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        
        $data = TransaksiKehadiran::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }

    public function export(Request $request){

        $nama = 'TemplateKehadiran_'.$request->query('unit').'.xlsx';

        $nama = str_replace(" ", "", $nama);

        return Excel::download(new KehadiranExport($request->query('periode'),$request->query('unit')),
            $nama);
    }

    public function stores(Request $request){

        if ($request->input('nilai_id')){
            $nilai = Nilai::findOrFail($request->input('nilai'));
            $nilai->nilai = $request->input('nilai');

            $nilai->save();
        }else{

            $nilai = new Nilai();

            $nilai->mahasiswa_id            = $request->input('mahasiswa');
            $nilai->semester                = $request->input('semester');
            $nilai->proyek_pendidikan_id    = $request->input('proyek');
            $nilai->makul_id                = $request->input('makul');
            $nilai->nilai                   = $request->input('nilai');

            $nilai->save();
        }

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function import(Request $request){

        Excel::import(new KehadiranImport, $request->file('file'));

        return $this->json(true, 'Import data kehadiran berhasil !');
    }

}