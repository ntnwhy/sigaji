<?php

namespace App\Http\Controllers\Admin;

use App\DataPegawai;
use App\MataKuliah;
use App\SetupMakul;
use App\TahunAkademik;
use App\ProgramStudi;
use App\MataKuliahProyekPendidikan;
use App\KelasMataKuliahProyekPendidikan;
use App\Models\Pegawai;
use App\Models\ProyekPendidikan;
use App\Models\Makul_proyekpend;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class SetupmakulController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){

        $data['title']          = 'Setup Mata Kuliah Flat';
        $data['pegawai']        = DataPegawai::query()->get();
        $data['tahunakademik']  = TahunAkademik::query()->where('status', true)->get();

        return $this->view('admin.setupmakul.view', $data);

    }

    public function rekap(Request $request){

        $data['title']  = 'Rekap Pemetaan Mata Kuliah';

        $data['session_user']  = $request->session()->get('session_user');
        $data['session_nama']  = $request->session()->get('session_nama');

        $data['tahunakademik']      = TahunAkademik::query()->where('status', true)->get();

        return $this->view('setupmakul.rekap', $data);
 
    }

    public function show(Request $request){

           $data = SetupMakul::query()->groupBy('ref_id_tahunakademik', 'ref_id_pegawai', 'ref_id_pp','ref_id_makul','is_flat','is_teori','is_praktek','sks_teori','sks_praktek')->select('ref_id_tahunakademik', 'ref_id_pegawai', 'ref_id_pp','ref_id_makul','is_flat','is_teori','is_praktek','sks_teori','sks_praktek', \DB::raw('count(ref_id_makul) as jml'), \DB::raw('sum(sks_teori) as jmlt'), \DB::raw('sum(sks_praktek) as jmlp'), \DB::raw('(sum(sks_teori) + sum(sks_praktek)) * count(ref_id_makul) as jmls'));
           $data->with('makul');
           $data->with('proyekpendidikan');
           $data->with('tahunakademik');
    
            if ($request->query('tahunakademik')){
                $data->where('ref_id_tahunakademik', $request->query('tahunakademik'));
            }

            if ($request->query('pegawai')){
                $data->where('ref_id_pegawai', $request->query('pegawai'));
            }

            $data->where('is_deleted', false);

        // $data       = SetupMakul::query()->with('proyekpendidikan')->with('makul');


        return $this->dataTables($data->get());
    }

    public function showrekap(Request $request){

        $data       = SetupMakul::query()->with('pegawai')->with('proyekpendidikan')->with('makul');

        if ($request->query('tahunakademik')){
            $data->where('ref_id_tahunakademik', $request->query('tahunakademik'));
        }

        $data->where('is_deleted', false);

        return $this->dataTables($data->get());
    }

    public function form($id = null, Request $request){

        $data['proyekpendidikan']   = ProyekPendidikan::query()->with('prodi')->where('StatusAktif','1')->orderby('IdProyekPendidikan','DESC')->get();

        $data['tahunakademik']      = TahunAkademik::query()->where('status', true)->get();
        $data['pegawai']            = Pegawai::query()->with('datapegawai')->where('id_edu', $idedu)->get();

        $form['url']                = url('setupmakul');
        $form['proyekpendidikan']   = 0;
        $form['matakuliah']         = 0;
        $form['kelaskuliah']        = 0;

        $form['nama']               = $request->session()->get('session_nama');
        $form['idedu']              = $request->session()->get('session_user');


        if ($id){
            $res = SetupMakul::query()->with('makul')->findOrFail($id);

            $form['url']                = url('setupmakul')."/".$res->id;

            $form['proyekpendidikan']   = $res->ref_id_pp;
            $form['matakuliah']         = $res->ref_id_makul;
            $form['kelaskuliah']        = $res->kelas_kuliah;
    
        }

        $data['title']  = 'Pemetaan Mata Kuliah';
        $data['form']   = $form;
        

        if(empty($data['session_user'])){

            return redirect('/');

        } else {

            return $this->view('setupmakul.form', $data);
            
        }

    }

    public function makul($id){

        $data = SetupMakul::with('makul')->with('tahunakademik')->where('ref_id_pegawai',$id)->get();

        return $this->json(true, 'Data Mata Kuliah', $data);
    }

    public function makulpp($id){

        $data = Makul_proyekpend::with('detailmakul')->with('detailpp')->where('IdProyekPendidikan',$id)->get();

        return $this->json(true, 'Data Mata Kuliah', $data);
    }

    public function kelaspp($id){

        $pecah  = explode("-", $id);
        $pp     = $pecah[0];
        $mk     = $pecah[1];


        $data = Makul_proyekpend::query()->with('detailkelas')->where('IdProyekPendidikan',$pp)->where('IdMataKuliah',$mk)->get();

        return $this->json(true, 'Data Mata Kuliah PP', $data);
    }

    // public function ceknominal($id){

    //     $data = SetupNominal::where('nama',$id)->get();

    //     return $this->json(true, 'Data Nominal', $data);
    // }

    public function cekmakul($id){

        $data = MataKuliah::query()->where('IdMataKuliah',$id)->get();

        return $this->json(true, 'Detail Mata Kuliah', $data);
    }   

    public function store(Request $request){

        if (Input::get('cbteori') == 'on'){
            $isteori = true;
            $sksteori = $request->input('sksteori');
        } else{
            $isteori = false;
            $sksteori = 0;            
        }

        if (Input::get('cbpraktek') == 'on'){
            $ispraktek = true;
            $skspraktek = $request->input('skspraktek');            
        } else{
            $ispraktek = false;
            $skspraktek = 0;           

        }

        $kk = $request->input('kelaskuliah');

        foreach($kk as $dkk){


                $data = new SetupMakul();

                $data->ref_id_tahunakademik = $request->input('tahunakademik');
                $data->ref_id_pegawai       = $request->input('pegawai');
                $data->ref_id_pp            = $request->input('proyekpendidikan');
                $data->ref_id_makul         = $request->input('matakuliah');
                $data->kelas_kuliah         = $dkk;
                $data->is_flat              = false;
                $data->created_by           = $request->input('idedu');
                $data->is_teori             = $isteori;
                $data->is_praktek           = $ispraktek;
                $data->sks_teori            = $sksteori;
                $data->sks_praktek          = $skspraktek;

                $data->save();

            
           
        }

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update($id, Request $request){

        if (Input::get('cbteori') == 'on'){
            $isteori = true;
            $sksteori = $request->input('sksteori');
        } else{
            $isteori = false;
            $sksteori = 0;            
        }

        if (Input::get('cbpraktek') == 'on'){
            $ispraktek = true;
            $skspraktek = $request->input('skspraktek');            
        } else{
            $ispraktek = false;
            $skspraktek = 0;           

        }

        $kk = $request->input('kelaskuliah');

        foreach($kk as $dkk){

            $data = SetupMakul::findOrFail($id);

            $data->ref_id_tahunakademik = $request->input('tahunakademik');
            $data->ref_id_pegawai       = $request->input('pegawai');
            $data->ref_id_pp            = $request->input('proyekpendidikan');
            $data->ref_id_makul         = $request->input('matakuliah');
            $data->kelas_kuliah         = $dkk;
            $data->is_flat              = false;
            $data->updated_by           = $request->input('idedu');
            $data->is_teori             = $isteori;
            $data->is_praktek           = $ispraktek;
            $data->sks_teori            = $sksteori;
            $data->sks_praktek          = $skspraktek;

            $data->save();

        }

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){

        $data = SetupMakul::findOrFail($id);
        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil !');
    }

    public function printform(Request $request, $id){


        $tahunakademik      = TahunAkademik::where('status', true)->firstOrFail();

        $data['title']      = "Form Pemetaan Mata Kuliah Dan SKS";
        $data['semester']   = "Semester". $tahunakademik->semester;
        $data['ta']         = "Tahun Akademik". $tahunakademik->tahun_akademik;
        $data['nama']       = $request->session()->get('session_nama');
        $data['prodi']      = ProgramStudi::query()->get();
        $data['tgl_kumpul'] = $tahunakademik->tgl_kumpul;
        $data['tgl_serah']  = $tahunakademik->tgl_serah;

        $data['detail'] = SetupMakul::query()->groupBy('ref_id_tahunakademik', 'ref_id_pegawai', 'ref_id_pp','ref_id_makul','is_teori','is_praktek','sks_teori','sks_praktek')->select('ref_id_tahunakademik', 'ref_id_pegawai', 'ref_id_pp','ref_id_makul','is_teori','is_praktek','sks_teori','sks_praktek', \DB::raw('count(ref_id_makul) as jml'), \DB::raw('sum(sks_teori) as jmlt'), \DB::raw('sum(sks_praktek) as jmlp'))->with('makul')->with('proyekpendidikan')->with('tahunakademik')->where('ref_id_pegawai', $id)->where('is_deleted', false)->get();

        // $data['detail'] = SetupMakul::query()->with('makul')->with('proyekpendidikan')->with('tahunakademik')->where('ref_id_pegawai', $id)->get();

        return $this->view('setupmakul.printform', $data);
        // return $this->dataTables($data);
    }

    public function ceksks($id){

        $data = Pegawai::query()->with('struktural')->where('id',$id)->get();

        return $this->json(true, 'Data Pegawai', $data);
    } 

    public function flat($id){

        $set = explode("-", $id);

        $idta = $set[0];
        $idpg = $set[1];
        $idpp = $set[2];
        $idmk = $set[3];
        

        $data = SetupMakul::where('ref_id_tahunakademik', $idta)->where('ref_id_pegawai', $idpg)->where('ref_id_pp', $idpp)->where('ref_id_makul', $idmk)->update(['is_flat'=>true]);

        return $this->json(true, 'Simpan data berhasil !');
    }
}