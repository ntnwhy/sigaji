<?php

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Periode;

class PeriodeController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Periode';

        return $this->view('admin.periode.view', $data);
    }

    public function show(Request $request){
        $data = Periode::query();
        $data->where($this->isDeleted, false);

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['tahun']   = Lookup::where('param', 'TAHUN')->where('status', true)->get();
        $data['bulan']   = Lookup::where('param', 'BULAN')->where('status', true)->get();

        $form['url']                    = url('periode');
        $form['tahun']                  = '';
        $form['bulan']                  = '';
        $form['status']                 = '';

        if ($id){
            $res = Periode::findOrFail($id);

            $form['url']                = url('periode').'/'.$res->id;
            $form['bulan']              = $res->bulan;
            $form['tahun']              = $res->tahun;
            $form['status']             = $res->status;
    
        }

        $data['title']  = 'Form Periode Penggajian';
        $data['form']   = $form;
        return $this->view('admin.periode.form', $data);
    }

    public function store(Request $request){
        $data = new Periode();

        $data->bulan                = $request->input('bulan');
        $data->tahun                = $request->input('tahun');
        $data->status               = $request->input('status');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Periode::findOrFail($id);

        $data->bulan                = $request->input('bulan');
        $data->tahun                = $request->input('tahun');
        $data->status               = $request->input('status');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = Periode::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil ');
    }
}