<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Lookup;
use App\DataPegawai;
use App\TransaksiLembur;
use App\Models\Periode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransaksiLemburController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Transaksi Lembur';
        $data['periode']    = Periode::query()->orderby('status' ,'desc')->orderby('id' ,'desc')->get();


        return $this->view('admin.transaksilembur.view', $data);
    }

    public function show(Request $request){

        $user = $this->getUsers()->name;
             
        $data = TransaksiLembur::query(); 
        $data->with('pegawai');
        $data->with('periode');
        $data->where($this->isDeleted, false);
        $data->where('created_by', $user);

        if($request->query('periode')){
            
            $data->where('ref_id_periode', $request->query('periode')); 

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $user = $this->getUsers()->name;

        $data['periode']            = Periode::query()->where($this->isDeleted, false)->where('status', '1')->get();

        $data['pegawai']            = DataPegawai::query()->get();
        $form['url']                = url('transaksilembur');
        
        $form['pegawai']            = '';
        $form['periode']            = '';
        $form['total_jam']          = '';

        if ($id){
            $res = TransaksiLembur::findOrFail($id);

            $form['url']                = url('transaksilembur').'/'.$res->id;
           
            $form['pegawai']            = $res->ref_id_pegawai;
            $form['periode']            = $res->ref_id_periode;
            $form['total_jam']          = $res->total_jam;
    
        }

        $data['title']  = 'Form Transaksi Lembur';
        $data['form']   = $form;
        return $this->view('admin.transaksilembur.form', $data);
    }

    public function store(Request $request){

        $user = $this->getUsers()->name;
        $data = new TransaksiLembur();

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->ref_id_periode       = $request->input('periode');
        $data->total_jam            = $request->input('total_jam');
        $data->created_by           = $user;
        $data->save();

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $user = $this->getUsers()->name;

        $data = TransaksiLembur::findOrFail($id);

        $data->ref_id_pegawai       = $request->input('pegawai');
        $data->ref_id_periode       = $request->input('periode');
        $data->total_jam            = $request->input('total_jam');
        $data->updated_by           = $user;
        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        
        $data = TransaksiLembur::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }

}