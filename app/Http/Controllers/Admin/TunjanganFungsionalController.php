<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Fungsional;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TunjanganFungsional;

class TunjanganFungsionalController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Master Tunjangan Fungsional';

        return $this->view('admin.tunjanganfungsional.view', $data);
    }

    public function show(Request $request){
        $data = TunjanganFungsional::query();
        $data->where($this->isDeleted, false);
        $data->with('fungsional');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['fungsional']   = Fungsional::query()->get();


        $form['url']                = url('tunjanganfungsional');
        $form['fungsional']         = '';
        $form['nominal']            = '';

        if ($id){
            $res = TunjanganFungsional::findOrFail($id);

            $form['url']                = url('tunjanganfungsional').'/'.$res->id;
            $form['fungsional']             = $res->ref_id_fungsi;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Tunjangan Fungsional';
        $data['form']   = $form;
        return $this->view('admin.tunjanganfungsional.form', $data);
    }

    public function store(Request $request){
        $data = new TunjanganFungsional();

        $data->ref_id_fungsional    = $request->input('fungsional');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = TunjanganFungsional::findOrFail($id);

        $data->ref_id_fungsional        = $request->input('fungsional');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = TunjanganFungsional::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}