<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 08/10/2018
 * Time: 2:37
 */

namespace App\Http\Controllers\Admin;
use App\Fungsi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TunjanganTetap;

class TunjanganTetapController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data['title']  = 'Master Tunjangan Tetap';

        return $this->view('admin.tunjangantetap.view', $data);
    }

    public function show(Request $request){
        $data = TunjanganTetap::query();
        $data->where($this->isDeleted, false);
        $data->with('fungsi');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $data['fungsi']   = Fungsi::query()->get();


        $form['url']                = url('tunjangantetap');
        $form['fungsi']             = '';
        $form['nominal']            = '';

        if ($id){
            $res = TunjanganTetap::findOrFail($id);

            $form['url']                = url('tunjangantetap').'/'.$res->id;
            $form['fungsi']             = $res->ref_id_fungsi;
            $form['nominal']            = $res->nominal;
    
        }

        $data['title']  = 'Form Tunjangan Tetap';
        $data['form']   = $form;
        return $this->view('admin.tunjangantetap.form', $data);
    }

    public function store(Request $request){
        $data = new TunjanganTetap();

        $data->ref_id_fungsi        = $request->input('fungsi');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = TunjanganTetap::findOrFail($id);

        $data->ref_id_fungsi        = $request->input('fungsi');
        $data->nominal              = $request->input('nominal');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function destroy($id){
        $data = TunjanganTetap::findOrFail($id);

        $this->remove($data);

        return $this->json(true, 'Hapus data berhasil @');
    }
}