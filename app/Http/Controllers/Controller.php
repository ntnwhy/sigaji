<?php

namespace App\Http\Controllers;

use App\Access;
use App\Helper\Constant;
use App\Menu;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class Controller extends BaseController{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $isDeleted = "is_deleted";
    protected $param = "param";
    protected $status = "status";

    protected function view($layout, $data = []){

        $role = $this->getUsers()->role_id;
        if ($role == 1){
            $menu = Menu::whereNull('menu_id')
                ->where('status', true)
                ->with('sub')
                ->orderBy('seq')
                ->get();
        }else{
            $menu = Menu::whereNull('menu_id')
                ->where('status', true)
                ->whereIn('id', $this->getAccess())
                ->with('sub')
                ->orderBy('seq')
                ->get();
        }


        $data['menu']   = $menu;
        return response()->view($layout, $data);
    }

    protected function getUsers(){

        return Auth::user();
    }

    protected function json($status, $msg, $data = []){

        return response()->json([
            'status'    => $status,
            'msg'       => $msg,
            'data'      => $data
        ]);
    }

    protected function dataTables($data){

        return DataTables::collection($data)->make(true);
    }

    protected function add($model){
        $model->created_by  = $this->getUsers()->name;

        $model->save();

        return $model;
    }

    protected function upd($model){
        $model->updated_by  = $this->getUsers()->name;

        return $model->save();
    }

    protected function remove($model){
        $model->deleted_by  = $this->getUsers()->name;
        $model->deleted_at  = date('Y-m-d H:i:s');
        $model->is_deleted  = true;

        return $model->save();
    }

    private function getAccess(){
        $data = Access::query()->where('role_id', Auth::user()->role_id)->get();

        $res = [];
        foreach ($data as $item) {
            $res[] = $item->menu_id;
        }

        return $res;
    }

}
