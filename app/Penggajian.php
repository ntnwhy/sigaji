<?php

namespace App;

use App\Models\Kepanitiaan;
use App\Penggajian;
use App\DataPegawai;
use App\Models\Periode;

use Illuminate\Database\Eloquent\Model;

class Penggajian extends Model
{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

	public function datapegawai(){
        return $this->hasOne(DataPegawai::class, 'id', 'ref_id_pegawai');
    }

    

}