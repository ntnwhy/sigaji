<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPegawai extends Model
{
    protected $connection = 'mysql';
    protected $table = "data_pegawai";
}
