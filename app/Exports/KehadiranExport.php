<?php

namespace App\Exports;

use App\Models\Pegawai;
use App\DataPegawai;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class KehadiranExport implements FromView {

    private $periode, $unit;

    public function __construct($periode, $unit){
        $this->periode  = $periode;
        // $this->jabatan  = $jabatan;
        $this->unit     = $unit;
    }

    public function view(): View{    

        $periode    = $this->periode;
        // $jabatan    = $this->jabatan;
        $unit       = $this->unit;  
 
        $data['temp'] = Pegawai::with('datapegawai')->where('ref_id_unit', $unit)->get();

        $data['periode'] = $periode;

        return view('excel.kehadiran_template', [
            'data' => $data
        ]);
    }
}
