<?php

namespace App\Exports;

use App\Models\Pegawai;
use App\DataPegawai;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class PotonganExport implements FromView {

    private $potongan;

    public function __construct($potongan){
        $this->potongan  = $potongan;
    }

    public function view(): View{    

        $data['potongan']    = $this->potongan;
 
        $data['temp'] = Pegawai::with('datapegawai')->where('jabatan', 'Pegawai Tetap')->orWhere('jabatan', 'Dosen Tetap')->get();

        return view('excel.potongan_template', [
            'data' => $data
        ]);
    }
}
