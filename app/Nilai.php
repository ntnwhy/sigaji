<?php

namespace App;

use App\Helper\ScalaHuruf;
use App\Models\Mahasiswa;
use App\Models\Makul;
use Illuminate\Database\Eloquent\Model;

class Nilai extends Model{

    protected $hidden = [
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
    ];

    protected $appends = ['huruf', 'akhir'];

    protected $fillable = [
        'mahasiswa_id',
        'makul_id'
    ];

    public function mahasiswa(){
        return $this->hasOne(Mahasiswa::class, 'id', 'mahasiswa_id');
    }

    public function makul(){
        return $this->hasOne(Makul::class, 'id', 'makul_id');
    }

    public function getHurufAttribute(){

        return ScalaHuruf::getScala($this->nilai);
    }

    public function getAkhirAttribute(){
        $m = $this->makul()->first();

        return $this->nilai * $m->sks_total;
    }
}
