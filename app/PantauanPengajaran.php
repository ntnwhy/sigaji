<?php

namespace App;

use App\Models\Kepanitiaan;
use App\Models\Pegawai;
use App\DataPegawai;
use App\MataKuliah;
use App\SetupNominal;
use App\ProgramStudi;
use App\Models\Periode;

use Illuminate\Database\Eloquent\Model;

class PantauanPengajaran extends Model
{
    protected $hidden = [
        'is_deleted',
    ];

    public function panitia(){
        return $this->hasOne(Kepanitiaan::class, 'id', 'ref_id_panitia');
    }

    public function pegawai(){
        return $this->hasOne(DataPegawai::class, 'id', 'ref_id_pegawai');
    }

    public function kodegaji(){
        return $this->hasOne(Pegawai::class, 'id', 'ref_id_pegawai');
    }

    public function periode(){
        return $this->hasOne(Periode::class, 'id', 'ref_id_periode');
    }

    public function makul(){
        return $this->hasOne(MataKuliah::class, 'IdMataKuliah', 'ref_id_makul');
    }

    public function nominal(){
        return $this->hasOne(SetupNominal::class, 'nama', 'tipe_makul');
    }

}