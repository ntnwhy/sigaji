<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model{

    protected $appends = ['links', 'parent'];
    protected $hidden = ['created_at, updated_at'];

    public function sub(){

        return $this->hasMany(Menu::class)->orderBy('seq');
    }

    public function getParentAttribute(){
        $data = $this->hasOne(Menu::class, 'id', 'menu_id')->first();

        if($data){
            return $data->nama;
        }else{
            return '';
        }
    }

    public function getLinksAttribute(){
        $data = $this->sub()->get();

        if(count($data) == 1){
            return $data[0]->url;
        }

        $link = [];
        foreach($data as $d){
            $link[] = $d->url;
        }

        return $link;
    }
}
