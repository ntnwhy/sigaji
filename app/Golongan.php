<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tbl_golongan';
}
