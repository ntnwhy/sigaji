<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramStudi extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = "ProgramStudi";
}
