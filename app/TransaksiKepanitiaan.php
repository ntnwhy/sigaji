<?php

namespace App;

use App\Models\Kepanitiaan;
use App\DataPegawai;
use App\Models\Periode;
use App\Models\Pegawai;

use Illuminate\Database\Eloquent\Model;

class TransaksiKepanitiaan extends Model
{
    protected $hidden = [
        'is_deleted',
    ];

    public function panitia(){
        return $this->hasOne(Kepanitiaan::class, 'id', 'ref_id_panitia');
    }

    public function pegawai(){
        return $this->hasOne(DataPegawai::class, 'id', 'ref_id_pegawai');
    }

    public function periode(){
        return $this->hasOne(Periode::class, 'id', 'ref_id_periode');
    }
}
