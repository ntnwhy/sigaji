<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 18/10/2018
 * Time: 22:30
 */

namespace App\Helper;

class Constant{

    public const KRS_STATUS_PENGAJUAN = 101;
    public const KRS_STATUS_SETUJU = 102;
    public const KRS_STATUS_BATAL = 103;

    public const STATUS_MHS_AKTIF = 1;
    public const STATUS_MHS_CUTI = 2;
    public const STATUS_MHS_MANGKIR = 3;
    public const STATUS_MHS_LULUS = 4;
    public const STATUS_MHS_DROP_OUT = 5;

    public const IS_DELETED = "is_deleted";

}