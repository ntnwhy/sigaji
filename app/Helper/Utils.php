<?php

function setActive($tag, $thisTag, $output = 'active'){
    if($tag == $thisTag){

        return $output;
    }

    $temp = explode('.', $thisTag);
    if(count($temp) > 1){
        foreach($temp as $s){

            if ($s == $tag){

                return $output;
            }
        }
    }

    return '';
}