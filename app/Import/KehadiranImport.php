<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 31/12/2018
 * Time: 3:02
 */

namespace App\Import;

use App\TransaksiKehadiran;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;

class KehadiranImport implements ToModel {

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */


    public function model(array $row){

        if ($row[0] == 'no'){

            return;
        }


        $hadir = TransaksiKehadiran::firstOrNew([
            'ref_id_pegawai'        => $row[1],
            'ref_id_periode'        => $row[2],
        ]);

        $hadir->ref_id_pegawai          = $row[1];
        $hadir->ref_id_periode          = $row[2];
        $hadir->jumlah_hari_kehadiran   = $row[4];
        $hadir->jumlah_jam_kehadiran    = $row[5];
        $hadir->persentase_kehadiran    = $row[6];
        $hadir->created_by              = "BAUK&KS";

        $hadir->save();

        return $hadir;
    }
}