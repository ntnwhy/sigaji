<?php
/**
 * Created by PhpStorm.
 * User: MA
 * Date: 31/12/2018
 * Time: 3:02
 */

namespace App\Import;

use App\Models\Potongan;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\Controller;

class PotonganImport implements ToModel {

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */


    public function model(array $row){

        if ($row[0] == 'no'){

            return;
        }

        if (empty($row[4])){

            return;
        }


        $potongan = Potongan::firstOrNew([
            'ref_id_pegawai'  => $row[1],
            'potongan'        => $row[3],
        ]);

        $potongan->ref_id_pegawai          = $row[1];
        $potongan->potongan                = $row[3];
        $potongan->nominal                 = $row[4];
        $potongan->created_by              = "BAUK&KS";

        $potongan->save();

        return $potongan;



        //numpang upload
        // if ($row[0] == 'IdNilaiMahasiswa'){

        //     return;
        // }

        // // if (empty($row[4])){

        // //     return;
        // // }


        // $potongan = new NilaiMahasiswa();

        // $potongan->IdMataKuliah            = $row[1];
        // $potongan->NIM                     = $row[4];
        // $potongan->NilaiMutu              = $row[16];
        // $potongan->SKS              = $row[17];
        // $potongan->SKSXNilai              = $row[18];
        // $potongan->NilaiHurufAkhir              = $row[19];
        // $potongan->EnterBy              = "Administrator";
        // $potongan->IsNilaiTransfer              = $row[25];

        // $potongan->save();

        // return $potongan;

        
    }
}