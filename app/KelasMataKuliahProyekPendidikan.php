<?php

namespace App;
use App\MataKuliah;
use App\ProyekPendidikan;

use Illuminate\Database\Eloquent\Model;

class KelasMataKuliahProyekPendidikan extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = "KelasMataKuliahProyekPendidikan";


    public function detailmakul(){
	    return $this->belongsTo(MataKuliah::class, 'IdMataKuliah', 'IdMataKuliah');
    }

    public function detailpp(){
	    return $this->hasOne(ProyekPendidikan::class, 'IdProyekPendidikan', 'IdProyekPendidikan');
    }
}
