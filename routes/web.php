<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'gajipokok'], function () {
    Route::get('/', 'Admin\GajiPokokController@index');
    Route::get('/show', 'Admin\GajiPokokController@show');
    Route::get('/form', 'Admin\GajiPokokController@form');
    Route::get('/form/{id}', 'Admin\GajiPokokController@form');
    Route::post('/', 'Admin\GajiPokokController@store');
    Route::post('/{id}', 'Admin\GajiPokokController@update');
    Route::get('hapus/{id}', 'Admin\GajiPokokController@destroy');
});

Route::group(['prefix' => 'pegawai'], function () {
    Route::get('/', 'Admin\PegawaiController@index');
    Route::get('/show', 'Admin\PegawaiController@show');
    Route::get('/form', 'Admin\PegawaiController@form');
    Route::get('/form/{id}', 'Admin\PegawaiController@form');
    Route::post('/', 'Admin\PegawaiController@store');
    Route::post('/{id}', 'Admin\PegawaiController@update');
    Route::get('hapus/{id}', 'Admin\PegawaiController@destroy');
    Route::get('cekgp/{id}', 'Admin\PegawaiController@cekgp');   
});

Route::group(['prefix' => 'penggajian'], function () {
    Route::get('/', 'Admin\PenggajianController@index');
    Route::get('/show', 'Admin\PenggajianController@show');
    Route::get('/showdetail/{id}', 'Admin\PenggajianController@showdetail');
    Route::get('/form', 'Admin\PenggajianController@form');
    Route::get('/form/{id}', 'Admin\PenggajianController@form');
    Route::post('/', 'Admin\PenggajianController@store');
    Route::post('/{id}', 'Admin\PenggajianController@update');
    Route::get('hapus/{id}', 'Admin\PenggajianController@destroy');
    Route::get('generate', 'Admin\PenggajianController@generate');
    Route::get('generategaji/{periode}', 'Admin\PenggajianController@generategaji');
    Route::get('gaji/{periode}', 'Admin\PenggajianController@gaji');
    Route::get('/slip', 'Admin\PenggajianController@slip');
    Route::get('/laporan', 'Admin\PenggajianController@laporan');
    Route::get('/showslip', 'Admin\PenggajianController@showslip');
});

Route::group(['prefix' => 'tunjangantetap'], function () {
    Route::get('/', 'Admin\TunjanganTetapController@index');
    Route::get('/show', 'Admin\TunjanganTetapController@show');
    Route::get('/form', 'Admin\TunjanganTetapController@form');
    Route::get('/form/{id}', 'Admin\TunjanganTetapController@form');
    Route::post('/', 'Admin\TunjanganTetapController@store');
    Route::post('/{id}', 'Admin\TunjanganTetapController@update');
    Route::get('hapus/{id}', 'Admin\TunjanganTetapController@destroy');
});

Route::group(['prefix' => 'tunjanganpendidikan'], function () {
    Route::get('/', 'Admin\TunjanganPendidikanController@index');
    Route::get('/show', 'Admin\TunjanganPendidikanController@show');
    Route::get('/form', 'Admin\TunjanganPendidikanController@form');
    Route::get('/form/{id}', 'Admin\TunjanganPendidikanController@form');
    Route::post('/', 'Admin\TunjanganPendidikanController@store');
    Route::post('/{id}', 'Admin\TunjanganPendidikanController@update');
    Route::get('hapus/{id}', 'Admin\TunjanganPendidikanController@destroy');
});

Route::group(['prefix' => 'tunjanganstruktural'], function () {
    Route::get('/', 'Admin\TunjanganStrukturalController@index');
    Route::get('/show', 'Admin\TunjanganStrukturalController@show');
    Route::get('/form', 'Admin\TunjanganStrukturalController@form');
    Route::get('/form/{id}', 'Admin\TunjanganStrukturalController@form');
    Route::post('/', 'Admin\TunjanganStrukturalController@store');
    Route::post('/{id}', 'Admin\TunjanganStrukturalController@update');
    Route::get('hapus/{id}', 'Admin\TunjanganStrukturalController@destroy');
});

Route::group(['prefix' => 'tunjanganfungsional'], function () {
    Route::get('/', 'Admin\TunjanganFungsionalController@index');
    Route::get('/show', 'Admin\TunjanganFungsionalController@show');
    Route::get('/form', 'Admin\TunjanganFungsionalController@form');
    Route::get('/form/{id}', 'Admin\TunjanganFungsionalController@form');
    Route::post('/', 'Admin\TunjanganFungsionalController@store');
    Route::post('/{id}', 'Admin\TunjanganFungsionalController@update');
    Route::get('hapus/{id}', 'Admin\TunjanganFungsionalController@destroy');
});

Route::group(['prefix' => 'potongan'], function () {
    Route::get('/', 'Admin\PotonganController@index');
    Route::get('/show', 'Admin\PotonganController@show');
    Route::get('/form', 'Admin\PotonganController@form');
    Route::get('/form/{id}', 'Admin\PotonganController@form');
    Route::post('/', 'Admin\PotonganController@store');
    Route::post('/{id}', 'Admin\PotonganController@update');
    Route::get('hapus/{id}', 'Admin\PotonganController@destroy');
    Route::get('export', 'Admin\PotonganController@export');
    Route::post('import/excel', 'Admin\PotonganController@import');
});

Route::group(['prefix' => 'periode'], function () {
    Route::get('/', 'Admin\PeriodeController@index');
    Route::get('/show', 'Admin\PeriodeController@show');
    Route::get('/form', 'Admin\PeriodeController@form');
    Route::get('/form/{id}', 'Admin\PeriodeController@form');
    Route::post('/', 'Admin\PeriodeController@store');
    Route::post('/{id}', 'Admin\PeriodeController@update');
    Route::get('hapus/{id}', 'Admin\PeriodeController@destroy');
});

Route::group(['prefix' => 'insentifkehadiran'], function () {
    Route::get('/', 'Admin\InsentifKehadiranController@index');
    Route::get('/show', 'Admin\InsentifKehadiranController@show');
    Route::get('/form', 'Admin\InsentifKehadiranController@form');
    Route::get('/form/{id}', 'Admin\InsentifKehadiranController@form');
    Route::post('/', 'Admin\InsentifKehadiranController@store');
    Route::post('/{id}', 'Admin\InsentifKehadiranController@update');
    Route::get('hapus/{id}', 'Admin\InsentifKehadiranController@destroy');
});

Route::group(['prefix' => 'kepanitiaan'], function () {
    Route::get('/', 'Admin\KepanitiaanController@index');
    Route::get('/show', 'Admin\KepanitiaanController@show');
    Route::get('/form', 'Admin\KepanitiaanController@form');
    Route::get('/form/{id}', 'Admin\KepanitiaanController@form');
    Route::post('/', 'Admin\KepanitiaanController@store');
    Route::post('/{id}', 'Admin\KepanitiaanController@update');
    Route::get('hapus/{id}', 'Admin\KepanitiaanController@destroy');   
    Route::get('sub/{id}', 'Admin\KepanitiaanController@sub');

    Route::get('/showpanitia', 'Admin\KepanitiaanController@showpanitia');
    Route::get('/formpanitia', 'Admin\KepanitiaanController@formpanitia');
    Route::get('/formpanitia/{id}', 'Admin\KepanitiaanController@formpanitia');
    Route::post('formpanitia/', 'Admin\KepanitiaanController@storepanitia');
    Route::post('formpanitia/{id}', 'Admin\KepanitiaanController@updatepanitia');
    Route::get('hapuspaniti/{id}', 'Admin\KepanitiaanController@destroypanitia'); 

});

Route::group(['prefix' => 'transaksikepanitiaan'], function () {
    Route::get('/', 'Admin\TransaksiKepanitiaanController@index');
    Route::get('/show', 'Admin\TransaksiKepanitiaanController@show');
    Route::get('/form', 'Admin\TransaksiKepanitiaanController@form');
    Route::get('/form/{id}', 'Admin\TransaksiKepanitiaanController@form');
    Route::post('/', 'Admin\TransaksiKepanitiaanController@store');
    Route::post('/{id}', 'Admin\TransaksiKepanitiaanController@update');
    Route::get('hapus/{id}', 'Admin\TransaksiKepanitiaanController@destroy');
    Route::get('sub/{id}', 'Admin\TransaksiKepanitiaanController@sub');   
    Route::get('jabatan/{id}', 'Admin\TransaksiKepanitiaanController@jabatan');   
});

Route::group(['prefix' => 'approvalkepanitiaan'], function () {
    Route::get('/', 'Admin\ApprovalKepanitiaanController@index');
    Route::get('/show', 'Admin\ApprovalKepanitiaanController@show');
    Route::get('/form', 'Admin\ApprovalKepanitiaanController@form');
    Route::get('/form/{id}', 'Admin\ApprovalKepanitiaanController@form');
    Route::post('/', 'Admin\ApprovalKepanitiaanController@store');
    Route::post('/{id}', 'Admin\ApprovalKepanitiaanController@update');
    Route::get('hapus/{id}', 'Admin\ApprovalKepanitiaanController@destroy');
    Route::get('approve/{id}', 'Admin\ApprovalKepanitiaanController@approve');
});

Route::group(['prefix' => 'transaksikehadiran'], function () {
    Route::get('/', 'Admin\TransaksiKehadiranController@index');
    Route::get('/show', 'Admin\TransaksiKehadiranController@show');
    Route::get('/form', 'Admin\TransaksiKehadiranController@form');
    Route::get('/form/{id}', 'Admin\TransaksiKehadiranController@form');
    Route::post('/', 'Admin\TransaksiKehadiranController@store');
    Route::post('/{id}', 'Admin\TransaksiKehadiranController@update');
    Route::get('hapus/{id}', 'Admin\TransaksiKehadiranController@destroy');  
    Route::get('export', 'Admin\TransaksiKehadiranController@export');
    Route::post('import/excel', 'Admin\TransaksiKehadiranController@import');
});


Route::group(['prefix' => 'approvalkehadiran'], function () {
    Route::get('/', 'Admin\ApprovalKehadiranController@index');
    Route::get('/show', 'Admin\ApprovalKehadiranController@show');
    Route::get('/form', 'Admin\ApprovalKehadiranController@form');
    Route::get('/form/{id}', 'Admin\ApprovalKehadiranController@form');
    Route::post('/', 'Admin\ApprovalKehadiranController@store');
    Route::post('/{id}', 'Admin\ApprovalKehadiranController@update');
    Route::get('hapus/{id}', 'Admin\ApprovalKehadiranController@destroy');
    Route::get('approve/{id}', 'Admin\ApprovalKehadiranController@approve');
});

Route::group(['prefix' => 'transaksilembur'], function () {
    Route::get('/', 'Admin\TransaksiLemburController@index');
    Route::get('/show', 'Admin\TransaksiLemburController@show');
    Route::get('/form', 'Admin\TransaksiLemburController@form');
    Route::get('/form/{id}', 'Admin\TransaksiLemburController@form');
    Route::post('/', 'Admin\TransaksiLemburController@store');
    Route::post('/{id}', 'Admin\TransaksiLemburController@update');
    Route::get('hapus/{id}', 'Admin\TransaksiLemburController@destroy');
    Route::get('export', 'Admin\TransaksiLemburController@export');
});

Route::group(['prefix' => 'approvallembur'], function () {
    Route::get('/', 'Admin\ApprovalLemburController@index');
    Route::get('/show', 'Admin\ApprovalLemburController@show');
    Route::get('/form', 'Admin\ApprovalLemburController@form');
    Route::get('/form/{id}', 'Admin\ApprovalLemburController@form');
    Route::post('/', 'Admin\ApprovalLemburController@store');
    Route::post('/{id}', 'Admin\ApprovalLemburController@update');
    Route::get('hapus/{id}', 'Admin\ApprovalLemburController@destroy');
    Route::get('approve/{id}', 'Admin\ApprovalLemburController@approve');
});

Route::group(['prefix' => 'approvalpantauan'], function () {
    Route::get('/', 'Admin\ApprovalPantauanController@index');
    Route::get('/show', 'Admin\ApprovalPantauanController@show');
    Route::get('/form', 'Admin\ApprovalPantauanController@form');
    Route::get('/form/{id}', 'Admin\ApprovalPantauanController@form');
    Route::post('/', 'Admin\ApprovalPantauanController@store');
    Route::post('/{id}', 'Admin\ApprovalPantauanController@update');
    Route::get('hapus/{id}', 'Admin\ApprovalPantauanController@destroy');
    Route::get('approve/{id}', 'Admin\ApprovalPantauanController@approve');
    Route::get('approvemasal/{idperiode}/{idpegawai}', 'Admin\ApprovalPantauanController@approvemasal');
    Route::get('/rekapitulasi', 'Admin\ApprovalPantauanController@rekapitulasi');
});

Route::group(['prefix' => 'pantauanpengajaran'], function () {
    Route::get('/', 'Admin\PantauanPengajaranController@index');
    Route::get('/show', 'Admin\PantauanPengajaranController@show');
    Route::get('/form', 'Admin\PantauanPengajaranController@form');
    Route::get('/form/{id}', 'Admin\PantauanPengajaranController@form');
    Route::get('/formduplicate/{id}', 'Admin\PantauanPengajaranController@formduplicate');
    Route::post('/', 'Admin\PantauanPengajaranController@store');
    Route::post('/{id}', 'Admin\PantauanPengajaranController@update');
    Route::get('hapus/{id}', 'Admin\PantauanPengajaranController@destroy');
    Route::get('export', 'Admin\PantauanPengajaranController@export');
    Route::get('makul/{id}', 'Admin\PantauanPengajaranController@makul');   
    Route::get('makulpp/{id}', 'Admin\PantauanPengajaranController@makulpp');   
    Route::get('kelaspp/{id}', 'Admin\PantauanPengajaranController@kelaspp');   
    Route::get('kelaspegawai/{id}', 'Admin\PantauanPengajaranController@kelaspegawai');   
    Route::get('ceknominal/{id}', 'Admin\PantauanPengajaranController@ceknominal');   
    Route::get('cekmakul/{id}', 'Admin\PantauanPengajaranController@cekmakul');   
    Route::get('rekap', 'Admin\PantauanPengajaranController@rekap');   
    Route::get('rekaptu', 'Admin\PantauanPengajaranController@rekaptu');   
    Route::get('showrekap', 'Admin\PantauanPengajaranController@showrekap');   
    Route::get('showrekaptu', 'Admin\PantauanPengajaranController@showrekaptu');   
});

Route::group(['prefix' => 'pantauanpengajarandtt'], function () {
    Route::get('/', 'Admin\PantauanPengajaranDttController@index');
    Route::get('/show', 'Admin\PantauanPengajaranDttController@show');
    Route::get('/form', 'Admin\PantauanPengajaranDttController@form');
    Route::get('/form/{id}', 'Admin\PantauanPengajaranDttController@form');
    Route::get('/formduplicate/{id}', 'Admin\PantauanPengajaranDttController@formduplicate');
    Route::post('/', 'Admin\PantauanPengajaranDttController@store');
    Route::post('/{id}', 'Admin\PantauanPengajaranDttController@update');
    Route::get('hapus/{id}', 'Admin\PantauanPengajaranDttController@destroy');
    Route::get('export', 'Admin\PantauanPengajaranDttController@export');
    Route::get('makul/{id}', 'Admin\PantauanPengajaranDttController@makul');   
    Route::get('makulpp/{id}', 'Admin\PantauanPengajaranDttController@makulpp');   
    Route::get('kelaspp/{id}', 'Admin\PantauanPengajaranDttController@kelaspp');   
    Route::get('kelaspegawai/{id}', 'Admin\PantauanPengajaranDttController@kelaspegawai');   
    Route::get('ceknominal/{id}', 'Admin\PantauanPengajaranDttController@ceknominal');   
    Route::get('cekmakul/{id}', 'Admin\PantauanPengajaranDttController@cekmakul');   
    Route::get('rekap', 'Admin\PantauanPengajaranDttController@rekap');   
    Route::get('rekaptu', 'Admin\PantauanPengajaranDttController@rekaptu');   
    Route::get('showrekap', 'Admin\PantauanPengajaranDttController@showrekap');   
    Route::get('showrekaptu', 'Admin\PantauanPengajaranDttController@showrekaptu');   
});

Route::group(['prefix' => 'setupmakul'], function () {
    Route::get('/', 'Admin\SetupmakulController@index');
    Route::get('/rekap', 'Admin\SetupmakulController@rekap');
    Route::get('/show', 'Admin\SetupmakulController@show');
    Route::get('/showrekap', 'Admin\SetupmakulController@showrekap');
    Route::get('/form', 'Admin\SetupmakulController@form');
    Route::get('/form/{id}', 'Admin\SetupmakulController@form');
    Route::post('/', 'Admin\SetupmakulController@store');
    Route::get('/printform/{id}', 'Admin\SetupmakulController@printform');
    Route::post('/upload/', 'Admin\SetupmakulController@uploadstore');
    Route::post('/{id}', 'Admin\SetupmakulController@update');
    Route::get('hapus/{id}', 'Admin\SetupmakulController@destroy');
    Route::get('/getchart', 'Admin\SetupmakulController@getchart');
    Route::get('makul/{id}', 'Admin\SetupmakulController@makul');   
    Route::get('makulpp/{id}', 'Admin\SetupmakulController@makulpp');
    Route::get('kelaspp/{id}', 'Admin\SetupmakulController@kelaspp');
    Route::get('cekmakul/{id}', 'Admin\SetupmakulController@cekmakul');
    Route::get('ceksks/{id}', 'Admin\SetupmakulController@ceksks');
    Route::get('flat/{id}', 'Admin\SetupmakulController@flat');
});

Route::group(['prefix' => 'setupnominal'], function () {
    Route::get('/', 'Admin\SetupNominalController@index');
    Route::get('/show', 'Admin\SetupNominalController@show');
    Route::get('/form', 'Admin\SetupNominalController@form');
    Route::get('/form/{id}', 'Admin\SetupNominalController@form');
    Route::post('/', 'Admin\SetupNominalController@store');
    Route::post('/{id}', 'Admin\SetupNominalController@update');
    Route::get('hapus/{id}', 'Admin\SetupNominalController@destroy');
});

Route::group(['prefix' => 'datapegawai'], function () {
    Route::get('/', 'Admin\DataPegawaiController@index');
    Route::get('/show', 'Admin\DataPegawaiController@show');
    Route::get('/form', 'Admin\DataPegawaiController@form');
    Route::get('/form/{id}', 'Admin\DataPegawaiController@form');
    Route::post('/', 'Admin\DataPegawaiController@store');
    Route::post('/{id}', 'Admin\DataPegawaiController@update');
    Route::get('hapus/{id}', 'Admin\DataPegawaiController@destroy');
});

Route::group(['prefix' => 'menus'], function () {
    Route::get('/', 'Userman\MenuController@index');
    Route::get('/show', 'Userman\MenuController@show');
    Route::get('/form', 'Userman\MenuController@form');
    Route::get('/form/{id}', 'Userman\MenuController@form');
    Route::post('/', 'Userman\MenuController@store');
    Route::post('/{id}', 'Userman\MenuController@update');
    Route::get('hapus/{id}', 'Userman\MenuController@destroy');
});

Route::group(['prefix' => 'roles'], function () {
    Route::get('/', 'Userman\RoleController@index');
    Route::get('/show', 'Userman\RoleController@show');
    Route::get('/form', 'Userman\RoleController@form');
    Route::get('/form/{id}', 'Userman\RoleController@form');
    Route::post('/', 'Userman\RoleController@store');
    Route::post('/{id}', 'Userman\RoleController@update');
    Route::get('hapus/{id}', 'Userman\RoleController@destroy');
});

Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'Userman\UsersController@index');
    Route::get('/show', 'Userman\UsersController@show');
    Route::get('/form', 'Userman\UsersController@form');
    Route::get('/form/{id}', 'Userman\UsersController@edit');
    Route::post('/', 'Userman\UsersController@store');
    Route::post('/{id}', 'Userman\UsersController@update');
    Route::get('hapus/{id}', 'Userman\UsersController@destroy');
});
