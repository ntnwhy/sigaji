@extends("base", ['tag' => 'userman.menu'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>

    <script>
        function save(){

            if ($('#nama').val() == ''){
                notifWarning('Isian nama tidak boleh kosong !');
                $('#nama').focus();

                return;
            }

            if ($('#icon').val() == ''){
                notifWarning('Isian Icon tidak boleh kosong !');
                $('#icon').focus();

                return;
            }

            if ($('#url').val() == ''){
                notifWarning('Isian Url tidak boleh kosong !');
                $('#url').focus();

                return;
            }

            if ($('#seq').val() == ''){
                notifWarning('Isian Urutan tidak boleh kosong !');
                $('#seq').focus();

                return;
            }

            if ($('#tag').val() == ''){
                notifWarning('Isian Tag tidak boleh kosong !');
                $('#tag').focus();

                return;
            }

            $.ajax({
                url : "{{ $form['link'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('menus') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('progdi') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('menus') }}" class="breadcrumb-item"> Menu</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nama:</label>
                                <div class="col-lg-9">
                                    <input type="text" value="{{ $form['nama'] }}" class="form-control"
                                           placeholder="Nama" id="nama" name="nama" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Icon:</label>
                                <div class="col-lg-9">
                                    <input type="text" value="{{ $form['icon'] }}" class="form-control"
                                           placeholder="Icon" id="icon" name="icon">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Url:</label>
                                <div class="col-lg-9">
                                    <input type="text" value="{{ $form['url'] }}" class="form-control"
                                           placeholder="Url" id="url" name="url">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Parent Menu:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="menu" id="menu">
                                        <option value="">Pilih Menu</option>
                                        @foreach($menu as $r)
                                        <option value="{{$r->id}}" @if($r->id == $form['menu']) selected @endif>{{ $r->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Tag:</label>
                                <div class="col-lg-9">
                                    <input type="text" value="{{ $form['tag'] }}" class="form-control"
                                           placeholder="Tag" id="tag" name="tag">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Urutan:</label>
                                <div class="col-lg-9">
                                    <input type="number" value="{{ $form['seq'] }}" class="form-control"
                                           placeholder="Urutan" id="seq" name="seq">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Status:</label>
                                <div class="col-lg-9">
                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input-styled" value="true" name="status" @if($form['status'] == true) checked @endif data-fouc>
                                                Aktif
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input-styled" value="false" name="status" @if($form['status'] == false) checked @endif data-fouc>
                                                Tidak Aktif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop