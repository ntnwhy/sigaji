
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title }} - Sistem Informasi </title>

    <link rel="shortcut icon" href="{{ asset('ico.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/extras/animate.min.css') }}" rel="stylesheet" type="text/css">
    @yield("css")

    <script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/layout_fixed_sidebar_custom.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/perfect_scrollbar.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/prism.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script>
        swal.setDefaults({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        function notifWarning(msg) {
            new PNotify({
                title: 'Perhatian !',
                text: msg,
                icon: 'icon-warning22',
                addclass: 'bg-warning border-warning',
                delay: 2000
            });
        }

        function notifSuccess(msg) {
            new PNotify({
                title: 'Berhasil !',
                text: msg,
                icon: 'icon-checkmark3',
                addclass: 'bg-success border-success',
                delay: 2000
            });
        }

        function goBlock(b){
            $.blockUI({
                showOverlay: b,
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }});
        }

    </script>
    @yield("js")

</head>

<body class="navbar-top">

<div class="navbar navbar-expand-md navbar-dark fixed-top">
    <div class="navbar-brand">
        <a href="{{ url('admin') }}" class="d-inline-block">
            <img src="{{ asset('LogoUSB.png') }}" >
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <span class="navbar-text ml-md-3 mr-md-auto">
				<span class="badge bg-success">Online</span>
			</span>

        <ul class="navbar-nav">

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('LogoUSB.png') }}" class="rounded-circle" alt="">
                    <span>{{ Auth::user()->name }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-divider"></div>
                    <a href="{{ url('admin/profil') }}" class="dropdown-item"><i class="icon-cog5"></i> Akun</a>
                    <a href="#" class="dropdown-item"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="icon-switch2"></i> Logout</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="page-content">

    <div class="sidebar sidebar-dark sidebar-main sidebar-fixed sidebar-expand-md">

        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>

        <div class="sidebar-content">

            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><img src="{{ asset('global_assets/images/placeholders/placeholder.jpg') }}" width="38" height="38" class="rounded-circle" alt=""></a>
                        </div>

                        <div class="media-body">
                            <div class="media-title font-weight-semibold">{{ Auth::user()->name }}</div>
                            <!-- <div class="font-size-xs opacity-50">
                                <i class="icon-pin font-size-sm"></i> &nbsp;{{ Auth::user()->akses }}
                            </div> -->
                        </div>

                    </div>
                </div>
            </div>

            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>

                    @foreach($menu as $r)
                        @if(count($r->sub))

                            <li class="nav-item nav-item-submenu {{ setActive($r->tag, $tag, 'nav-item-expanded nav-item-open') }}">
                                <a href="#" class="nav-link"><i class="{{ $r->icon }}"></i> <span>{{ $r->nama }}</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                                    @foreach($r->sub as $sub)

                                        <li class="nav-item"><a href="{{ url($sub->url) }}" class="nav-link {{ setActive($sub->tag, $tag) }}">{{ $sub->nama }}</a></li>

                                    @endforeach

                                </ul>
                            </li>

                        @else

                            <li class="nav-item">
                                <a href="{{ url($r->url) }}" class="nav-link {{ setActive($r->tag, $tag) }}">
                                    <i class="{{ $r->icon }}"></i>
                                    <span>{{ $r->nama }}</span>
                                </a>
                            </li>

                        @endif
                    @endforeach

                </ul>
            </div>

        </div>

    </div>

    <div class="content-wrapper">

        @yield("content")

        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; {{ date('Y') }}. <a href="#">Anton Wahyu Nugroho</a>
					</span>

            </div>
        </div>
    </div>

</div>

</body>
</html>
