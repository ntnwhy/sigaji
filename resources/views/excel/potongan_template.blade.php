<table>
    <thead>
    <tr>
        <th>no</th>
        <th>ref_id_pegawai</th>
        <th>nama</th>
        <th>potongan</th>
        <th>nominal</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data['temp'] as $r)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $r->id }}</td>            
            <td>{{ $r->datapegawai['nama'] }}</td>
            <td>{{ $data['potongan'] }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>