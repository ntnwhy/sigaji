@extends("base", ['title'   => 'Dashboard', 'tag' => 'home'])

@section("js")

@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">Dashboard</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">

        <div class="row">
            <div class="col-md-12">
                <!-- dblue -->
                <!-- <a href="#" class="btn btn-primary btn-lg" role="button"><i class="fab fa-dollar"></i><i class="fab fa-500px"></i> <br />Master  <br />Pendidikan</a>    -->             
            </div>
        </div>
    
    </div>

@stop