
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title }} - Sistem Informasi Pemetaan Mata Kuliah  </title>

    <link rel="shortcut icon" href="{{ asset('ico.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/css/extras/animate.min.css') }}" rel="stylesheet" type="text/css">
    @yield("css")

    <script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/layout_fixed_sidebar_custom.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/perfect_scrollbar.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/prism.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <script src="{{ asset('assets/js/app.js') }}"></script>
        <script>
        window.print();
    </script>

</head>

<body>

    <div class="row">
        <div class="col-md-12" align="center">        
            <h3>{{ $title }}</h3>
            <h3>{{ $semester }}</h3>
            <h3>{{ $ta }}</h3>
        </div>

    </div>

    <div class="row">
        <div class="col-md-9">
            <table class="table table-sm" border="1" width="100%">
                <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kode |<br>Mata Kuliah</th>
                    <th colspan="2">SKS</th>
                    <th rowspan="2">Jml<br>Kel</th>
                    <th rowspan="2">SKS x<br>Kel</th>
                    <th rowspan="2">Smt</th>
                    <th rowspan="2">Kode<br>Jur</th>
                    <th rowspan="2">Paraf<br>Kaprodi</th>
                </tr>
                <tr>
                    <th>T</th>
                    <th>P</th>
                </tr>
                </thead>
                <tbody>
                    @php($sumt=0) 
                    @php($sump=0) 
                    @php($sumkali=0) 
                    @foreach($detail as $row)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>
                            {{ $row->makul->KodeMataKuliah }} | {{ $row->makul->NamaMataKuliah }} 
                        </td>
                        <td>
                            @if($row->is_teori == true)

                                @php($t=$row->sks_teori)
                            @else
                                @php($t=0)
                            @endif

                                {{ $t }}

                                @php($sumt+=$t)
                        </td>
                        <td>

                            @if($row->is_praktek == true)

                                @php($p=$row->sks_praktek)
                            @else
                                @php($p=0)
                            @endif
                                {{ $p }}

                            @php($sump+=$p)



                        </td>
                        <td>{{ $row->jml }}</td>
                        <td>
                            @php($skskali=($t + $p) * $row->jml)
                            {{ $skskali }}
                            @php($sumkali+=$skskali)

                        </td>
                        <td>{{ $row->makul->Semester }}</td>
                        <td>{{ $row->proyekpendidikan->IdProgramStudi }}</td>
                        <td></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td><p></p></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>                      
                    </tr>
                    <tr>
                        <td><p></p></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><p></p></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>                       
                        <td colspan="2"><b> Jumlah Total SKS Mengajar </b></td>
                        <td>{{$sumt}}</td>
                        <td>{{$sump}}</td>
                        <td></td>
                        <td>{{$sumkali}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2"><b> Jabatan Struktural </b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>            
        </div>
        <div class="col-md-3">
            <small>Kode Jurusan</small><br>
            @foreach($prodi as $rows)
                <small>{{$rows->IdProgramStudi}} : {{$rows->NamaProgramStudi}}</small><br>
            @endforeach   

        </div>
        
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4" align="center">
            <br>
            <p>Menyetujui,</p>
            <br>
            <br>
            <br>
            <p>Dr. Dra. Peni Pujiastuti, M.Si. </p>
            <p>Wakil Rektor I</p>
        </div>
        <div class="col-md-4" align="center">
            <br>
            <p>Mengetahui,</p>
            <br>
            <br>
            <br>
            <p> ................................ </p>
            <p>Dekan Fakultas</p>
        </div>
        <div class="col-md-4" align="center">
            Surakarta, <br>
            <p>Dosen Pengampu,</p>
            <br>
            <br>
            <br>
            <p> {{ $nama }} </p>
            <p></p>

        </div>
        
    </div>

    <div class="row">
        <div class="col-md-12">
            <p><strong>Keterangan : </strong></p>
            <p>1. Formulir dikumpulkan paling lambat tanggal {{ $tgl_kumpul }} ke Program Studi untuk dilakukan verifikasi.</p>
            <p>2. Setelah diverifikasi Program Studi dan mendapatkan tanda tangan Dekan Fakultas, maka formulir diserahkan ke Wakil Rektor I paling lambat tanggal {{ $tgl_serah }} dan digunakan sebagai acuan penentuan honorarium .</p>
        </div>       
    </div>

</body>
</html>