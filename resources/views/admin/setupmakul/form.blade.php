@extends("base", ['title'   => 'SetupMakul', 'tag' => 'setupmakul'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/pickers/anytime.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>
        
        $(document).ready(function() {

            $('.select').select2({
                allowClear: true
            });

            if($('#proyekpendidikan').val()){ambilMakulpp()};               
            
        });

        function ambilMakulpp() {

            var id = $('#proyekpendidikan').val();
            
            $.ajax({
                url : "{{ url('setupmakul/makulpp') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var str = '<select class="form-control select" onchange="cekmk()" name="matakuliah" id="matakuliah">\n' +
                        '<option value="0">Pilih Mata Kuliah</option>';

                    var data = respon.data;
                    for (var i=0; i<data.length; i++){     
                        var tmp = data[i];                                         

                        str += '<option value="'+ tmp.detailmakul.IdMataKuliah +'">'+ tmp.detailmakul.KodeMataKuliah +' | '+ tmp.detailmakul.NamaMataKuliah +' | SKS = '+ tmp.detailmakul.TotalSKS +'</option>';
                    }

                    str += '</select>';

                    $('#matakuliah').html(str);
                    gasmakul();
                    if($('#matakuliah').val()){ambilkelaskuliah()};

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function gasmakul(){

            $('#matakuliah').val($('#idmk').val());

        }

        function ceksks(){

            var id = $('#matakuliah').val();

            $.ajax({
                url : "{{ url('setupmakul/cekmakul') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var data = respon.data[0];
                    
                    if(data.TipeMataKuliah == 'Teori'){
                        $('#cbteori').prop('checked', true);
                        $('#cbpraktek').prop('checked', false);
                        $('#sksteori').val(data.SKSTeori);

                    } else if(data.TipeMataKuliah == 'Praktek'){
                        $('#cbteori').prop('checked', false);
                        $('#cbpraktek').prop('checked', true);
                        $('#skspraktek').val(data.SKSPraktek);

                    } else {
                        $('#cbteori').prop('checked', true);
                        $('#cbpraktek').prop('checked', true);
                        $('#sksteori').val(data.SKSTeori);
                        $('#skspraktek').val(data.SKSPraktek);
                    }
                    

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });


        }

        function ambilkelaskuliah() {      

            ceksks();

            var idpp = $('#proyekpendidikan').val();
            var idmk = $('#matakuliah').val();

            var id = idpp+"-"+idmk ;
            
            $.ajax({
                url : "{{ url('setupmakul/kelaspp') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var str = '<select class="form-control select" multiple="multiple" name="kelaskuliah[]" id="kelaskuliah">';

                    var data = respon.data[0].detailkelas;
                    for (var i=0; i<data.length; i++){     
                        var tmp = data[i];                                         

                        str += '<option value="'+ tmp.KodeKelas +'" >'+ tmp.KodeKelas +' </option>';

                    }

                    str += '</select>';

                    $('#kelaskuliah').html(str);
                    gaskelas();

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function gaskelas(){

            $('#kelaskuliah').val($('#idkelas').val());

        }

        function save(){

            if ($('#matakuliah').val() == '0'){
                notifWarning('Isian matakuliah tidak boleh kosong !');
                $('#matakuliah').focus();
                return;
            }

            if ($('#kelaskuliah').val() == '0'){
                notifWarning('Isian kelaskuliah tidak boleh kosong !');
                $('#kelaskuliah').focus();
                return;
            }

            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('setupmakul') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('tunjanganfungsional') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('peserta') }}" class="breadcrumb-item">{{ $title }}</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">ID Edumanage : </label>
                                <div class="col-lg-9">
                                    <input type="text" value="{{ $form['idedu'] }} " class="form-control" id="idedu" name="idedu" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nama : </label>
                                <div class="col-lg-9">
                                    
                                    <select class="form-control select" name="pegawai" id="pegawai">
                                        @foreach($pegawai as $row)
                                            <option value="{{ $row->id }}" >{{ $row->datapegawai['nama'] }}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Tahun Akademik : </label>
                                <div class="col-lg-9">
                                    <select class="form-control select" name="tahunakademik" id="tahunakademik">
                                        @foreach($tahunakademik as $row)
                                            <option value="{{ $row->id }}" > {{ $row->tahun_akademik }} {{ $row->semester }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Proyek Pendidikan:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" onchange="ambilMakulpp()" name="proyekpendidikan" id="proyekpendidikan">
                                        <option value="0"> Pilih Proyek Pendidikan</option>  
                                        @foreach($proyekpendidikan as $row)
                                            <option value="{{ $row->IdProyekPendidikan }}" @if( $form['proyekpendidikan'] == $row->IdProyekPendidikan ) selected @endif> {{ $row->ProyekPendidikan }} | {{ $row->prodi['NamaProgramStudi'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Mata Kuliah:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" name="matakuliah" onchange="ambilkelaskuliah()" id="matakuliah">
                                                                            
                                    </select>     
                                    <input type="hidden" class="form-control" name="idmk" onchange="gasmakul()" id="idmk" value="{{ $form['matakuliah'] }}" >                               
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Kelas Kuliah:</label>
                                <div class="col-lg-9">
                                   <select class="form-control select" multiple="multiple" name="kelaskuliah[]" id="kelaskuliah">
                                                                       
                                    </select>  
                                    <input type="hidden" class="form-control" name="idkelas" onchange="gaskelas()" id="idkelas" value="{{ $form['kelaskuliah'] }}" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label"></label>
                                <div class="col-lg-3">
                                   <input type="checkbox" name="cbteori" id="cbteori"> Teori 
                                </div>
                                <div class="col-lg-6">
                                   <input type="checkbox" name="cbpraktek" id="cbpraktek"> Praktek 
                                </div>
                            </div>    

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">SKS </label>

                                <label class="col-lg-1 col-form-label">SKS Teori </label>
                                <div class="col-lg-2">
                                   <input type="number" name="sksteori" id="sksteori" step="0.1">  
                                </div>

                                <label class="col-lg-1 col-form-label">SKS Praktek</label>
                                <div class="col-lg-2">
                                   <input type="number" name="skspraktek" id="skspraktek" step="0.1">  
                                </div>
                            </div> 

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop