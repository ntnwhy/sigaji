@extends("base", ['title'   => 'SetupMakul', 'tag' => 'setupmakul'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script>
        var table;

        $(document).ready(function() {

            $('.select').select2({
                allowClear: true
            });

            initTable();           


        });


        function initTable(){

            $.extend( $.fn.dataTable.defaults, {
                autoWidth: true,
                ordering: true,
                columnDefs: [
                    {
                        width: 100,
                        targets: [ 0 ]
                    }
                ],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'Awal', 'last': 'Akhir', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 10, 25, 50, 75, 100, 500, 1000 ],
                displayLength: 25,
                buttons: [
                    
                ]
            });

            table = $('.datatable-basic').DataTable({
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
         
                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                       
                    var Total = api
                        .column(6)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    
                        
                    // Update footer by showing the total with the reference of the column index 
                    $( api.column( 5 ).footer() ).html('Total');
                    $( api.column( 6 ).footer() ).html(Total.toFixed(2));
                    
                    ceksks(Total.toFixed(2));
                    

                },
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{url('setupmakul/show')}}",
                    "type": "GET",
                    "data": {
                        tahunakademik: function () { return $('#tahunakademik').val(); },
                        pegawai: function () { return $('#pegawai').val(); }

                    },
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [   
                    { 'data': 'proyekpendidikan.ProyekPendidikan'},
                    { 'data': 'makul.NamaMataKuliah'},
                    { 'data': 'jml'},
                    { 
                        "data": null,
                        render: function ( data, type, full, meta ) {

                            var tp = '';

                            if(full['is_teori'] == 1){

                                tp += ' T ';

                            } 

                            if(full['is_praktek'] == 1){ 

                                tp += ' P ';

                            } 

                            return tp;

                        }
                    },
                    { 'data': 'jmlt'},
                    { 'data': 'jmlp'},
                    { "data": 'jmls'},
                    {
                        "data": null,
                        render: function ( data, type, full, meta ) {     

                            idta = full['ref_id_tahunakademik'];
                            idpg = full['ref_id_pegawai'];
                            idpp = full['ref_id_pp'];
                            idmk = full['ref_id_makul'];
                    

                            if(full['is_flat'] == false){


                               return '<button class="btn btn-danger"onclick="flat(idta='+idta+',idpg='+idpg+',idpp='+idpp+',idmk='+idmk+')" class="dropdown-item"><i class="icon-exclamation"></i> </button>';                        
                                
                            }  else {

                                return 'Mata Kuliah Flat';
                            }

                        }
                    }
                ]
            });

        }


        function reload_table(){
            table.ajax.reload(null, false);
        }


        function ceksks(sks){

            var id = $('#pegawai').val();

            $.ajax({
                url : "{{ url('setupmakul/ceksks') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){

                    var data = respon.data[0].struktural;   

                    if(data == null){
                        var sksj = 0;
                    } else {
                        var sksj = data.sks;                        
                    }

                    var tsks = parseInt(sksj) + Number(sks);

                    $('#sksj').val(sksj);
                    $('#tsks').val(tsks.toFixed(2));
                                   

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function flat(idta,idpg,idpp,idmk){

            id = idta +"-"+ idpg +"-"+ idpp +"-"+ idmk;

            swal({
                title: 'Apakah anda yakin?'+id,
                text: "Untuk setting flat data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Simpan!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: true,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('setupmakul/flat') }}/"+id,
                        type: "GET",
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }

    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="/"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">{{ $title }}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Filter Data</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>

                </div>

            </div>

            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tahun Akademik:</label>
                            <select class="form-control select" onchange="reload_table()" data-placeholder="Pilih Tahun Akademik" name="tahunakademik" id="tahunakademik" data-fouc>                                
                                @foreach($tahunakademik as $row)
                                    <option value="{{ $row->id }}" > {{ $row->tahun_akademik }} {{ $row->semester }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pegawai:</label>
                            <select class="form-control select" onchange="reload_table()" data-placeholder="Pilih Pegawai" name="pegawai" id="pegawai" data-fouc>                                
                                @foreach($pegawai as $row)
                                    <option value="{{ $row->id }}" > {{ $row->nama }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data {{ $title }}</h5>             
                <div class="d-flex justify-content-center">
                    <button onclick="reload_table()" class="btn btn-link btn-float text-default"><i class="icon-database-refresh text-primary"></i><span>Refresh</span></button>                    
                </div>


            </div>

            <div class="card-body">
                <table class="table table-striped datatable-basic">
                    <thead>
                    <tr>
                        <th>Proyek Pendidikan</th>
                        <th>Mata Kuliah</th>
                        <th>Jumlah Kelas</th>
                        <th>T / P</th>
                        <th>SKS Teori </th>
                        <th>SKS Praktek </th>
                        <th>SKS Diampu </th>
                        <th>Aksi </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>SKS Jabatan</th>
                            <th><input type="text" class="form-control" name="sksj" id="sksj" readonly></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><b>Total SKS Semester</b></th>
                            <th><input type="text" class="form-control" name="tsks" id="tsks" readonly></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
                
            </div>
        </div>

    </div>

@stop