@extends("base", ['tag' => 'transaksi.pantauanpengajarandtt'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/pickers/anytime.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>        

        $(document).ready(function() {

            $('.select').select2({
                allowClear: true
            });

            $('.tgl').AnyTime_picker({
                format: '%d/%m/%Z',
                autoclose: true
            });

            if($('#idmk').val()){cekmk();}

        });

        function ambilMakulsetup() {

            var id = $('#pegawai').val();
            
            $.ajax({
                url : "{{ url('pantauanpengajarandtt/makul') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var str = '<select class="form-control select" onchange="cekmk()" name="matakuliah" id="matakuliah">\n' +
                        '<option value="0">Pilih Mata Kuliah</option>';

                    var data = respon.data;
                    for (var i=0; i<data.length; i++){
                        var tmp = data[i];
                        if(tmp.tahunakademik.status == true){

                        str += '<option value="'+ tmp.makul.IdMataKuliah  +'-'+ tmp.is_flat  +'"> '+ tmp.makul.NamaMataKuliah +' </option>';

                        }
                    }

                    str += '</select>';

                    $('#matakuliah').html(str);

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function ambilMakulpp() {

            var id = $('#proyekpendidikan').val();
            
            $.ajax({
                url : "{{ url('pantauanpengajarandtt/makulpp') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var str = '<select class="form-control select" onchange="cekmk()" name="matakuliah" id="matakuliah">\n' +
                        '<option value="0">Pilih Mata Kuliah</option>';

                    var data = respon.data;
                    for (var i=0; i<data.length; i++){     
                        var tmp = data[i];                                         

                        str += '<option value="'+ tmp.detailmakul.IdMataKuliah +'">'+ tmp.detailmakul.KodeMataKuliah +' | '+ tmp.detailmakul.NamaMataKuliah +' </option>';

                    }

                    str += '</select>';

                    $('#matakuliah').html(str);

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function ambilkelaskuliah() {

            var idpp = $('#proyekpendidikan').val();
            var idmk = $('#matakuliah').val();

            var id = idpp+"-"+idmk
            
            $.ajax({
                url : "{{ url('pantauanpengajarandtt/kelaspp') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var str = '<select class="form-control select"  name="kelaskuliah" id="kelaskuliah">\n' +
                        '<option value="0">Pilih Kelas Kuliah</option>';

                    var data = respon.data[0].detailkelas;
                    for (var i=0; i<data.length; i++){     
                        var tmp = data[i];                                         

                        str += '<option value="'+ tmp.KodeKelas +'" >'+ tmp.KodeKelas +' </option>';

                    }

                    str += '</select>';

                    $('#kelaskuliah').html(str);

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function cekmk() {

            ambilkelaskuliah();

            var makul = $('#matakuliah').val();
            var splitmakul = makul.split("-");
            var id = splitmakul[0];
            var flat = splitmakul[1];
            
            $('#is_flat').val(flat);

            $.ajax({
                url : "{{ url('pantauanpengajarandtt/cekmakul') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    var detail = respon.data[0];

                   $('#idmk').val(detail.IdMataKuliah);

                   $('#sks').val('');

                   if(detail.TipeMataKuliah == "Teori"){

                    $('#tipe_makul').val('Teori');
                    $('#tipe_makul option:not(:selected)').attr('disabled', true);

                   } else if(detail.TipeMataKuliah == "Praktek"){
                    
                    $('#tipe_makul').val('Praktek');
                    $('#tipe_makul option:not(:selected)').attr('disabled', true);

                   } else if(detail.TipeMataKuliah == "Teori dan Praktek"){
                   
                    
                    $('#tipe_makul').val('Teori dan Praktek');
                    $('#tipe_makul option:not(:selected)').attr('disabled', false);                    
                    $('#tipe_makul option:selected').attr('disabled', true);                    
                    $('#sksbantu').val(detail.SKSTeori+'-'+detail.SKSPraktek);

                   }                 

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function gasmakul(){


            $('#matakuliah').val($('#idmk').val());

        }

        function ceksks(){

            var sks =  $('#sksbantu').val();
            var res = sks.split("-");

            if($('#tipe_makul').val() == 'Teori'){

                 $('#sks').val(res[0]);
            } else if($('#tipe_makul').val() == 'Praktek') {
                $('#sks').val(res[1]);
            } else {
                $('#sks').val('');
            }


        }

        function idmakul(){


            $('#idmk').val($('#matakuliah').val());

        }

        function save(){

            if($('#periode').val() == 0){
                notifWarning('Isian Periode Penggajian tidak boleh kosong !');
                $('#periode').focus();

                return;
            }

            if($('#pegawai').val() == 0){
                notifWarning('Isian Dosen tidak boleh kosong !');
                $('#pegawai').focus();

                return;
            }

            if($('#idmk').val() == ""){

                if($('#matakuliah').val() == ""){
                    notifWarning('Isian Mata Kuliah tidak boleh kosong !');
                    $('#matakuliah').focus();

                    return;
                }
            }

            if($('#tipe_makul').val() == ""){
                notifWarning('Isian Tipe Mata Kuliah tidak boleh kosong !');
                $('#tipe_makul').focus();

                return;
            }

            if($('#kelaskuliah').val() == ""){
                notifWarning('Isian Kelas Kuliah tidak boleh kosong !');
                $('#kelaskuliah').focus();

                return;
            }


            if($('#tanggal').val() == ""){
                notifWarning('Isian Tanggal Ajar tidak boleh kosong !');
                $('#tanggal').focus();

                return;
            }

            if($('#durasi').val() == 0){
                notifWarning('Isian Durasi Ajar tidak boleh kosong !');
                $('#durasi').focus();

                return;
            }
          
            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('pantauanpengajarandtt') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }


    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('pantauanpengajarandtt') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('pantauanpengajarandtt') }}" class="breadcrumb-item"> Pantauan Pengajaran</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf 

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Periode :</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="periode" id="periode"> 
                                        @foreach($periode as $row)
                                        <option value="{{ $row->id }}"> {{ $row->bulan }} {{ $row->tahun }} </option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>  

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nama Pegawai:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" name="pegawai" id="pegawai">
                                        <option value="0">Pilih Pegawai</option>  
                                        @foreach($pegawai as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['pegawai']) selected @endif >
                                        {{ $row->datapegawai['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Proyek Pendidikan:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" onchange="ambilMakulpp()" name="proyekpendidikan" id="proyekpendidikan">
                                        <option value="0"> Pilih Proyek Pendidikan</option>   
                                        
                                        @foreach($proyekpendidikan as $row)
                                        <option value="{{ $row->IdProyekPendidikan }}" @if( $form['proyekpendidikan'] == $row->IdProyekPendidikan ) selected @endif> {{ $row->ProyekPendidikan }} | {{ $row->prodi['NamaProgramStudi'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Mata Kuliah:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" name="matakuliah" onchange="cekmk()" id="matakuliah">
                                        <option value="{{ $form['matakuliah'] }}">
                                            @if($form['matakuliah'])         
                                                {{ $form['kelaskuliah'] }}
                                            @else
                                                 Pilih Mata Kuliah       
                                            @endif
                                        </option>                                        
                                    </select>                                
                                    <input type="hidden" class="form-control" name="idmk" onchange="gasmakul()" id="idmk" value="{{ $form['matakuliah'] }}" >
                                    <input type="hidden" class="form-control" name="is_flat" id="is_flat" >
                                </div>
                            </div>

                            <div class="form-group row">

                                <label class="col-lg-3 col-form-label">Kelas Kuliah:</label>
                                <div class="col-lg-5">
                                   <select class="form-control select" name="kelaskuliah" id="kelaskuliah">
                                        <option value="{{ $form['kelaskuliah'] }}">
                                            @if($form['kelaskuliah'])         
                                                {{ $form['kelaskuliah'] }}
                                            @else
                                                 Pilih Kelas Kuliah        
                                            @endif
                                        </option>                                        
                                    </select>  
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="tipemakul" id="tipemakult" value="Teori" @if($form['tipemakul'] == 'Teori') checked="true" @endif>
                                      <label class="form-check-label" for="tipemakult">Teori</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="tipemakul" id="tipemakulp" value="Praktek" @if($form['tipemakul'] == 'Praktek') checked="true" @endif>
                                      <label class="form-check-label" for="tipemakulp">Praktek</label>
                                    </div>
                                </div>
                            </div>
       
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Tanggal Kehadiran:</label>
                                <div class="col-lg-5">
                                   <input type="text" value="{{ $form['tanggal'] }}" class="form-control tgl" placeholder="Tanggal Pantauan" id="tanggal" name="tanggal" required>
                                </div>

                                <label class="col-lg-2 col-form-label">Durasi Kehadiran (Jam):</label>
                                <div class="col-lg-2">
                                   <select class="form-control select" name="durasi" id="durasi">
                                        <option value="0">Pilih Durasi Mengajar</option>  
                                        @foreach($durasi as $row)
                                        <option value="{{ $row->item_value }}" @if($row->item_value == $form['durasi']) selected @endif>{{ $row->item_display }}  </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop