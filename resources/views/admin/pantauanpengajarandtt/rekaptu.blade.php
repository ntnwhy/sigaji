@extends("base", ['tag' => 'transaksi.pantauanpengajaran'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>
        var table;

        $(document).ready(function() {
            $('.select').select2({
                allowClear: true
            });

            initTable();
        });

        function initTable(){

            table = $('.table-customers').DataTable({
                autoWidth: false,
                ordering: false,
                processing: true,
                serverSide: true,
                columnDefs: [
                    {
                        width: 75,
                        targets: [ 0 ]
                    }
                ],
                order: [[ 0, 'asc' ]],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 25, 50, 75, 100 ],
                displayLength: 25,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ],
                ajax: {
                    "url": "{{url('pantauanpengajaran/showrekaptu')}}",
                    "type": "GET",
                    "data": {
                        periode: function () { return $('#periode').val(); }
                    },
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [                  
                    { 'data': null,
                        render: function (data, type, full, meta) {
                            return full['periode']['bulan'] + " " + full['periode']['tahun'];                            
                        }
                    },
                    { 'data': 'pegawai.nama'},
                    { "data": "makul.KodeMataKuliah" },
                    { "data": "makul.NamaMataKuliah" },
                    { "data": "tipe_makul" },
                    { "data": "kelas_kuliah" },
                    { "data": "sks" },
                    { "data": "total" }
                ]
            });
        }

        function reload_table(){

            table.ajax.reload(null, false);
        }

    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="/"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">
                   
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">{{ $title }}</span>
                </div>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>           

        </div>
    </div>

    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Filter Data</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Periode:</label>
                            <select class="form-control select" onchange="reload_table()" data-placeholder="Pilih Periode"
                                    name="periode" id="periode" data-fouc>
                                
                                @foreach($periode as $r)
                                    <option value="{{$r->id}}">{{$r->bulan}} {{$r->tahun}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data {{ $title }} </h5>

            </div>

            <div class="card-body"></div>

            <table class="table table-striped text-nowrap table-customers">
                <thead>
                <tr>
                    <th>Periode</th>
                    <th>Nama Pegawai</th>
                    <th>Kode MK </th>
                    <th>Mata Kuliah </th>
                    <th>Tipe MK </th>
                    <th>Kelas Kuliah </th>
                    <th>SKS </th>
                    <th>Total Jam</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>


@stop