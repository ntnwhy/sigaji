@extends("base", ['tag' => 'transaksi.pantauanpengajaran'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>
        var table;
        var kelasKuliah = '';

        $(document).ready(function() {
            $('.select').select2({
                allowClear: true
            });

            initTable();
        });

        function initTable(){

            table = $('.table-customers').DataTable({
                autoWidth: false,
                ordering: false,
                processing: true,
                serverSide: true,
                scrollX: true,
                fixedColumns:   {
                    leftColumns: 4
                },
                order: [[ 0, 'asc' ]],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 10, 25, 50, 75, 100 ],
                displayLength: 10,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ],
                ajax: {
                    "url": "{{url('pantauanpengajaran/show')}}",
                    "type": "GET",
                    "data": {
                        periode: function () { return $('#periode').val(); }
                    },
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [
                    {
                        "data": null,
                        render: function ( data, type, full, meta ) {

                            if(full['status'] == 0){

                                return '<div class="list-icons">\n' +
                                    '                            <div class="dropdown">\n' +
                                    '                                <a href="#" class="list-icons-item" data-toggle="dropdown">\n' +
                                    '                                    <i class="icon-menu9"></i>\n' +
                                    '                                </a>\n' +
                                    '\n' +
                                    '                                <div class="dropdown-menu dropdown-menu-right">\n' +
                                    '                                    <a href="{{ url('pantauanpengajaran/form') }}/'+ full['id'] +'" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>\n' +
                                    '                                    <a href="{{ url('pantauanpengajaran/formduplicate') }}/'+ full['id'] +'" class="dropdown-item"><i class="icon-copy"></i> Duplicate</a>\n' +
                                    '                                    <button onclick="haspus('+ full['id'] +')" class="dropdown-item"><i class="icon-trash"></i> Hapus</button>\n' +

                                    '                                </div>\n' +
                                    '                            </div>\n' +
                                    '                        </div>';

                            } else {

                                return '<i class="icon-minus"></i>';

                            }

                        }
                    },
                    { 'data': 'ref_id_periode', "visible": false},
                    { 'data': null,
                        render: function (data, type, full, meta) {
                            return full['periode']['bulan'] + " " + full['periode']['tahun'];                            
                        }
                    },
                    { 'data': 'pegawai.nama'},
                    { "data": "makul.KodeMataKuliah"},
                    { "data": "makul.NamaMataKuliah"},
                    { "data": "kelas_kuliah"},
                    { "data": "tipe_makul"},
                    { "data": "tanggal",
                        render: function (data, type, full, meta) {
                            var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                            var bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

                            var tanggal = new Date(data).getDate();
                            var xhari = new Date(data).getDay();
                            var xbulan = new Date(data).getMonth();
                            var xtahun = new Date(data).getYear();
                            var hari = hari[xhari];
                            var bulan = bulan[xbulan];
                            var tahun = (xtahun < 1000)?xtahun + 1900 : xtahun;

                            return hari +', ' + tanggal + ' ' + bulan + ' ' + tahun
                        }
                    },
                    { "data": "durasi" },
                    { "data": "status",
                        render: function (data, type, full, meta) {
                            if (data == '1'){
                                return '<span class="badge badge-success">Disetujui</span>';
                            }else{
                                return '<span class="badge badge-danger">Belum Disetujui</span>'
                            }
                        }
                    },
                    { "data": "created_by" },
                    { "data": "created_at" }
                ]
            });
        }

        function reload_table(){

            table.ajax.reload(null, false);
        }

        function haspus(id){

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk menghapus data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, hapus!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('pantauanpengajaran/hapus') }}"+"/"+id,
                        type: "GET",
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }

        function generateImporter(){
            if($('#proyek').val() == ''){
                notifWarning('Isian Proyek Pendidikan tidak boleh kosong !');
                $('#proyek').focus();

                return;
            }

            if($('#makul').val() == ''){
                notifWarning('Isian Mata Kuliah tidak boleh kosong !');
                $('#makul').focus();

                return;
            }

            var query = "proyek="+$('#proyek').val()+"&kelas_kuliah="+$('#makul').val();

            var str = '';
            var proyek = $('#proyek').select2('data');
            var makul = $('#makul').select2('data');

            str += proyek[0].text+"_"+makul[0].text;

            query += "&nama="+str;

            window.open("{{url('penilaian/export')}}?"+query, '_blank');
        }

        function showImport(){
            $('#modal_default').modal('show');
        }

        function upload(){
            var input = $('#file')[0];
            if (!input.files && !input.files[0]) {
                notifWarning('File tidak boleh Kosong !');

                return;
            }

            var ukuran = input.files[0].size / 1024 / 1024;
            if (ukuran > 2){
                notifWarning('Ukuran File tidak boleh lebih dari 2 MB');

                return;
            }

            var file = input.files[0];
            var form = new FormData();

            form.append('file', file);
            form.append('_token', $('meta[name="csrf-token"]').attr('content'));

            $.ajax({
                url : "{{ url('/penilaian/import') }}",
                type: "POST",
                data: form,
                contentType: false,
                processData: false,
                dataType: "json",
                beforeSend:function(request) {
                    $('#modal_default').modal('hide');
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    reload_table();

                    $('#file').val(null);

                    swal(
                        'Berhasil !',
                        respon.msg,
                        'success'
                    );

                },error: function (jqXHR, textStatus, errorThrown){
                    $.unblockUI();
                    swal(
                        'Perhatian !!',
                        errorThrown,
                        'danger'
                    );
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="/"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                    <button onclick="reload_table()" class="btn btn-link btn-float text-default"><i class="icon-database-refresh text-primary"></i><span>Refresh</span></button>
                    <a href="{{ url('pantauanpengajaran/form') }}" class="btn btn-link btn-float text-default"><i class="icon-stack-plus text-primary"></i><span>Tambah</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"></i><span></span></a>
                    <a href="{{ url('pantauanpengajarandtt') }}" class="btn btn-link btn-float text-warning"><i class="icon-stack-plus text-warning"></i><span>Tambah DTT</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">{{ $title }}</span>
                </div>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>           

        </div>
    </div>

    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Filter Data</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>

                </div>

            </div>

            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Periode:</label>
                            <select class="form-control select" onchange="reload_table()" data-placeholder="Pilih Periode"
                                    name="periode" id="periode" data-fouc>                                
                                @foreach($periode as $r)
                                    <option value="{{$r->id}}">{{$r->bulan}} {{$r->tahun}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data {{ $title }}</h5>                
                <a href="{{ url('pantauanpengajaran/rekaptu') }}" class="btn btn-success btn-lg"><i class="fas fa-file-excel"> </i> <span>   Rekapitulasi Pantauan DT dan DTT</span></a>
            </div>

            <div class="card-body">
                
            <table class="table table-striped text-nowrap table-customers">
                <thead>
                <tr>
                    <th></th>
                    <th>id</th>
                    <th>Periode</th>
                    <th>Nama</th>
                    <th>Kode MK</th>
                    <th>Mata Kuliah</th>
                    <th>Kelas Kuliah</th>
                    <th>Tipe</th>
                    <th>Tanggal</th>
                    <th>Durasi</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Entered Date</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table> 

            </div>

            
        </div>
    </div>


@stop