@extends("base", ['tag' => 'approvallembur'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

    <script>
        var table;

        $(document).ready(function() {
            $('.select').select2({
                allowClear: true
            });

            initTable();   
           
        });

        function initTable(){

            table = $('.table-customers').DataTable({
                autoWidth: false,
                ordering: false,
                processing: true,
                serverSide: true,
                columnDefs: [
                {
                    width: 75,
                    targets: [0]
                } ],
                order: [[ 0, 'asc' ]],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 25, 50, 75, 100 ],
                displayLength: 25,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ],
                ajax: {
                    "url": "{{url('approvallembur/show')}}",
                    "type": "GET",
                    "data": {
                        periode: function () { return $('#periode').val(); }
                    },
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [
                    {
                        "data": "id",
                        render: function ( data, type, full, meta ) {
                            return ' <button onclick="approve('+data+')" class="btn btn-primary"><i class="far fa-paper-plane"></i><span> Approve</span></button>';
                        }
                    },
                    { 'data': 'periode.bulan'},
                    { 'data': 'periode.tahun'},
                    { 'data': 'pegawai.nama'},
                    { "data": "total_jam" },
                    { "data": "status",
                        render: function (data, type, full, meta) {
                            if (data == '1'){
                                return '<span class="badge badge-success">Disetujui</span>';
                            }else{
                                return '<span class="badge badge-danger">Belum Disetujui</span>'
                            }
                        }
                    },
                ]
            });
        }

        function reload_table(){

            table.ajax.reload(null, false);
        }

        function approve(id){

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk approve data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Approve!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('approvallembur/approve') }}"+"/"+id,
                        type: "GET",
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="/"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                    <button onclick="reload_table()" class="btn btn-link btn-float text-default"><i class="icon-database-refresh text-primary"></i><span>Refresh</span></button>                    
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">{{ $title }}</span>
                </div>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>           

        </div>
    </div>

    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Filter Data</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Periode:</label>
                            <select class="form-control select" data-placeholder="Pilih Periode"
                                    name="periode" id="periode" data-fouc>
                                <option></option>
                                @foreach($periode as $r)
                                    <option value="{{$r->id}}">{{$r->bulan}} {{$r->tahun}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>:</label>
                            <select class="form-control select" data-placeholder="Pilih Jabatan"
                                    name="makul" id="makul" data-fouc>
                                <option value="Dosen Tetap">Dosen Tetap</option>
                                <option value="Pegawai Tetap">Pegawai Tetap</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data {{ $title }}</h5>

            </div>

            <div class="card-body">
               
                <table class="table table-striped text-nowrap table-customers">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>Bulan Periode</th>
                    <th>Tahun Periode</th>
                    <th>Nama Pegawai</th>
                    <th>Jumlah Jam </th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

            </div>
            
        </div>
    </div>


@stop