@extends("base", ['tag' => 'penggajian'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>
        var table;
        var tabledetail;

        $(document).ready(function() {
            $('.select').select2({
                allowClear: true
            });

            initTable();
        });


        function initTable(){
            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                ordering: false,
                columnDefs: [
                    {
                        width: 100,
                        targets: [ 0 ]
                    }
                ],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'Awal', 'last': 'Akhir', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 10, 25, 50, 75, 100, 500, 1000 ],
                displayLength: 25,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ]
            });

            table = $('#tabel').DataTable({
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
         
                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
         
                    // computing column Total of the complete result 
                       
                    var Total = api
                        .column( 2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    
                        
                    // Update footer by showing the total with the reference of the column index 
                $( api.column( 0 ).footer() ).html('Total');
                    $( api.column( 2 ).footer() ).html(Total);
                },
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "ajax": {
                    "url": "{{url('penggajian/show')}}",
                    "type": "GET",
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [                   
                    { 'data': 'datapegawai.nip'},
                    { 'data': 'datapegawai.nama'},
                    { "data": 'gaji'},
                    {
                        "data": function (data, type, dataToSet) {
                            var periode = String(data.ref_id_periode)+'.';
                            var pegawai = String(data.ref_id_pegawai);
                            var key     = periode.concat(pegawai);
                            return ' <button onclick="detail('+key+')" class="btn btn-primary"><i class="far fa-paper-plane"></i><span> Detail Gaji</span></button>';
                        }
                    },
                ],
            });
        };

        function reload_table(){
            table.ajax.reload(null, false);
        }

        function detail(id){
 
            $('#modaldetail').modal('show');

            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                ordering: false,
                columnDefs: [
                    {
                        width: 100,
                        targets: [ 0 ]
                    }
                ],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'Awal', 'last': 'Akhir', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 10, 25, 50, 75, 100, 500, 1000 ],
                displayLength: 25,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ]
            });

            tabledetail = $('#tabeldetail').DataTable({
                "processing": true,
                "destroy": true,
                "serverSide": true,               
                "ajax": {
                    "url": "{{ url('penggajian/showdetail') }}"+"/"+id,
                    "type": "GET",
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [                   
                    { 'data': 'komponen'},
                    { "data": 'nominal'}
                ]
            });
            
        }

       
        function cetakslip(){

            var periode = $('#periode').val();

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk memproses gaji pada periode "+periode+" data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Proses Data Gaji!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('penggajian/gaji') }}"+"/"+periode,
                        type: "GET",
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }

        function generategaji(){

            var periode = $('#periode').val();

            if(!periode){
                swal(
                    'Peringatan !',
                    'Mohon periode penggajian dipilih terlebih dahulu ! ',
                    'warning'
                );
                return false;
            }

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk memproses gaji pada periode "+periode+" data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Proses Data Gaji!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('penggajian/generategaji') }}"+"/"+periode,
                        type: "GET",
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }

    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="/"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                    <button onclick="reload_table()" class="btn btn-link btn-float text-default"><i class="icon-database-refresh text-primary"></i><span>Refresh</span></button>
                    <a href="{{ url('gajipokok/form') }}" class="btn btn-link btn-float text-default"><i class="icon-stack-plus text-primary"></i><span>Tambah</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">{{ $title }}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Proses Data</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Periode:</label>
                            <select class="form-control select" data-placeholder="Pilih Periode"
                                    name="periode" id="periode" data-fouc>
                                <option></option>
                                @foreach($periode as $r)
                                    <option value="{{$r->id}}">{{$r->bulan}} {{$r->tahun}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="text-right">
                   
                    <button onclick="generategaji()" class="btn btn-danger"><i class="icon-download"></i> Generate Gaji</button>
                    <a href="{{ url('penggajian/slip') }}/"><button type="button" class="btn btn-primary rounded-round"><i class="icon-printer"></i> Cetak Slip </button></a>
                    <a href="{{ url('penggajian/laporan') }}/"><button type="button" class="btn btn-primary rounded-round"><i class="icon-printer"></i> Cetak Laporan </button></a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data {{ $title }}</h5>

            </div>

            <div class="card-body"></div>

            <table class="table table-striped text-nowrap datatable-basic" id="tabel" name="tabel">
                <thead>
                <tr>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Gaji</th>
                    <th>Detail</th>

                </tr>
                </thead>
                <tbody></tbody>
                <tfoot align="right">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>

        <!-- Modal  -->
        <div class="modal fade" id="modaldetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Gaji</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped text-nowrap" id="tabeldetail" name="tabeldetail">
  
                            <thead>
                                <tr>
                                    <th>Komponen </th>
                                    <th>Nominal </th>
                                </tr>
                            </thead>
                            <tbody></tbody>                 
                            
                        </table>
                    </div>                    
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

    </div>


@stop