@extends("base", ['tag' => 'pegawai'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>

    <script>

        function save(){

            if ($('#golongan').val() == ''){
                notifWarning('Isian Golongan tidak boleh kosong !');
                $('#golongan').focus();

                return;
            }

    
            // if ($('#nama').val() == ''){
            //     notifWarning('Isian Nama tidak boleh kosong !');
            //     $('#nama').focus();

            //     return;
            // }

            // if ($('#jenis').val() == ''){
            //     notifWarning('Isian Jenis tidak boleh kosong !');
            //     $('#jenis').focus();

            //     return;
            // }

            // if ($('#tipe').val() == ''){
            //     notifWarning('Isian Tipe tidak boleh kosong !');
            //     $('#tipe').focus();

            //     return;
            // }

            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('gajipokok') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('progdi') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('gajipokok') }}" class="breadcrumb-item"> Gaji Pokok</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Golongan:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="golongan" id="golongan">
                                        <option value="">Pilih Golongan</option>
                                        @foreach($golongan as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['golongan']) selected @endif>{{ $row->golongan }} {{ $row->pangkat }}</option>
                                        @endforeach
                                    </select>
                                </div>                                
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Masa Kerja:</label>
                                <div class="col-lg-9">
                                    <input type="number" value="{{ $form['masakerja'] }}" class="form-control" placeholder="Masa Kerja" id="masakerja" name="masakerja" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nominal:</label>
                                <div class="col-lg-9">
                                    <input type="number" value="{{ $form['nominal'] }}" class="form-control" placeholder="Nominal" id="nominal" name="nominal" required>
                                </div>
                            </div>
    

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop