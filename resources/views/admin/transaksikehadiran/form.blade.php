@extends("base", ['tag' => 'transaksi.kehadiran'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>        

        $(document).ready(function() {
            $('.select').select2({
                allowClear: true
            });

        });

        function save(){
          
            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('transaksikehadiran') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }


    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('kepanitiaan') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('transaksikehadiran') }}" class="breadcrumb-item"> Transaksi Kehadiran</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf 

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Periode :</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="periode" id="periode">
                                        @foreach($periode as $row)
                                        <option value="{{ $row->id }}"> {{ $row->bulan }} {{ $row->tahun }} </option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>  

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nama Pegawai:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" name="pegawai" id="pegawai">
                                        <option value="0">Pilih Pegawai</option>  
                                        @foreach($pegawai as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['pegawai']) selected @endif>{{ $row->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Jumlah Hari Kehadiran:</label>
                                <div class="col-lg-3">
                                    <input type="number" class="form-control" name="hari_hadir" id="hari_hadir" min="0" value="{{ $form['hari_hadir'] }}"> 
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text"> Hari <i class="icon-clock"></i></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Jumlah Jam Kehadiran:</label>
                                <div class="col-lg-3">
                                    <input type="number" class="form-control" name="jam_hadir" id="jam_hadir" min="0" value="{{ $form['jam_hadir'] }}"> 
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text"> Jam <i class="icon-clock"></i></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Persentase Kehadiran:</label>
                                <div class="col-lg-3">
                                    <input type="number" class="form-control" name="persentase" id="persentase" min="0" value="{{ $form['persentase'] }}"> 
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text"> % <i class="icon-clock"></i></span>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop