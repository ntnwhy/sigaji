@extends("base", ['tag' => 'transaksi.kehadiran'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>
        var table;

        $(document).ready(function() {
            $('.select').select2({
                allowClear: true
            });

            initTable();
        });

        function initTable(){

            table = $('.table-customers').DataTable({                
                autoWidth: false,
                ordering: false,
                processing: true,
                serverSide: true,
                columnDefs: [
                    {
                        width: 75,
                        targets: [ 0 ]
                    }
                ],
                order: [[ 0, 'asc' ]],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 25, 50, 75, 100 ],
                displayLength: 25,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ],
                ajax: {
                    "url": "{{url('transaksikehadiran/show')}}",
                    "type": "GET",
                    "data": {
                        periode: function () { return $('#periode').val(); },
                        jabatan: function () { return $('#jabatan').val(); },
                        unit: function () { return $('#unit').val(); }
                    },
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [
                    {
                        "data": "id",
                        render: function ( data, type, full, meta ) {
                            return '<div class="list-icons">\n' +
                                '   <div class="dropdown">\n' +
                                '       <a href="#" class="list-icons-item" data-toggle="dropdown">\n' +
                                '       <i class="icon-menu9"></i>\n' +
                                '       </a>\n' +
                                '\n' +
                                '       <div class="dropdown-menu dropdown-menu-right">\n' +
                                '       <a href="{{ url('transaksikehadiran/form') }}/'+ data +'" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>\n' +
                                '       <button onclick="haspus('+data+')" class="dropdown-item"><i class="icon-trash"></i> Hapus</button>\n' +
                                '       </div>\n' +
                                '   </div>\n' +
                                '</div>';
                        }
                    },
                    { 'data': null,
                        render: function (data, type, full, meta) {
                            return full['periode']['bulan'] + " " + full['periode']['tahun'];                            
                        }
                    },
                    { 'data': 'pegawai.unit.kode'},
                    { 'data': 'pegawai.datapegawai.nama'},
                    { "data": "jumlah_hari_kehadiran",
                        render: function (data, type, full, meta) {                            
                            return data +' Hari';                            
                        }
                    },
                    { "data": "jumlah_jam_kehadiran",
                        render: function (data, type, full, meta) {                            
                            return data +' Jam';                            
                        }
                    },
                    { "data": "persentase_kehadiran",
                        render: function (data, type, full, meta) {                            
                            return data +' %';                            
                        }
                    },
                    { "data": "status",
                        render: function (data, type, full, meta) {
                            if (data == '1'){
                                return '<span class="badge badge-success">Disetujui</span>';
                            }else{
                                return '<span class="badge badge-danger">Belum Disetujui</span>'
                            }
                        }
                    },
                ]
            });

        }

        function reload_table(){

            table.ajax.reload(null, false);
        }

        function haspus(id){

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk menghapus data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, hapus!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('transaksikepanitiaan/hapus') }}"+"/"+id,
                        type: "GET",
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }

        function generateImporter(){
            if($('#periode').val() == ''){
                notifWarning('Isian Periode Penggajian tidak boleh kosong !');
                $('#periode').focus();

                return;
            }

            if($('#unit').val() == ''){
                notifWarning('Isian Unit Kerja atau Homebase tidak boleh kosong !');
                $('#unit').focus();

                return;
            }

            var query = "periode="+$('#periode').val()+"&jabatan="+$('#jabatan').val()+"&unit="+$('#unit').val();

            // query += "&nama="+str;

            window.open("{{url('transaksikehadiran/export')}}?"+query, '_blank');
        }

        function showImport(){
            $('#modal_default').modal('show');
        }

        function upload(){
            var input = $('#file')[0];
            if (!input.files && !input.files[0]) {
                notifWarning('File tidak boleh Kosong !');

                return;
            }

            var ukuran = input.files[0].size / 1024 / 1024;
            if (ukuran > 2){
                notifWarning('Ukuran File tidak boleh lebih dari 2 MB');

                return;
            }

            var file = input.files[0];
            var form = new FormData();

            form.append('file', file);
            form.append('_token', $('meta[name="csrf-token"]').attr('content'));

            $.ajax({
                url : "{{ url('transaksikehadiran/import/excel') }}",
                type: "POST",
                data: form,
                contentType: false,
                processData: false,
                dataType: "json",
                beforeSend:function(request) {
                    $('#modal_default').modal('hide');
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    reload_table();

                    $('#file').val(null);

                    swal(
                        'Berhasil !',
                        respon.msg,
                        'success'
                    );

                },error: function (jqXHR, textStatus, errorThrown){
                    $.unblockUI();
                    swal(
                        'Perhatian !!',
                        errorThrown,
                        'danger'
                    );
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="/"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                    <button onclick="reload_table()" class="btn btn-link btn-float text-default"><i class="icon-database-refresh text-primary"></i><span>Refresh</span></button>
                    <a href="{{ url('transaksikehadiran/form') }}" class="btn btn-link btn-float text-default"><i class="icon-stack-plus text-primary"></i><span>Tambah</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">{{ $title }}</span>
                </div>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>           

        </div>
    </div>

    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Filter Data</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Periode:</label>
                            <select class="form-control select" data-placeholder="Pilih Periode"
                                    name="periode" id="periode" onchange="reload_table()" data-fouc>
                                @foreach($periode as $r)
                                    <option value="{{$r->id}}">{{$r->bulan}} {{$r->tahun}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>:</label>
                            <select class="form-control select" data-placeholder="Pilih Jabatan"
                                    name="jabatan" id="jabatan" onchange="reload_table()" data-fouc>
                                <option value="">Pilih Jabatan</option>
                                <option value="Dosen Tetap">Dosen Tetap</option>
                                <option value="Pegawai Tetap">Pegawai Tetap</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>:</label>
                            <select class="form-control select" data-placeholder="Pilih Unit Kerja"
                                    name="unit" id="unit" onchange="reload_table()" data-fouc>
                                <option value="">Pilih Unit Kerja</option>
                                @foreach($unitkerja as $r)
                                    <option value="{{$r->id}}">{{$r->nama_unit}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <button type="button" id="btn" onclick="reload_table()" class="btn btn-primary">Cari<i class="icon-reload-alt ml-2"></i></button>
                    <button type="button" id="btn" onclick="generateImporter()" class="btn btn-success">Generate Template Excel<i class="icon-cloud-download2 ml-2"></i></button>
                    <button type="button" id="btn" onclick="showImport()" class="btn btn-info">Import Excel<i class="icon-import ml-2"></i></button>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data {{ $title }}</h5>

            </div>

            <div class="card-body"></div>

            <table class="table table-striped text-nowrap table-customers">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>Periode</th>
                    <th>Unit Kerja</th>
                    <th>Nama Pegawai</th>
                    <th>Jumlah Hari</th>
                    <th>Jumlah Jam </th>
                    <th>Persentase Kehadiran </th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <div id="modal_default" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Import Data Excel</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <form method="post" id="form_modal">
                        @csrf
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">File Kehadiran Excel:</label>
                            <div class="col-lg-8">
                                <input type="file" class="form-control" accept=".xlsx, .xls"
                                       placeholder="File Kehadiran" name="file" id="file">
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="button" onclick="upload()" class="btn bg-primary">Import</button>
                </div>
            </div>
        </div>
    </div>


@stop