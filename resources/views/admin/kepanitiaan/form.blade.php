@extends("base", ['tag' => 'masterdata.kepanitiaan'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>

    <script>        

        $(document).ready(function() {
            $('.select').select2({
                allowClear: true
            });

            if($('#panitia').val()){
                ambilSub(); 
                         
            }

        });

        function save(){

            if ($('#golongan').val() == ''){
                notifWarning('Isian Golongan tidak boleh kosong !');
                $('#golongan').focus();

                return;
            }          

            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('kepanitiaan') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }

        function ambilSub() {

            var id = $('#panitia').val();
            
            $.ajax({
                url : "{{ url('kepanitiaan/sub') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var str = '<select class="form-control" name="subpanitia" id="subpanitia">\n' +
                        '<option value="">Pilih Sub Kepanitiaan</option>';

                    var data = respon.data;
                    for (var i=0; i<data.length; i++){
                        var tmp = data[i];

                        str += '<option value="'+ tmp.id  +'">'+ tmp.nama_subkepanitiaan +'</option>'
                    }

                    str += '</select>';

                    $('#subpanitia').html(str);
                    $('#subpanitia').val({{$form['subpanitia']}});

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function savepanitia(){

            if ($('#mpanitia').val() == ''){
                notifWarning('Isian Panitia tidak boleh kosong !');
                $('#mpanitia').focus();

                return;
            }          

            $.ajax({
                url : "{{ $form['urlpanitia'] }}",
                type: "POST",
                data: $('#formpanitia').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('kepanitiaan') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }


    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('kepanitiaan') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('kepanitiaan') }}" class="breadcrumb-item"> Master Kepanitiaan</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf 

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Kepanitiaan:</label>
                                <div class="col-lg-8">
                                    <select class="form-control select" onchange="ambilSub()" name="panitia" id="panitia">
                                        <option value="0">Pilih Kepanitiaan</option>
                                        @foreach($panitia as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['panitia']) selected @endif>{{ $row->nama_kepanitiaan }}</option>
                                        @endforeach
                                    </select>   
                                </div>
                                <a href="{{ url('kepanitiaan/formpanitia') }}" class="col-lg-1 btn btn-warning" > <i class="fas fa-plus-circle"></i></a>
                                
                            </div>  

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Sub Kepanitiaan:</label>
                                <div class="col-lg-8">
                                    <select class="form-control select" name="subpanitia" id="subpanitia">
                                        <option value="0">Pilih Sub Kepanitiaan</option>                                        
                                    </select>
                                </div>                                
                                <button class="col-lg-1 btn btn-warning" data-toggle="modal" data-target="#modalsubpanitia" > <i class="fas fa-plus-circle"></i> </button>                              

                            </div>    

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Jabatan Kepanitiaan:</label>
                                <div class="col-lg-8">
                                    <select class="form-control select" name="jabatanpanitia" id="jabatanpanitia">
                                        <option value="0">Pilih Jabatan Kepanitiaan</option>  
                                        @foreach($jabatanpanitia as $row)
                                        <option value="{{ $row->item_value }}" @if($row->item_value == $form['jabatanpanitia']) selected @endif>{{ $row->item_display }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button class="col-lg-1 btn btn-warning" data-toggle="modal" data-target="#modaljabatanpanitia"> <i class="fas fa-plus-circle"></i> </button> 

                            </div> 

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nominal:</label>
                                <div class="col-lg-9">
                                    <input type="number" value="{{ $form['nominal'] }}" class="form-control" placeholder="Nominal" id="nominal" name="nominal" required>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal Panitia-->
    <div class="modal fade" id="modalpanitia" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Kepanitiaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" id="formpanitia">
                @csrf                           
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Kepanitiaan: </label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" placeholder="Kepanitiaan" id="mpanitia" name="mpanitia" required>
                    </div>
                </div>

                <div class="text-right">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal SubPanitia-->
    <div class="modal fade" id="modalsubpanitia" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">SubKepanitiaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal JabatanPanitia-->
    <div class="modal fade" id="modaljabatanpanitia" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Jabatan Kepanitiaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

@stop