<!DOCTYPE html>
<html lang="en">
<head>
    <link href="{{ url('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">

    <style type="text/css" media="print">
        @page { 
            size: portrait;
            margin-top: 40px;
            margin-bottom: 40px;
            margin-right: 20px;
            margin-left: 20px;
        }
       
    </style>
   
    <script src="{{ url('/global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>

    </script>
</head>

<!-- <body> -->
<body onload="window.print();">
    
@foreach ($cetak as $cetak)

    <div style="page-break-after:always">

        <div class="row">
            <div class="col-md-12">
                <font size="3" face="Times New Roman" >
                <table width="60%">
                    <tr style="height: 0;">
                        <td align="center" width="100%">
                            <img src="{{ url('/LogoUSB.PNG') }}" alt="Logo USB" heigth="30px">
                        </td>
                    </tr>
                    <tr style="height: 0;">
                        <td align="center" width="100%">
                            <small>Jalan Letjen Sutoyo Mojosongo</small><br>
                            <small>Website : www.setiabudi.ac.id Telp : (0271) 852518</small>                            
                        </td>
                    </tr>
                </table>                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <font size="3" face="Times New Roman" >
                <table width="60%" >
                    <tr style="height: 0;">
                        <td align="right" width="15%">
                            Nama
                        </td>
                        <td align="left" width="60%">
                            : {{$cetak['datapegawai']->nama}}
                        </td>
                        <td align="right" width="10%">
                            Jabatan 
                        </td>
                        <td align="left" width="15%">
                            : {{$cetak['jabatan']}}
                        </td>
                    </tr>                    
                </table>                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">    
                <table width="60%" border="1">
                    <tr style="height: 0;">
                        <td colspan="2" align="center">
                            Gaji 
                        </td>
                    </tr> 
                </table>            
                <table width="60%" border="1">
 
                    <tr style="height: 0;">
                        <td align="center" width="5%">
                            Keterangan
                        </td>
                        <td align="center" width="25%">
                            Jumlah
                        </td>
                    </tr>       
                    @php($sum=0)        

                    @foreach ($cetak['detailgaji'] as $detailgaji)
                    <tr style="height: 0;">
                        <td align="center" width="25%">
                           {{$detailgaji['komponen']}}
                        </td>
                        <td align="left" width="30%">
                            Rp {{$detailgaji['nominal']}}
                            @php($sum+=$detailgaji['nominal'])
                        </td>
                    </tr>                                  
                    @endforeach
                    <tr style="height: 0;">
                        <td align="center" width="25%">
                           Gaji Bruto
                        </td>
                        <td align="left" width="30%">
                            Rp {{$sum}}
                        </td>
                    </tr>
                    @php($sump=0)
                    @foreach ($cetak['detailpotongan'] as $detailpotongan)
                    <tr style="height: 0;">
                        <td align="center" width="25%">
                           {{$detailpotongan['komponen']}}
                        </td>
                        <td align="left" width="30%">
                            Rp {{$detailpotongan['nominal']}}
                            @php($sump+=$detailpotongan['nominal'])
                        </td>
                    </tr>                                  
                    @endforeach
                    <tr style="height: 0;">
                        <td align="center" width="25%">
                           Total Potongan
                        </td>
                        <td align="left" width="30%">
                            Rp {{$sump}}
                        </td>
                    </tr>
                    <tr style="height: 0;">
                        <td align="center" width="25%">
                           Total Gaji
                        </td>
                        <td align="left" width="30%">
                            Rp {{$sum - $sump }}
                        </td>
                    </tr>
  
                </table>
            </div>
        </div>
        <br>

        <br>
    </div>
      
@endforeach

</body>
</html>