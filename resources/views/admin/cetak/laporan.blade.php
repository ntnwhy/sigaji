<!DOCTYPE html>
<html lang="en">
<head>
    <link href="{{ url('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">

    <style type="text/css" media="print">
        @page { 
            size: portrait;
            margin-top: 40px;
            margin-bottom: 40px;
            margin-right: 20px;
            margin-left: 20px;
        }
       
    </style>
   
    <script src="{{ url('/global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ url('/global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script>

    </script>
</head>

<!-- <body> -->
<body onload="window.print();">
    


    <div style="page-break-after:always">       
        <div class="row">
            <div class="col-md-12">    
                <table width="100%" border="1">
                    <tr style="height: 0;">
                        <td colspan="2" align="center">
                         <h3> Laporan Gaji</h3> 
                        </td>
                    </tr> 
                </table>            
                @php($gt=0)
                @foreach ($cetak as $cetak)
                <table width="100%" border="1"> 
                    <tr style="height: 0;">
                        <td width="30%">
                            Nama
                        </td>
                        <td width="10%">
                            Jabatan
                        </td>
                        <td width="30%">
                            Gaji
                        </td>
                        <td width="15%">
                            Potongan
                        </td>
                        <td width="15%">
                            Total
                        </td>
                    </tr>       

                    <tr style="height: 0;">
                        <td>
                           {{$cetak['datapegawai']->nama}}
                        </td>
                        <td>
                           {{$cetak['jabatan']}}
                        </td>

                        @php($sum=0)

                        <td>
                        @foreach ($cetak['detailgaji'] as $detailgaji)
                            {{$detailgaji['komponen']}} = Rp {{$detailgaji['nominal']}} 
                            @php($sum+=$detailgaji['nominal'])
                        </br>
                        @endforeach
                            <b>Gaji Bruto = Rp {{$sum}}</b>
                        </td>
                        @php($sump=0)
                        <td>
                        @foreach ($cetak['detailpotongan'] as $detailpotongan)
                            {{$detailpotongan['komponen']}} = Rp {{$detailpotongan['nominal']}} 
                            @php($sump+=$detailpotongan['nominal'])
                        </br>
                        @endforeach
                            <b>Potongan = Rp {{$sump}}</b>
                        </td>
                        <td>
                            @php($t=$sum-$sump)
                            @php($gt+=$t)
                            <b>Gaji Bersih </br> 
                            Rp {{$t}}</b>
                        </td>
                    </tr>
                                  
                </table>
                <hr>
                    @endforeach

                <table width="100%" border="1"> 
                    <tr>
                       <td><b>Grand Total : </b></td> 
                       <td><b>Rp {{$gt}}</b></td> 
                    </tr>
                </table>
  
            </div>
        </div>
        <br>

        <br>
    </div>
      


</body>
</html>