@extends("base", ['tag' => 'masterdata.transaksikepanitiaan'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>

    <script>        

        function save(){
          
            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('transaksikepanitiaan') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }

        function ambilSub() {

            var id = $('#panitia').val();
            
            $.ajax({
                url : "{{ url('transaksikepanitiaan/sub') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var str = '<select class="form-control" name="subpanitia" id="subpanitia">\n' +
                        '<option value="">Pilih Sub Kepanitiaan</option>';

                    var data = respon.data;
                    for (var i=0; i<data.length; i++){
                        var tmp = data[i];

                        str += '<option value="'+ tmp.ref_id_subkepanitiaan  +'">'+ tmp.subpanitia.nama_subkepanitiaan +'</option>'
                    }

                    str += '</select>';

                    $('#subpanitia').html(str);

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }

        function ambilJabatan() {
            
            var id = $('#panitia').val()+"-"+$('#subpanitia').val();

           
            $.ajax({
                url : "{{ url('transaksikepanitiaan/jabatan') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    
                    var str = '<select class="form-control" name="jabatan" id="jabatan">\n' +
                        '<option value="">Pilih Jabatan Kepanitiaan</option>';

                    var data = respon.data;
                    for (var i=0; i<data.length; i++){
                        var tmp = data[i];

                        str += '<option value="'+ tmp.id  +'">'+ tmp.jabatan_kepanitiaan +'</option>'
                    }

                    str += '</select>';

                    $('#jabatan').html(str);

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }


    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('kepanitiaan') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('transaksikepanitiaan') }}" class="breadcrumb-item"> Transaksi Kepanitiaan</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf 

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Kepanitiaan:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" onchange="ambilSub()" name="panitia" id="panitia">
                                        <option value="0">Pilih Kepanitiaan</option>
                                        @foreach($panitia as $row)
                                        <option value="{{ $row->ref_id_kepanitiaan }}" @if($row->id == $form['panitia']) selected @endif>{{ $row->ref_id_kepanitiaan }} - {{ $row->panitia->nama_kepanitiaan }} </option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>  

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Sub Kepanitiaan:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" onchange="ambilJabatan()" name="subpanitia" id="subpanitia">
                                        <option value="0">Pilih Sub Kepanitiaan</option>                                        
                                    </select>
                                </div>
                            </div>    

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Jabatan Kepanitiaan:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="jabatan" id="jabatan">
                                        <option value="0">Pilih Jabatan Kepanitiaan</option>                                         
                                    </select>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Pegawai:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="pegawai" id="pegawai">
                                        <option value="0">Pilih Pegawai</option>  
                                        @foreach($pegawai as $row)
                                        <option value="{{ $row->id }}" @if($row->jabatan_kepanitiaan == $form['pegawai']) selected @endif>{{ $row->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Periode :</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="periode" id="periode">
                                        @foreach($periode as $row)
                                        <option value="{{ $row->id }}"> {{ $row->bulan }} {{ $row->tahun }} </option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>  

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop