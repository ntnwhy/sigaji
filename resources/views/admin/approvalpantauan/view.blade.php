@extends("base", ['tag' => 'approvalpantauan'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

    <script>
        var table;

        $(document).ready(function() {
            $('.select').select2({
                allowClear: true
            });

            initTable();   
           
        });

        function ubah(id){

            $('#mdedit').modal('show');
            $('#id').val(id);


        }

        function initTable(){

            table = $('.table-customers').DataTable({
                autoWidth: false,
                ordering: false,
                processing: true,
                serverSide: true,
                columnDefs: [
                {
                    width: 75,
                    targets: [0]
                } ],
                order: [[ 0, 'asc' ]],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 25, 50, 75, 100 ],
                displayLength: 25,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ],
                ajax: {
                    "url": "{{url('pantauanpengajaran/show')}}",
                    "type": "GET",
                    "data": {
                        periode: function () { return $('#periode').val(); }
                    },
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [
                    { 'data': 'id', "visible": false},
                    { "data": null,
                        render: function (data, type, full, meta) {
                            if (full['status'] == '1'){                            
                                return '<span class="badge badge-success">Disetujui</span>';
                            }else{
                                return '<span class="badge badge-danger">Belum Disetujui</span> <button onclick="approve('+full['id']+')" class="btn btn-link" id="btstatus" name="btstatus"><i class="fas fa-check"></i> </button> <button onclick="ubah('+full['id']+')" class="btn btn-link" id="btubah" name="btubah"><i class="fas fa-edit"></i> </button>';
                            }
                        }
                    },
                    { 'data': null,
                        render: function (data, type, full, meta) {
                            return full['periode']['bulan'] + " " + full['periode']['tahun'];                            
                        }
                    },
                    { 'data': 'pegawai.nama'},
                    { 'data': null,
                        render: function (data, type, full, meta) {
                            return full['makul']['KodeMataKuliah'] + " " + full['makul']['NamaMataKuliah'] + " | " + full['tipe_makul'] + " | " + full['sks'];
                        }
                    },
                    { "data": "kelas_kuliah" },
                    { "data": "durasi" },
                    { "data": "created_by" },
                    { "data": "created_at" },
                    { "data": "approved_by" },
                    { "data": "approved_at" },
                ]
            });
        }

        function reload_table(){

            table.ajax.reload(null, false);
        }

        function save(){

            if($('#durasi').val() == 0){
                notifWarning('Isian Durasi Ajar tidak boleh kosong !');
                $('#durasi').focus();

                return;
            }

            var id = $('#id').val();
          
            $.ajax({
                url : "{{ url('approvalpantauan')}}/"+id,
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('pantauanpengajaran') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }

        function approve(id){

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk approve data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Approve!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('approvalpantauan/approve') }}"+"/"+id,
                        type: "GET",
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }

        function approvemasal(){

            if($('#periode').val() == 0){
                notifWarning('Pilihan Periode Wajib dipilih !');
                $('#periode').focus();
                return;
            }

            periode = $('#periode option:selected').text();
            idperiode = $('#periode').val();
            idpegawai = $('#pegawai').val();

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk approve data pada periode "+ periode +" ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Approve!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('approvalpantauan/approvemasal') }}"+"/"+idperiode+"/"+idpegawai,
                        type: "get",
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="/"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">{{ $title }}</span>
                </div>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>           

        </div>
    </div>

    <div class="content">

        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Filter Data</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">   

                <form method="post" id="form">
                    @csrf                          
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Periode :</label>
                                <select class="form-control select" data-placeholder="Pilih Periode"
                                        name="periode" id="periode" data-fouc>
                                    <option></option>
                                    @foreach($periode as $r)
                                        <option value="{{$r->id}}">{{$r->bulan}} {{$r->tahun}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>     
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Pegawai :</label>
                                <select class="form-control select" name="pegawai" id="pegawai">
                                    <option value="">Semua Pegawai</option>  
                                    @foreach($pegawai as $row)
                                    <option value="{{ $row->id }}" >
                                    {{ $row->datapegawai['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>               
                    </div>
                </form>

                <div class="text-right">
                    <button type="button" id="btn" onclick="reload_table()" class="btn btn-primary">Cari<i class="icon-reload-alt ml-2"></i></button>
                   <!--  <button type="button" id="btn" onclick="generateImporter()" class="btn btn-success">Generate Template Excel<i class="icon-cloud-download2 ml-2"></i></button>
                    <button type="button" id="btn" onclick="showImport()" class="btn btn-info">Import Excel<i class="icon-import ml-2"></i></button> -->
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data {{ $title }}</h5>
                <a href="{{ url('pantauanpengajaran/rekap') }}" class="btn btn-success"><i class="icon-file"></i><span> Rekapitulasi Pantauan</span></a>
                 <button onclick="approvemasal()" class="btn btn-link" id="btapprovemasal" name="btapprovemasal"><i class="fas fa-check"></i> Approve Masal</button>

            </div>

            <div class="card-body">
               
            <table class="table table-striped text-nowrap table-customers">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>Status </th>
                    <th>Periode</th>
                    <th>Nama Pegawai</th>
                    <th>Mata Kuliah</th>
                    <th>Kelas Kuliah </th>
                    <th>Durasi </th>
                    <th>Entered By </th>
                    <th>Entered Date </th>
                    <th>Approved By </th>
                    <th>Approved Date </th>
                </thead>
                <tbody></tbody>
            </table>

            </div>
            
        </div>
    </div>

    <div class="modal" id="mdedit" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Ubah Durasi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="post" id="form">
                @csrf 

                <div class="form-group row">
                    
                    <input type="hidden" class="form-control" id="id" name="id" readonly>
                    

                    <label class="col-lg-4 col-form-label">Durasi Kehadiran (Jam):</label>
                    <div class="col-lg-2">
                       <input type="number" class="form-control" min="1" max="8" name="durasi" id="durasi" >
                    </div>
                </div>

            </form>

          </div>
          <div class="modal-footer">
            <div class="text-right">
                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


@stop