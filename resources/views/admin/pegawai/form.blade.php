@extends("base", ['tag' => 'pegawai'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>

    <script>

        function cekgp() {

            if ($('#golongan').val() == ''){
                notifWarning('Isian Golongan tidak boleh kosong !');
                $('#golongan').focus();

                return;
            }

            var gol         = $('#golongan').val();
            var mk          = $('#masa_kerja').val();
            var id          = gol+"-"+mk;
            
            $.ajax({
                url : "{{ url('pegawai/cekgp') }}/"+id,
                type: "GET",
                cache:false,
                dataType: "json",
                success: function(respon){
                    var detail = respon.data[0];

                   // $('#golongan').val(detail.ref_id_makul);
                   // $('#masa_kerja').val(detail.masa_kerja);
                   $('#gp').val(detail.id);

                             

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);
                }
            });
        }


        function save(){

            if ($('#golongan').val() == ''){
                notifWarning('Isian Golongan tidak boleh kosong !');
                $('#golongan').focus();

                return;
            }

    
            // if ($('#nama').val() == ''){
            //     notifWarning('Isian Nama tidak boleh kosong !');
            //     $('#nama').focus();

            //     return;
            // }

            // if ($('#jenis').val() == ''){
            //     notifWarning('Isian Jenis tidak boleh kosong !');
            //     $('#jenis').focus();

            //     return;
            // }

            // if ($('#tipe').val() == ''){
            //     notifWarning('Isian Tipe tidak boleh kosong !');
            //     $('#tipe').focus();

            //     return;
            // }

            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('pegawai') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('pegawai') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('pegawai') }}" class="breadcrumb-item"> Pegawai</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">NIS:</label>
                                <div class="col-lg-3">
                                    <input type="text" value="{{ $form['nis'] }}" class="form-control" id="nis" name="nis" required>
                                </div>

                                <label class="col-lg-3 col-form-label">Kode Gaji:</label>
                                <div class="col-lg-3">
                                    <input type="text" value="{{ $form['kode_gaji'] }}" class="form-control" id="kode_gaji" name="kode_gaji" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nama:</label>
                                <div class="col-lg-9">
                                    <input type="text" value="{{ $form['nama'] }}" class="form-control" id="nama" name="nama" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Jabatan:</label>
                                <div class="col-lg-3">
                                    <select class="form-control" name="jabatan" id="jabatan">
                                        <option value="">Pilih Jabatan</option>
                                        @foreach($jabatan as $row)
                                        <option value="{{ $row->item_value }}" @if($row->item_value == $form['jabatan']) selected @endif>{{ $row->item_display }} </option>
                                        @endforeach
                                    </select>
                                </div>
                                <label class="col-lg-3 col-form-label">Jabatan:</label>
                                <div class="col-lg-3">
                                    <select class="form-control" name="fungsi" id="fungsi">
                                        <option value="">Pilih Fungsi</option>
                                        @foreach($fungsi as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['fungsi']) selected @endif>{{ $row->kode }}  {{ $row->nama_fungsi }}</option>
                                        @endforeach
                                    </select>
                                </div> 
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Golongan:</label>
                                <div class="col-lg-3">
                                    <select class="form-control" name="golongan" id="golongan" onchange="cekgp()">
                                        <option value="">Pilih Golongan</option>
                                        @foreach($golongan as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['golongan']) selected @endif>{{ $row->golongan }} {{ $row->pangkat }}</option>
                                        @endforeach
                                    </select>
                                </div> 

                                <label class="col-lg-3 col-form-label">Masa Kerja:</label>
                                <div class="col-lg-3">
                                    <select class="form-control" name="masa_kerja" id="masa_kerja" onchange="cekgp()">
                                        <option value="1">Pilih Masa Kerja</option>
                                        @foreach($masa_kerja as $row)
                                        <option value="{{ $row->item_value }}" @if($row->item_value == $form['masa_kerja']) selected @endif>{{ $row->item_display }} </option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="gp" id="gp" value="{{ $form['gp'] }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Unit Kerja / Homebase:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="unit" id="unit">
                                        <option value="">Pilih Unit Kerja / Homebase</option>
                                        @foreach($unit as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['unit']) selected @endif>{{ $row->kode }} - {{ $row->nama_unit }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Jabatan Fungsional:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="jabatan_fungsional" id="jabatan_fungsional">
                                        <option value="">Pilih Jabatan Fungsional</option>
                                        @foreach($jabatan_fungsional as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['jabatan_fungsional']) selected @endif>{{ $row->kode }} {{ $row->nama_jabatan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Jabatan Struktural:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="jabatan_struktural" id="jabatan_struktural">
                                        <option value="">Pilih Jabatan Struktural</option>
                                        @foreach($jabatan_struktural as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['jabatan_struktural']) selected @endif>{{ $row->kode }} {{ $row->nama_jabatan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Pendidikan Akhir:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="pendidikan" id="pendidikan">
                                        <option value="">Pilih Pendidikan Terakhir</option>
                                        @foreach($pendidikan as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['pendidikan']) selected @endif>{{ $row->kode }} {{ $row->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop