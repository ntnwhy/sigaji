@extends("base", ['tag' => 'pegawai'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>

    <script>
        var table;

        $(document).ready(function() {
            initTable();
        });


        function initTable(){

            table = $('.table-customers').DataTable({
                autoWidth: false,
                ordering: false,
                processing: true,
                serverSide: true,
                scrollX: true,
                fixedColumns:   {
                    leftColumns: 4
                },
                order: [[ 0, 'asc' ]],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 10, 25, 50, 75, 100 ],
                displayLength: 10,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ],
                ajax: {
                    "url": "{{url('pegawai/show')}}",
                    "type": "GET",
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [
                    {
                        "data": "id",
                        render: function ( data, type, full, meta ) {
                            return '<div class="list-icons">\n' +
                                '                            <div class="dropdown">\n' +
                                '                                <a href="#" class="list-icons-item" data-toggle="dropdown">\n' +
                                '                                    <i class="icon-menu9"></i>\n' +
                                '                                </a>\n' +
                                '\n' +
                                '                                <div class="dropdown-menu dropdown-menu-right">\n' +
                                '                                    <a href="{{ url('pegawai/form') }}/'+ data +'" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>\n' +
                                '                                    <button onclick="haspus('+data+')" class="dropdown-item"><i class="icon-trash"></i> Hapus</button>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>';
                        }
                    },
                    { 'data': 'datapegawai.nip'},
                    { 'data': 'datapegawai.nama'},
                    { "data": 'jabatan'},
                    { "data": 'fungsi.nama_fungsi',
                        render: function(data, type, full, meta){
                            if(data){
                                return data;
                            }else{
                                return '-';
                            }
                        }
                    },
                    { "data": 'gajipokok.golongan.golongan',
                        render: function(data, type, full, meta){
                            if(data){
                                return data;
                            }else{
                                return '-';
                            }
                        }
                    },
                    { "data": 'gajipokok.masa_kerja',
                        render: function(data, type, full, meta){
                            if(data){
                                return data;
                            }else{
                                return '-';
                            }
                        }
                    },
                    { "data": 'jabfung.nama_jabatan',
                        render: function(data, type, full, meta){
                            if(data){
                                return data;
                            }else{
                                return '-';
                            }
                        }
                    },
                    { "data": 'struktural.nama_jabatan',
                        render: function(data, type, full, meta){
                            if(data){
                                return data;
                            }else{
                                return '-';
                            }
                        }
                    },
                    { "data": 'pendidikan.kode',
                        render: function(data, type, full, meta){
                            if(data){
                                return data;
                            }else{
                                return '-';
                            }
                        }
                    }
                ]
            });
        }

        function reload_table(){
            table.ajax.reload(null, false);
        }

        function haspus(id){

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk menghapus data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, hapus!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('pegawai/hapus') }}"+"/"+id,
                        type: "GET",
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }
    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="/"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                    <button onclick="reload_table()" class="btn btn-link btn-float text-default"><i class="icon-database-refresh text-primary"></i><span>Refresh</span></button>
                    <a href="{{ url('pegawai/form') }}" class="btn btn-link btn-float text-default"><i class="icon-stack-plus text-primary"></i><span>Tambah</span></a>
                    
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <span class="breadcrumb-item active">{{ $title }}</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data {{ $title }}</h5>

            </div>

            <div class="card-body"></div>

            <table class="table table-striped text-nowrap table-customers">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Status Pegawai</th>
                    <th>Fungsi</th>
                    <th>Golongan</th>
                    <th>Masa Kerja</th>
                    <th>Jabatan Fungsional</th>
                    <th>Jabatan Struktural</th>
                    <th>Pendidikan Akhir</th>

                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>


@stop