@extends("base", ['tag' => 'masterdata.transaksikepanitiaan'])

@section("js")
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_checkboxes_radios.js') }}"></script>

    <script>        

        $(document).ready(function() {

            $('.select').select2({
                allowClear: true
            });
           
        });

        function save(){

            if($('#periode').val() == 0){
                notifWarning('Isian Periode Penggajian tidak boleh kosong !');
                $('#periode').focus();

                return;
            }

            if($('#pegawai').val() == 0){
                notifWarning('Isian Pegawai tidak boleh kosong !');
                $('#pegawai').focus();

                return;
            }

            if($('#panitia').val() == 0){
                notifWarning('Isian Panitia tidak boleh kosong !');
                $('#panitia').focus();

                return;
            }

         
            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache:false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('transaksikepanitiaan') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });
        }

        function getnominal(){

            var sid =  $('#jabatan').val();

            $("#nominal").prop('selectedIndex', $("#panitia").prop('selectedIndex')); 

            // $('#nominal').val($('#jabatan').val());

            $('#nominal option:not(:selected)').attr('disabled', true);                    
            $('#nominal option:selected').attr('disabled', false); 

        }


    </script>
@stop

@section("content")
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><a href="{{ url('kepanitiaan') }}"><i class="icon-arrow-left52 mr-2"></i></a>
                    <span class="font-weight-semibold">{{ $title }}</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">

                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="{{ url('transaksikepanitiaan') }}" class="breadcrumb-item"> Transaksi Kepanitiaan</a>
                    <span class="breadcrumb-item active">Form</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>
                    </div>

                    <div class="card-body">
                        <form method="post" id="form">
                            @csrf 

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Periode :</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="periode" id="periode">
                                        @foreach($periode as $row)
                                        <option value="{{ $row->id }}"> {{ $row->bulan }} {{ $row->tahun }} </option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>  

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Pegawai:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" name="pegawai" id="pegawai">
                                        <option value="0">Pilih Pegawai</option>  
                                        @foreach($pegawai as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['pegawai']) selected @endif>{{ $row->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Kepanitiaan:</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" name="panitia" id="panitia" onchange="getnominal()">
                                        <option value="0">Pilih Kepanitiaan</option>
                                        @foreach($panitia as $row)
                                        <option value="{{ $row->id }}" @if($row->id == $form['panitia']) selected @endif> {{ $row->subpanitia->nama_subkepanitiaan }} | {{ $row->jabatan_kepanitiaan }}  </option>
                                        @endforeach
                                    </select>    
                                </div>
                            </div>  

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nominal :</label>
                                <div class="col-lg-3">
                                    <select class="form-control" name="nominal" id="nominal">
                                        <option value="0"></option>  
                                        @foreach($panitia as $row)
                                        <option value="{{ $row->nominal }}" @if($row->nominal == $form['nominal']) selected @endif> {{ $row->nominal }} </option>
                                        @endforeach                                       
                                    </select>

                                </div>
                                <label class="col-lg-3 col-form-label">Nominal Diusulkan :</label>
                                <div class="col-lg-3">
                                    <input type="number" value="{{ $form['nominalu'] }}" name="nominalu" id="nominalu" class="form-control">
                                </div>
                            </div> 

                            <div class="text-right">
                                <button type="button" id="btn" onclick="save()" data-loading-text="<i class='icon-spinner4 spinner'></i> Loading"  class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop